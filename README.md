GPU Module (GM)
==========

[[_TOC_]]


This module is for interfacing any C/C++ code with cuSPARSE/cuBLAS/cuSOLVER libraries in order
to compute matrix chain products among other functionalities. The matrices can be
float, double, cuComplex or cuDoubleComplex.  
The main goal of this module is to load CUDA libraries in the way it's generally done
for software plug-ins.  
It allows to work with the same software on environments where CUDA is available or
not.  
In the latter case the module won't load, but because of asbence of hard linking,
the program won't fail to run (as it would have done with library linking at build
time).  
A second aim of this module is to provide a simple API to GPU dense, sparse matrices
or array of matrices with no bother about CUDA API.  

**WARNING**: the project code is in beta status (besides it suffers of a lack of documentation).
Many tests are made indirectly in the [FAµST project](https://gitlab.inria.fr/faustgrp/faust).
  
Building
========

	mkdir build && cd build
	# on *nix systems by default the cuda toolkit is installed in /usr/local but you
    # might specify another path
	cmake -DCUDA_TOOLKIT_ROOT_DIR=/path/to/cuda-9.2 -DCMAKE_CUDA_COMPILER=/path/to/cuda-9.2/bin/nvcc  ..
	make
	# the library libgm.so/dylib/dll should be built and the test program test_gm too
    # (if Eigen3 is installed on the system)

**NOTE**: a makefile is also available in the project to build the program without
cmake (but it's written for a specific configuration). 

**Details for Windows Visual Studio**

	mkdir build && cd build
	cmake -G "Visual Studio 14 Win64" .. # no need to specify CUDA toolkit root dir.
                                         # if it's installed in a default location
										 # the architecture field 'Win64' really matters because nvcc (cuda 9.2)
                                         # won't compile for 32bits archi.
	cmake --build . --config Release # make is not needed, cmake is able to build
	cmake --install . --config Release # to install in C:\Program Files...

**Details for gcc**

- gcc10 suits well the project compilation with cuda 11 (gcc11 fails to build the
project).
To specify the proper compiler to use you should set the cmake variables ``CMAKE_CXX_COMPILER``,
``CMAKE_C_COMPLER``, for example:

	``cmake -DCUDA_TOOLKIT_ROOT_DIR=$HOME/cuda-11.6 -DCMAKE_CUDA_COMPILER=$HOME/cuda-11.6/bin/nvcc -DCMAKE_CXX_COMPILER=/opt/gcc10/bin/c++  -DCMAKE_C_COMPILER=/opt/gcc10/bin/gcc ..``

- CUDA 9.2 doesn't build with gcc version greater or equal than 7 and you'll get the
following error if you try.  

	``cuda-9.2/bin/..//include/crt/host_config.h:119:2: erreur: #error -- unsupported GNU version! gcc versions later than 7 are not supported!``

About CUDA
==========

This code is written upon CUDA 9 / 11 / 12 specific versions and is most likely not
working with another major version (maybe even with different minor version).  
Besides, at least cuSPARSE, cuBLAS and cuSOLVER must be installed (with headers for
development) to build the project.  

Using
=====

The first step to follow in user code is including the library header and activating
the library loading functions:

	#define __GM_LOADER__ // give you access to library loading helpers
	#include "gm_interf.h"

See test/ directory for examples.

An advanced example of user code is the [FAµST project](https://gitlab.inria.fr/faustgrp/faust).

TODO: more explanations.

Tests
=====

The code tests are located in the test project directory.  
It includes a rough benchmark against Eigen3.  
CMake builds it by default if Eigen is available.  
Look at the [FAµST project](https://gitlab.inria.fr/faustgrp/faust) for more tests.
