#ifndef __TEST__UTILS__
#define __TEST_UTILS__
#include <random>
#include <vector>
#include <Eigen/Core>
#include <memory>
#include <Eigen/SparseCore>

#define MAX_M 2048
#define MAX_N 2048
#define MIN_N 1024
#define MIN_M 1024
#define DEFAULT_DENSITY .2f

enum MatType
{
    DENSE,
    SPARSE,
    MIXED
};

template <typename T>
using SparseMat = Eigen::SparseMatrix<T,Eigen::RowMajor>;

template<typename T>
using DenseMat = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>;;

template<typename T>
struct MatContainer
{
    SparseMat<T>* sp_mat;
    DenseMat<T>* ds_mat;
    MatContainer(SparseMat<T>& sp_mat);
    MatContainer(DenseMat<T>& ds_mat);
    ~MatContainer();
};

template <typename T>
void randScalar(T &scalar);

template <typename T>
void randSpMat(int num_rows, int num_cols, float density, SparseMat<T>& mat);


template<typename T>
void randSizeSpMat(float density, SparseMat<T> & mat, int32_t min_nrows = MIN_M, int32_t max_nrows = MAX_M, int32_t min_ncols = MIN_N, int32_t max_ncols = MAX_N);

template <typename T>
void randSizeDsMat(DenseMat<T>& mat);

template <typename T>
void randDsMat(int num_rows, int num_cols, DenseMat<T>& mat);

template <typename T>
void randSizeDsMat(DenseMat<T>& mat);

template <typename T>
std::vector<std::shared_ptr<MatContainer<T>>> randMats(int nmats, MatType mt);

template<typename T>
void dispSpMat(SparseMat<T> &mat);

template<typename T>
void dispDsMat(DenseMat<T> &mat);

template<typename T>
void dispMat(MatContainer<T>& mc);

template<typename T>
void dispMats(std::vector<std::shared_ptr<MatContainer<T>>>& mats);

//TODO: move to eigen
template<typename T>
void gen_nmats(int nmats, int ***mat_sizes, float ***mats);

///TODO: move to eigen
template<typename T>
void disp_mat(int *mat_size, float *mat);

//TODO: move to eigen
template<typename T>
void disp_mats(int nmats, int **mat_sizes, float ** mats);

//TODO: move to eigen (dense or sparse)
template<typename T>
short test_eq(int nrows, int ncols, float* mat1, float* mat2);

template<typename T>
bool test_eq(SparseMat<T>& a, SparseMat<T>& b);

template<typename T>
void eig_chain_matmul(std::vector<std::shared_ptr<MatContainer<T>>>& ml, DenseMat<T>& res, gm_Op op = OP_NOTRANSP);
#include <complex>
using namespace std;

template<typename T>
gm_DenseMat_t wrapper_gpu_chain_matmul(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op = OP_NOTRANSP, gm_DenseMat_t mat=nullptr);

template<typename T>
void wrapper_dot(gm_DenseMat_t x, gm_DenseMat_t y, T *alpha, void *dsm_funcs);


template<typename T>
void wrapper_cpu2gpu(T *ds_mat, int32_t nrows, int32_t ncols, gm_MatArray_t gpu_mat_arr, void* marr_funcs);

template<typename T>
void wrapper_cpu2gpu( int32_t nrows, int32_t ncols, int32_t nnz, int32_t* outerIndexPtr, int32_t *innerIndexPtr, T* valuePtr, gm_MatArray_t gpu_mat_arr, void* marr_funcs);

template<typename T>
void wrapper_gpu_copy(gm_DenseMat_t src_mat, gm_DenseMat_t dst_mat, void* dsm_funcs);

template<typename T>
gm_DenseMat_t wrapper_cpu2gpu(T* ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs);

template<typename T>
gm_SparseMat_t wrapper_cpu2gpu( int32_t nrows, int32_t ncols, int32_t nnz, int32_t* outerIndexPtr, int32_t *innerIndexPtr, T* valuePtr, void* spm_funcs);

template<typename T>
void wrapper_gpu2cpu(T* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs);

template<typename T>
void wrapper_gpu2cpu(gm_SparseMat_t gpu_sp_mat, int32_t nnz, int32_t * outerIndexPtr, int32_t* innerIndexPtr, T* valuePtr, void* spm_funcs);

template<typename T>
void wrapper_add_cpu_dsm(T* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs);

template<typename T>
void wrapper_add_cpu_spm(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values, void* dsm_funcs);

template<typename T>
void wrapper_sub_cpu_dsm(T* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs);

template<typename T>
void wrapper_sub_cpu_spm(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values, void* dsm_funcs);

template<typename T>
void wrapper_mul_gpu_dsm_tocpu(gm_DenseMat_t, gm_DenseMat_t, T* data, void* dsm_funcs);

template<typename T>
void wrapper_mul_gpu_dsm_tocpu_ext(gm_DenseMat_t, gm_DenseMat_t, T* data, gm_Op, gm_Op, void* dsm_funcs);

template<typename T>
void wrapper_trace(void* dsm_funcs, gm_DenseMat_t m, T *trace);

template<typename T>
void wrapper_max(void* dsm_funcs, gm_DenseMat_t m, T *max);

template<typename T>
void wrapper_min(void* dsm_funcs, gm_DenseMat_t m, T *min);

template<typename T>
gm_DenseMat_t wrapper_prox_sp(gm_DenseMat_t m, int32_t k, void* dsm_funcs);

template<typename T>
T eig_max(DenseMat<T> &M);

template<typename T>
T eig_min(DenseMat<T> &M);

template<typename T>
gm_DenseMat_t wrapper_prox_sp(gm_DenseMat_t m, int32_t k, void* dsm_funcs);


#include "test_gm_utils.hpp"
#endif
