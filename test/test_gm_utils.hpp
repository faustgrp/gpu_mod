//template<>
//gm_DenseMat_t wrapper_gpu_chain_matmul<float>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op)
//{
//	return ((gm_MatArrayFunc_float*)marr_funcs)->chain_matmul(gpu_mat_arr, 1.f, op);
//}
//
//template<>
//gm_DenseMat_t wrapper_gpu_chain_matmul<double>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op)
//{
//	return ((gm_MatArrayFunc_double*)marr_funcs)->chain_matmul(gpu_mat_arr, 1., op);
//}
//
//template<>
//gm_DenseMat_t wrapper_gpu_chain_matmul<complex<float>>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op)
//{
//	cuComplex one;
//	one.x = 1;
//	one.y = 0;
//	return ((gm_MatArrayFunc_cuComplex*)marr_funcs)->chain_matmul(gpu_mat_arr, one, op);
//}
//
//template<>
//gm_DenseMat_t wrapper_gpu_chain_matmul<complex<double>>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op)
//{
//	cuDoubleComplex one;
//	one.x = 1;
//	one.y = 0;
//	return ((gm_MatArrayFunc_cuDoubleComplex*)marr_funcs)->chain_matmul(gpu_mat_arr, one, op);
//}

template<>
gm_DenseMat_t wrapper_gpu_chain_matmul<float>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op, gm_DenseMat_t mat)
{
	if(mat == nullptr) return ((gm_MatArrayFunc_float*)marr_funcs)->chain_matmul(gpu_mat_arr, 1.f, op);
	return ((gm_MatArrayFunc_float*)marr_funcs)->chain_matmul_by_dsm(gpu_mat_arr, 1.f, op, mat);
}

template<>
gm_DenseMat_t wrapper_gpu_chain_matmul<double>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op, gm_DenseMat_t mat)
{
	if(mat == nullptr) return ((gm_MatArrayFunc_double*)marr_funcs)->chain_matmul(gpu_mat_arr, 1., op);
	return ((gm_MatArrayFunc_double*)marr_funcs)->chain_matmul_by_dsm(gpu_mat_arr, 1., op, mat);
}

template<>
gm_DenseMat_t wrapper_gpu_chain_matmul<complex<float>>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op, gm_DenseMat_t mat)
{
	cuComplex one;
	one.x = 1;
	one.y = 0;
	if(mat == nullptr) return ((gm_MatArrayFunc_cuComplex*)marr_funcs)->chain_matmul(gpu_mat_arr, one, op);
	return ((gm_MatArrayFunc_cuComplex*)marr_funcs)->chain_matmul_by_dsm(gpu_mat_arr, one, op, mat);
}

template<>
gm_DenseMat_t wrapper_gpu_chain_matmul<complex<double>>(gm_MatArray_t gpu_mat_arr, void* marr_funcs, gm_Op op, gm_DenseMat_t mat)
{
	cuDoubleComplex one;
	one.x = 1;
	one.y = 0;
	if(mat == nullptr) return ((gm_MatArrayFunc_cuDoubleComplex*)marr_funcs)->chain_matmul(gpu_mat_arr, one, op);
	return ((gm_MatArrayFunc_cuDoubleComplex*)marr_funcs)->chain_matmul_by_dsm(gpu_mat_arr, one, op, mat);
}

template<>
void wrapper_cpu2gpu<float>(float *ds_mat, int32_t nrows, int32_t ncols, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
	((gm_MatArrayFunc_float*) marr_funcs)->togpu_dsm(gpu_mat_arr, nrows, ncols, ds_mat);
}

template<>
void wrapper_cpu2gpu<double>(double *ds_mat, int32_t nrows, int32_t ncols, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
	((gm_MatArrayFunc_double*) marr_funcs)->togpu_dsm(gpu_mat_arr, nrows, ncols, ds_mat);
}


//TODO: group the two complex functions in one with enable_if
template<>
void wrapper_cpu2gpu<complex<float>>(complex<float> *data, int32_t nrows, int32_t ncols, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
//	size_t numel = nrows*ncols;
//	cuComplex * _data = new cuComplex[numel];
//	for(int i=0;i<numel;i++)
//	{
//		_data[i].x = std::real(data[i]);
//		_data[i].y = std::imag(data[i]);
//	}
	((gm_MatArrayFunc_cuComplex*) marr_funcs)->togpu_dsm(gpu_mat_arr, nrows, ncols, /*_data*/ (cuComplex*)reinterpret_cast<float*>(data));
//	delete[] _data;
}

template<>
void wrapper_cpu2gpu<complex<double>>(complex<double> *data, int32_t nrows, int32_t ncols, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
//	size_t numel = nrows*ncols;
//	cuDoubleComplex * _data = new cuDoubleComplex[numel];
//	for(int i=0;i<numel;i++)
//	{
//		_data[i].x = std::real(data[i]);
//		_data[i].y = std::imag(data[i]);
//	}
	((gm_MatArrayFunc_cuDoubleComplex*) marr_funcs)->togpu_dsm(gpu_mat_arr, nrows, ncols, /*_data*/ (cuDoubleComplex*)reinterpret_cast<double*>(data));
//	delete[] _data;
}

template<>
gm_DenseMat_t wrapper_cpu2gpu<complex<double>>(complex<double>* ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
//	size_t numel = nrows*ncols;
//	cuDoubleComplex * _data = new cuDoubleComplex[numel];
//	for(int i=0;i<numel;i++)
//	{
//		_data[i].x = std::real(ds_mat[i]);
//		_data[i].y = std::imag(ds_mat[i]);
//	}
	gm_DenseMat_t cu_mat_hdr = ((gm_DenseMatFunc_cuDoubleComplex*) dsm_funcs)->togpu(nrows, ncols, /*_data*/ (cuDoubleComplex*)reinterpret_cast<double*>(ds_mat));
//	delete[] _data;
	return cu_mat_hdr;
}

template<>
gm_DenseMat_t wrapper_cpu2gpu<complex<float>>(complex<float>* ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
//	size_t numel = nrows*ncols;
//	cuComplex * _data = new cuComplex[numel];
//	for(int i=0;i<numel;i++)
//	{
//		_data[i].x = std::real(ds_mat[i]);
//		_data[i].y = std::imag(ds_mat[i]);
//	}
	gm_DenseMat_t cu_mat_hdr = ((gm_DenseMatFunc_cuComplex*) dsm_funcs)->togpu(nrows, ncols, (cuComplex*)reinterpret_cast<float*>(ds_mat)/*_data*/);
//	delete[] _data;
	return cu_mat_hdr;
}

template<>
gm_DenseMat_t wrapper_cpu2gpu<float>(float* ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	return ((gm_DenseMatFunc_float*) dsm_funcs)->togpu(nrows, ncols, ds_mat);
}

	template<>
gm_DenseMat_t wrapper_cpu2gpu<double>(double* ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	return ((gm_DenseMatFunc_double*) dsm_funcs)->togpu(nrows, ncols, ds_mat);
}


template<>
gm_SparseMat_t wrapper_cpu2gpu<float>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, float *valuePtr, void* spm_funcs)
{
    return ((gm_SparseMatFunc_float*) spm_funcs)->togpu(nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, valuePtr);
}

template<>
gm_SparseMat_t wrapper_cpu2gpu<double>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, double *valuePtr, void* spm_funcs)
{
    return ((gm_SparseMatFunc_double*) spm_funcs)->togpu(nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, valuePtr);
}

template<>
gm_SparseMat_t wrapper_cpu2gpu<complex<float>>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, complex<float> *valuePtr,void* spm_funcs)
{
//    cuComplex * _data = new cuComplex[nnz];
//    for(int i=0;i<nnz;i++)
//    {
//        _data[i].x = std::real(valuePtr[i]);
//        _data[i].y = std::imag(valuePtr[i]);
//    }
    auto gpu_sp_mat = ((gm_SparseMatFunc_cuComplex*) spm_funcs)->togpu(nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, (cuComplex*) reinterpret_cast<float*>(valuePtr)/*_data*/);
//    delete[] _data;
    return gpu_sp_mat;
}

template<>
gm_SparseMat_t wrapper_cpu2gpu<complex<double>>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, complex<double> *valuePtr, void* spm_funcs)
{
//    cuDoubleComplex * _data = new cuDoubleComplex[nnz];
//    for(int i=0;i<nnz;i++)
//    {
//        _data[i].x = std::real(valuePtr[i]);
//        _data[i].y = std::imag(valuePtr[i]);
//    }
    auto gpu_sp_mat =((gm_SparseMatFunc_cuDoubleComplex*) spm_funcs)->togpu(nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, (cuDoubleComplex*) reinterpret_cast<double*>(valuePtr) /*_data*/);
//    delete[] _data;
    return gpu_sp_mat;
}

template<>
void wrapper_gpu2cpu<complex<float>>(complex<float>* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
//	size_t numel = nrows*ncols;
//	cuComplex * _data = new cuComplex[numel];
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->tocpu(gpu_ds_mat, (cuComplex*) reinterpret_cast<float*>(ds_mat)/*_data*/);
//	for(int i=0;i<numel;i++)
//	{
//		ds_mat[i] = complex<float>(_data[i].x, _data[i].y);
//	}
//	delete[] _data;
}

template<>
void wrapper_gpu2cpu<complex<double>>(complex<double>* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
//	size_t numel = nrows*ncols;
//	cuDoubleComplex * _data = new cuDoubleComplex[numel];
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->tocpu(gpu_ds_mat, (cuDoubleComplex*) reinterpret_cast<double*>(ds_mat) /*_data*/);
//	for(int i=0;i<numel;i++)
//	{
//		ds_mat[i] = complex<double>(_data[i].x, _data[i].y);
//	}
//	delete[] _data;
}

template<>
void wrapper_add_cpu_dsm<float>(float* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->add_cpu_dsm(gpu_ds_mat, (float*) ds_mat/*_data*/, nrows, ncols);
}

template<>
void wrapper_add_cpu_dsm<double>(double* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->add_cpu_dsm(gpu_ds_mat, (double*) ds_mat/*_data*/, nrows, ncols);
}

template<>
void wrapper_add_cpu_dsm<complex<float>>(complex<float>* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->add_cpu_dsm(gpu_ds_mat, (cuComplex*) reinterpret_cast<float*>(ds_mat)/*_data*/, nrows, ncols);
}

template<>
void wrapper_add_cpu_dsm<complex<double>>(complex<double>* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->add_cpu_dsm(gpu_ds_mat, (cuDoubleComplex*) reinterpret_cast<double*>(ds_mat) /*_data*/, nrows, ncols);
}

template<>
void wrapper_add_cpu_spm<float>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, float* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->add_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, values);
}

template<>
void wrapper_add_cpu_spm<double>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, double* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->add_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, values);
}

template<>
void wrapper_add_cpu_spm<complex<float>>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, complex<float>* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->add_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, (cuComplex*) reinterpret_cast<double*>(values));
}

template<>
void wrapper_add_cpu_spm<complex<double>>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, complex<double>* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->add_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, (cuDoubleComplex*) reinterpret_cast<double*>(values));
}

template<>
void wrapper_sub_cpu_dsm<float>(float* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->sub_cpu_dsm(gpu_ds_mat, (float*) ds_mat/*_data*/, nrows, ncols);
}

template<>
void wrapper_sub_cpu_dsm<double>(double* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->sub_cpu_dsm(gpu_ds_mat, (double*) ds_mat/*_data*/, nrows, ncols);
}


template<>
void wrapper_sub_cpu_dsm<complex<float>>(complex<float>* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->sub_cpu_dsm(gpu_ds_mat, (cuComplex*) reinterpret_cast<float*>(ds_mat)/*_data*/, nrows, ncols);
}

template<>
void wrapper_sub_cpu_dsm<complex<double>>(complex<double>* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->sub_cpu_dsm(gpu_ds_mat, (cuDoubleComplex*) reinterpret_cast<double*>(ds_mat) /*_data*/, nrows, ncols);
}

template<>
void wrapper_sub_cpu_spm<float>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, float* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->sub_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, values);
}

template<>
void wrapper_sub_cpu_spm<double>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, double* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->sub_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, values);
}

template<>
void wrapper_sub_cpu_spm<complex<float>>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, complex<float>* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->sub_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, (cuComplex*) reinterpret_cast<float*>(values));
}

template<>
void wrapper_sub_cpu_spm<complex<double>>(gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, complex<double>* values, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->sub_cpu_spm(gpu_ds_mat, nrows, ncols, nnz, row_ptr, col_inds, (cuDoubleComplex*) reinterpret_cast<double*>(values));
}

template<>
void wrapper_mul_gpu_dsm_tocpu<float>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, float* data, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->mul_gpu_dsm_tocpu(a_mat, b_mat, data);
}

template<>
void wrapper_mul_gpu_dsm_tocpu<double>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, double* data, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->mul_gpu_dsm_tocpu(a_mat, b_mat, data);
}

template<>
void wrapper_mul_gpu_dsm_tocpu<complex<float>>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, complex<float>* data, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->mul_gpu_dsm_tocpu(a_mat, b_mat, reinterpret_cast<cuComplex*>(data));
}

template<>
void wrapper_mul_gpu_dsm_tocpu<complex<double>>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, complex<double>* data, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->mul_gpu_dsm_tocpu(a_mat, b_mat, reinterpret_cast<cuDoubleComplex*>(data));
}

template<>
void wrapper_mul_gpu_dsm_tocpu_ext<float>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, float* data, gm_Op op_a, gm_Op op_b, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->mul_gpu_dsm_tocpu_ext(a_mat, b_mat, data, op_a, op_b);
}

template<>
void wrapper_mul_gpu_dsm_tocpu_ext<double>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, double* data, gm_Op op_a, gm_Op op_b, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->mul_gpu_dsm_tocpu_ext(a_mat, b_mat, data, op_a, op_b);
}

template<>
void wrapper_mul_gpu_dsm_tocpu_ext<complex<float>>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, complex<float>* data, gm_Op op_a, gm_Op op_b, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->mul_gpu_dsm_tocpu_ext(a_mat, b_mat, reinterpret_cast<cuComplex*>(data), op_a, op_b);
}

template<>
void wrapper_mul_gpu_dsm_tocpu_ext<complex<double>>(gm_DenseMat_t a_mat, gm_DenseMat_t b_mat, complex<double>* data, gm_Op op_a, gm_Op op_b, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->mul_gpu_dsm_tocpu_ext(a_mat, b_mat, reinterpret_cast<cuDoubleComplex*>(data), op_a, op_b);
}

template<>
void wrapper_dot<float>(gm_DenseMat_t x, gm_DenseMat_t y, float *alpha, void *dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->dot(x, y, alpha);
}

template<>
void wrapper_dot<double>(gm_DenseMat_t x, gm_DenseMat_t y, double* alpha, void *dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->dot(x, y, alpha);
}

template<>
void wrapper_dot<complex<float>>(gm_DenseMat_t x, gm_DenseMat_t y, complex<float> *alpha, void *dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->dot(x, y, reinterpret_cast<cuComplex*>(alpha));
}

template<>
void wrapper_dot<complex<double>>(gm_DenseMat_t x, gm_DenseMat_t y, complex<double> *alpha, void *dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->dot(x, y, reinterpret_cast<cuDoubleComplex*>(alpha));
}

template<>
void wrapper_gpu2cpu<float>(float* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_float*)dsm_funcs)->tocpu(gpu_ds_mat, ds_mat);
}

template<>
void wrapper_gpu2cpu<double>(double* ds_mat, gm_DenseMat_t gpu_ds_mat, int32_t nrows, int32_t ncols, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->tocpu(gpu_ds_mat, ds_mat);
}

template<>
void wrapper_gpu_copy<double>(gm_DenseMat_t src_mat, gm_DenseMat_t dst_mat, void* dsm_funcs)
{
	((gm_DenseMatFunc_double*)dsm_funcs)->copy(src_mat, dst_mat);
}

template<>
void wrapper_gpu_copy<float>(gm_DenseMat_t src_mat, gm_DenseMat_t dst_mat, void* dsm_funcs)
{

	((gm_DenseMatFunc_float*)dsm_funcs)->copy(src_mat, dst_mat);
}

template<>
void wrapper_gpu_copy<complex<float>>(gm_DenseMat_t src_mat, gm_DenseMat_t dst_mat, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuComplex*)dsm_funcs)->copy(src_mat, dst_mat);
}

template<>
void wrapper_gpu_copy<complex<double>>(gm_DenseMat_t src_mat, gm_DenseMat_t dst_mat, void* dsm_funcs)
{
	((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->copy(src_mat, dst_mat);
}

template<>
void wrapper_cpu2gpu<float>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, float *valuePtr, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
	((gm_MatArrayFunc_float*) marr_funcs)->togpu_spm(gpu_mat_arr, nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, valuePtr);
}

template<>
void wrapper_cpu2gpu<double>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, double *valuePtr, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
	((gm_MatArrayFunc_double*) marr_funcs)->togpu_spm(gpu_mat_arr, nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, valuePtr);
}

template<>
void wrapper_cpu2gpu<complex<float>>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, complex<float> *valuePtr,gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
//	cuComplex * _data = new cuComplex[nnz];
//	for(int i=0;i<nnz;i++)
//	{
//		_data[i].x = std::real(valuePtr[i]);
//		_data[i].y = std::imag(valuePtr[i]);
//	}
	((gm_MatArrayFunc_cuComplex*) marr_funcs)->togpu_spm(gpu_mat_arr, nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, /*_data*/ (cuComplex*) reinterpret_cast<float*>(valuePtr));
//	delete[] _data;
}

template<>
void wrapper_cpu2gpu<complex<double>>(int32_t nrows, int32_t ncols, int32_t nnz,  int32_t* outerIndexPtr, int32_t *innerIndexPtr, complex<double> *valuePtr, gm_MatArray_t gpu_mat_arr, void* marr_funcs)
{
//	size_t numel = nrows*ncols;
//	cuDoubleComplex * _data = new cuDoubleComplex[nnz];
//	for(int i=0;i<nnz;i++)
//	{
//		_data[i].x = std::real(valuePtr[i]);
//		_data[i].y = std::imag(valuePtr[i]);
//	}
	((gm_MatArrayFunc_cuDoubleComplex*) marr_funcs)->togpu_spm(gpu_mat_arr, nrows, ncols, nnz, outerIndexPtr, innerIndexPtr, (cuDoubleComplex*) reinterpret_cast<double*>(valuePtr) /*_data*/);
//	delete[] _data;
}

template<>
void wrapper_gpu2cpu<float>(gm_SparseMat_t gpu_sp_mat, int32_t nnz, int32_t * outerIndexPtr, int32_t *innerIndexPtr, float* valuePtr, void* spm_funcs)
{
	((gm_SparseMatFunc_float*) spm_funcs)->tocpu(gpu_sp_mat, outerIndexPtr, innerIndexPtr, valuePtr);
}

template<>
void wrapper_gpu2cpu<double>(gm_SparseMat_t gpu_sp_mat, int32_t nnz, int32_t * outerIndexPtr, int32_t* innerIndexPtr, double* valuePtr, void* spm_funcs)
{
	((gm_SparseMatFunc_double*) spm_funcs)->tocpu(gpu_sp_mat, outerIndexPtr, innerIndexPtr, valuePtr);
}

template<>
void wrapper_gpu2cpu<complex<float>>(gm_SparseMat_t gpu_sp_mat, int32_t nnz, int32_t * outerIndexPtr, int32_t *innerIndexPtr, complex<float>* valuePtr, void* spm_funcs)
{
	cuComplex * _data = new cuComplex[nnz];
	((gm_SparseMatFunc_cuComplex*) spm_funcs)->tocpu(gpu_sp_mat, outerIndexPtr, innerIndexPtr, _data);
	for(int i=0;i<nnz;i++)
	{
		valuePtr[i] = complex<float>(_data[i].x, _data[i].y);
	}
	delete[] _data;
}

template<>
void wrapper_gpu2cpu<complex<double>>(gm_SparseMat_t gpu_sp_mat, int32_t nnz, int32_t * outerIndexPtr, int32_t * innerIndexPtr, complex<double>* valuePtr, void* spm_funcs)
{
	cuDoubleComplex * _data = new cuDoubleComplex[nnz];
	((gm_SparseMatFunc_cuDoubleComplex*) spm_funcs)->tocpu(gpu_sp_mat, outerIndexPtr, innerIndexPtr, _data);
	for(int i=0;i<nnz;i++)
	{
		valuePtr[i] = complex<double>(_data[i].x, _data[i].y);
	}
	delete[] _data;
}

	template<>
void wrapper_max<double>(void* dsm_funcs, gm_DenseMat_t m, double *max)
{
	((gm_DenseMatFunc_double* )dsm_funcs)->max(m, max);
}

template<>
void wrapper_max<float>(void*  dsm_funcs, gm_DenseMat_t m, float *max)
{
	((gm_DenseMatFunc_float* )dsm_funcs)->max(m, max);
}

template<>
void wrapper_max<complex<float>>(void*  dsm_funcs, gm_DenseMat_t m, complex<float> *max)
{
	((gm_DenseMatFunc_cuComplex* )dsm_funcs)->max(m, (cuComplex*)reinterpret_cast<float*>(max));
}

	template<>
void wrapper_max<complex<double>>(void* dsm_funcs, gm_DenseMat_t m, complex<double> *max)
{
	((gm_DenseMatFunc_cuDoubleComplex* )dsm_funcs)->max(m, (cuDoubleComplex*) reinterpret_cast<double*>(max));
}

	template<>
void wrapper_min<double>(void* dsm_funcs, gm_DenseMat_t m, double *min)
{
	((gm_DenseMatFunc_double* )dsm_funcs)->min(m, min);
}

template<>
void wrapper_min<float>(void*  dsm_funcs, gm_DenseMat_t m, float *min)
{
	((gm_DenseMatFunc_float* )dsm_funcs)->min(m, min);
}

template<>
void wrapper_min<complex<float>>(void*  dsm_funcs, gm_DenseMat_t m, complex<float> *min)
{
	((gm_DenseMatFunc_cuComplex* )dsm_funcs)->min(m, (cuComplex*)reinterpret_cast<float*>(min));
}

	template<>
void wrapper_min<complex<double>>(void* dsm_funcs, gm_DenseMat_t m, complex<double> *min)
{
	((gm_DenseMatFunc_cuDoubleComplex* )dsm_funcs)->min(m, (cuDoubleComplex*) reinterpret_cast<double*>(min));
}

	template<>
void wrapper_trace<double>(void* dsm_funcs, gm_DenseMat_t m, double *trace)
{
	((gm_DenseMatFunc_double* )dsm_funcs)->trace(m, trace);
}

template<>
void wrapper_trace<float>(void*  dsm_funcs, gm_DenseMat_t m, float *trace)
{
	((gm_DenseMatFunc_float* )dsm_funcs)->trace(m, trace);
}

template<>
void wrapper_trace<complex<float>>(void*  dsm_funcs, gm_DenseMat_t m, complex<float> *trace)
{
	((gm_DenseMatFunc_cuComplex* )dsm_funcs)->trace(m, (cuComplex*)reinterpret_cast<float*>(trace));
}

	template<>
void wrapper_trace<complex<double>>(void* dsm_funcs, gm_DenseMat_t m, complex<double> *trace)
{
	((gm_DenseMatFunc_cuDoubleComplex* )dsm_funcs)->trace(m, (cuDoubleComplex*) reinterpret_cast<double*>(trace));
}

template<typename T>
gm_DenseMat_t wrapper_prox_sp(gm_DenseMat_t m, int32_t k, void* dsm_funcs)
{
	//TODO: it should be a template specialization instead of if-else
	if(sizeof(T) == sizeof(float))
		return ((gm_DenseMatFunc_float*)dsm_funcs)->prox_sp(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(double))
		return ((gm_DenseMatFunc_double*)dsm_funcs)->prox_sp(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(complex<float>))
		return ((gm_DenseMatFunc_cuComplex*)dsm_funcs)->prox_sp(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(complex<double>))
		return ((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->prox_sp(m, k, /* normalized */ false, /* pos */false);

}

template<typename T>
gm_DenseMat_t wrapper_prox_spcol(gm_DenseMat_t m, int32_t k, void* dsm_funcs)
{
	//TODO: it should be a template specialization instead of if-else
	if(sizeof(T) == sizeof(float))
		return ((gm_DenseMatFunc_float*)dsm_funcs)->prox_spcol(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(double))
		return ((gm_DenseMatFunc_double*)dsm_funcs)->prox_spcol(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(complex<float>))
		return ((gm_DenseMatFunc_cuComplex*)dsm_funcs)->prox_spcol(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(complex<double>))
		return ((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->prox_spcol(m, k, /* normalized */ false, /* pos */false);

}

template<typename T>
gm_DenseMat_t wrapper_prox_splin(gm_DenseMat_t m, int32_t k, void* dsm_funcs)
{
	//TODO: it should be a template specialization instead of if-else
	if(sizeof(T) == sizeof(float))
		return ((gm_DenseMatFunc_float*)dsm_funcs)->prox_splin(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(double))
		return ((gm_DenseMatFunc_double*)dsm_funcs)->prox_splin(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(complex<float>))
		return ((gm_DenseMatFunc_cuComplex*)dsm_funcs)->prox_splin(m, k, /* normalized */ false, /* pos */false);
	else if(sizeof(T) == sizeof(complex<double>))
		return ((gm_DenseMatFunc_cuDoubleComplex*)dsm_funcs)->prox_splin(m, k, /* normalized */ false, /* pos */false);

}
/*********************************** WARNING below this line a cuComplex/cuDoubleComplex becomes a complex<float>/complex<double> ****************************/
// shunt of cuComplex cuDoubleComplex types defs for Eigen (which doesn't understand cuda types)
#define cuComplex std::complex<float>
#define cuDoubleComplex std::complex<double>

template<typename T>
void randSizeSpMat(float density, SparseMat<T> & mat, int32_t min_nrows, int32_t max_nrows, int32_t min_ncols, int32_t max_ncols)
{
    // should use C++ random but it doesn't matter
    int32_t m = (int32_t) ((float) rand() * (max_nrows-min_nrows) / RAND_MAX)+min_nrows;
    int32_t n = (int32_t) ((float) rand() * (max_ncols-min_ncols) / RAND_MAX)+min_ncols;
    randSpMat(m, n, density, mat);
}

template <typename T>
void randSpMat(int num_rows, int num_cols, float density, SparseMat<T>& mat)
{
    std::default_random_engine generator(rand());
//    std::uniform_real_distribution<T> distribution(0, 1);
	std::uniform_int_distribution<int> int_distribution(0, num_rows*num_cols-1);
    typedef Eigen::Triplet<T,int> U;
    std::vector<U> tripletList;
    T rs;
    mat = SparseMat<T>(num_rows, num_cols);
    try {
        int num_elts = (int)(num_rows*num_cols*density);
//		cout << "num_elts:" << num_elts << endl;
        tripletList.reserve(num_elts);
        int i,col_id,row_id, r;
        i = 0;
        while(i < num_elts)
        {
            r = int_distribution(generator);
            row_id = r/num_cols;
            col_id = r - row_id * num_cols;
            assert(row_id >=0 && row_id < num_rows);
            assert(col_id >= 0 && col_id < num_cols);
//            rs = distribution(generator);
			randScalar(rs);
//            std::cout << row_id << " " << col_id << " " << rs << std::endl;

            tripletList.push_back(U(row_id,col_id, rs));
            i++;
        }
        mat.setFromTriplets(tripletList.begin(), tripletList.end());
    }
    catch(std::exception e) {
        //      std::cerr << "Out of memory." << std::endl;
    }
//	cout << "sp mat:" << mat << endl;
}

template <typename T>
void randDsMat(int num_rows, int num_cols, DenseMat<T>& mat)
{
    mat = DenseMat<T>::Random(num_rows, num_cols);
}

template <typename T>
void randSizeDsMat(DenseMat<T>& mat)
{
    int32_t m = (int32_t) ((float) rand() * (MAX_M-MIN_M) / RAND_MAX)+MIN_M;
    int32_t n = (int32_t) ((float) rand() * (MAX_N-MIN_N) / RAND_MAX)+MIN_N;
    randDsMat(m,n, mat);
}

template <typename T>
std::vector<std::shared_ptr<MatContainer<T>>> randMats(int nmats, MatType mt)
{
    unsigned int m, n;
    n = (int) ((float) rand() * (MAX_M-MIN_M) / RAND_MAX)+MIN_M;
    std::vector<std::shared_ptr<MatContainer<T>>> mats;
    std::shared_ptr<MatContainer<T>> mc;
    MatType ite_mt;
    for(int i = 0;i < nmats; i++)
    {
        m = n;
        n = (int) ((float) rand() * (MAX_N-MIN_N) / RAND_MAX)+MIN_N;
        if(mt == MIXED)
            if(rand() < RAND_MAX/2)
                ite_mt = SPARSE;
            else
                ite_mt = DENSE;
        else
            ite_mt = mt;
        switch(ite_mt)
        {
            case SPARSE:
                {
                    SparseMat<T> mat;
                    randSpMat(m,n, DEFAULT_DENSITY, mat);
                    mc = std::make_shared<MatContainer<T>>(mat);
                }
                break;
            case DENSE:
                {
                    DenseMat<T> mat;
                    randDsMat(m, n, mat);
                    mc = std::make_shared<MatContainer<T>>(mat);
                }
                break;
            default:
                throw std::runtime_error("Unknow matrix type.");
        }
        mats.push_back(mc);
    }
    return mats;
}

template<typename T>
void gen_nmats(int nmats, int ***mat_sizes, T ***mats)
{
	unsigned int m, n;
	n = (int) ((float) rand() * (MAX_M-MIN_M) / RAND_MAX)+MIN_M;
	T * mat;
	*mat_sizes = (int**) malloc(sizeof(int**)*nmats);
	*mats = (T**) malloc(sizeof(T*)*nmats);
	for(int i = 0;i < nmats; i++)
	{
		m = n;
		n = (int) ((float) rand() * (MAX_N-MIN_N) / RAND_MAX)+MIN_N;
		(*mats)[i] = (T*) malloc(sizeof(T) * m * n);
		(*mat_sizes)[i] = (int*) malloc(sizeof(int)*2);
		(*mat_sizes)[i][0] = m;
		(*mat_sizes)[i][1] = n;
		for (int j = 0; j < m * n; j++)
			(*mats)[i][j] = ((T)rand()) / static_cast<T>(RAND_MAX);
	}
}


template<typename T>
void dispSpMat(SparseMat<T> &mat)
{
    std::cout << "SPARSE " << mat.rows() << " x " << mat.cols() << " nnz: " << mat.nonZeros() << std::endl;
}

template<typename T>
void dispDsMat(DenseMat<T> &mat)
{
    std::cout << "DENSE " << mat.rows() << " x " << mat.cols() << " nnz: " << mat.nonZeros() << std::endl;
}

template<typename T>
void dispMat(MatContainer<T>& mc)
{
    if(mc.sp_mat)
        dispSpMat(*mc.sp_mat);
    else if(mc.ds_mat)
        dispDsMat(*mc.ds_mat);
}

template<typename T>
void dispMats(std::vector<std::shared_ptr<MatContainer<T>>>& mats)
{
    std::cout << "Chain of " << mats.size() << " matrices:" << std::endl;
    for(int i=0;i < mats.size(); i++)
    {
        dispMat(*mats[i]);
    }
}

template<typename T>
void eig_chain_matmul(std::vector<std::shared_ptr<MatContainer<T>>>& ml, DenseMat<T>& res, gm_Op op)
{
    std::cout << "my_chain_matmul" << std::endl;
	auto it = ml.end()-1;
//	cout << "ml.size():" << ml.size() << endl;
    if((*it)->sp_mat)
        res = DenseMat<T>(*(*it)->sp_mat);
    else
        res = *(*it)->ds_mat;
//	cout << "resensor size: " << res.size(0) << " x " << res.size(0) << endl;
	while(it != ml.begin())
	{
//		torch::matmul(*(--it), res);
        --it;
        if((*it)->sp_mat)
            res = *(*it)->sp_mat*res;
        else
            res = *(*it)->ds_mat*res;
	}
	if(op == OP_TRANSP)
		res = res.transpose().eval();
	else if(op == OP_CONJTRANSP)
	{
		res = res.adjoint().eval();
	}
}

template<typename T>
void disp_mat(int *mat_size, T *mat)
{
	int m = mat_size[0], n = mat_size[1];
	printf("(%p) : size %d x %d\n", mat, m, n);
	if( m < 50 && n < 50)
		for(int j = 0; j < m; j++)
		{
			for(int k=0;k < n; k++)
			{
				printf("%f ", mat[k*m+j]);
			}
			printf("\n");
		}
}

template<typename T>
void disp_mats(int nmats, int **mat_sizes, T ** mats)
{
	int m, n;
	for(int i=0;i < nmats;i++)
	{
		printf("mat %d ", i);
//		m = mat_sizes[i][0], n = mat_sizes[i][1];
//		printf("mat %d (%p) : %d x %d\n", i, mats[i], m, n);
//		if( m < 10 && n < 10)
//			for(int j = 0; j < m; j++)
//			{
//				for(int k=0;k < n; k++)
//				{
//					printf("%f ", mats[i][k*m+j]);
//				}
//				printf("\n");
//			}
		disp_mat(mat_sizes[i], mats[i]);
	}
}

template<typename T>
short test_eq(int nrows, int ncols, T* mat1, T* mat2)
{
    for(int i=0;i<nrows*ncols; i++)
        if(mat1[i] != mat2[i]) return 0;
    return 1;
}

template<typename T>
bool test_eq(SparseMat<T>& a, SparseMat<T>& b)
{
    if(a.rows() != b.rows() || a.cols() != b.cols())
        return false;
    if(a.nonZeros() != b.nonZeros())
        return false;
    for(int i=0;i<b.nonZeros();i++)
        if(a.valuePtr()[i] != b.valuePtr()[i] | a.innerIndexPtr()[i] != b.innerIndexPtr()[i])
            return false;
    for(int i=0; i <= b.rows(); i++)
        if(a.outerIndexPtr()[i] != b.outerIndexPtr()[i])
            return false;
    return true;
}

template<typename T>
MatContainer<T>::MatContainer(SparseMat<T>& sp_mat): sp_mat(new SparseMat<T>(sp_mat)), ds_mat(nullptr)
{

}

template<typename T>
MatContainer<T>::MatContainer(DenseMat<T> &ds_mat) : ds_mat(new DenseMat<T>(ds_mat)), sp_mat(nullptr)
{

}

template<typename T>
MatContainer<T>::~MatContainer()
{
    if(sp_mat != nullptr)
        delete sp_mat;
    if(ds_mat != nullptr)
        delete ds_mat;
}

template <>
void randScalar<float>(float &scalar)
{
	std::default_random_engine generator(rand());
	std::uniform_real_distribution<float> distribution(0, 1);
	scalar = distribution(generator);
}

template <>
void randScalar<double>(double &scalar)
{
	std::default_random_engine generator(rand());
	std::uniform_real_distribution<double> distribution(0, 1);
	scalar = distribution(generator);
}

template <>
void randScalar<complex<float>>(complex<float> &scalar)
{
	std::default_random_engine generator(rand());
	randScalar<float>(reinterpret_cast<float*>(&scalar)[0]);
	randScalar<float>(reinterpret_cast<float*>(&scalar)[1]);
}

template <>
void randScalar<complex<double>>(complex<double> &scalar)
{
	std::default_random_engine generator(rand());
	randScalar<double>(reinterpret_cast<double*>(&scalar)[0]);
	randScalar<double>(reinterpret_cast<double*>(&scalar)[1]);
}


template<>
float eig_max<float>(DenseMat<float>& M)
{
	return M.maxCoeff();
}

	template<>
double eig_max<double>(DenseMat<double> &M)
{
	return M.maxCoeff();
}

	template<>
complex<double> eig_max<complex<double>>(DenseMat<complex<double>> &M)
{
	complex<double> cpu_max = *M.data();
	for(int i=1;i < M.rows()*M.cols();i++)
	{
		auto cur_elt = *(M.data()+i);
		if(std::abs(cur_elt) > std::abs(cpu_max))
			cpu_max = cur_elt;
	}
	return cpu_max;
}

	template<>
complex<float> eig_max<complex<float>>(DenseMat<complex<float>> &M)
{
	complex<float> cpu_max = *M.data();
	for(int i=1;i < M.rows()*M.cols();i++)
	{
		auto cur_elt = *(M.data()+i);
		if(std::abs(cur_elt) > std::abs(cpu_max))
			cpu_max = cur_elt;
	}
	return cpu_max;
}

template<>
float eig_min<float>(DenseMat<float>& M)
{
	return M.minCoeff();
}

	template<>
double eig_min<double>(DenseMat<double> &M)
{
	return M.minCoeff();
}

	template<>
complex<double> eig_min<complex<double>>(DenseMat<complex<double>> &M)
{
	complex<double> cpu_min = *M.data();
	for(int i=1;i < M.rows()*M.cols();i++)
	{
		auto cur_elt = *(M.data()+i);
		if(std::abs(cur_elt) < std::abs(cpu_min))
			cpu_min = cur_elt;
	}
	return cpu_min;
}

	template<>
complex<float> eig_min<complex<float>>(DenseMat<complex<float>> &M)
{
	complex<float> cpu_min = *M.data();
	for(int i=1;i < M.rows()*M.cols();i++)
	{
		auto cur_elt = *(M.data()+i);
		if(std::abs(cur_elt) < std::abs(cpu_min))
			cpu_min = cur_elt;
	}
	return cpu_min;
}
