// this header is to avoid user code to know anything about cuda devkit at compilation time agains this lib
// redefine cuda complex types for the user code point of view (no need to access the cuda dev kit headers if the gpu_mod header is provided)
#ifndef __GM_CU_COMPLEX__
#define __GM_CU_COMPLEX__
typedef struct cuComplex
{
	float x;
	float y;
} cuComplex;

typedef struct cuDoubleComplex
{
	double x;
	double y;
} cuDoubleComplex;
#endif
