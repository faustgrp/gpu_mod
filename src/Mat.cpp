#include "Mat.h"

Mat::Mat(int32_t nrows, int32_t ncols) : nrows(nrows), ncols(ncols)
{
}

Mat& Mat::operator=(const Mat& other)
{
	nrows = other.nrows;
	ncols = other.ncols;
	return *this;
}
