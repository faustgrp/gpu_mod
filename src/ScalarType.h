#ifndef __GPU_MOD_SCALARTYPE__
#define __GPU_MOD_SCALARTYPE__
#include <complex>
#include <cusparse.h>
#include <cublas_v2.h>

enum Scalar //TODO: rename to ScalarType
{
	GM_FLOAT,
	GM_DOUBLE,
	GM_CPLX_FLOAT,
	GM_CPLX_DOUBLE,
};

template<typename T>
Scalar type2Scalar();
#include "ScalarType.hpp"
#endif
