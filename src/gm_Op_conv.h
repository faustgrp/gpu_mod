#ifndef __GM_OP_CONV__
#define __GM_OP_CONV__
#include "gm_Op.h"
#include <cusparse.h>
#include <cublas_v2.h>

cublasOperation_t gm_Op2cublas(gm_Op op);
cusparseOperation_t gm_Op2cusparse(gm_Op op);
#endif

