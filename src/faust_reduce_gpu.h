/****************************************************************************/
/*                              Description:                                */
/*  For more information on the FAuST Project, please visit the website     */
/*  of the project : <http://faust.inria.fr>                         */
/*                                                                          */
/*                              License:                                    */
/*  Copyright (2019):    Hakim Hadj-Djilani, Nicolas Bellot, Adrien Leman, Thomas Gautrais,      */
/*                      Luc Le Magoarou, Remi Gribonval                     */
/*                      INRIA Rennes, FRANCE                                */
/*                      http://www.inria.fr/                                */
/*                                                                          */
/*  The FAuST Toolbox is distributed under the terms of the GNU Affero      */
/*  General Public License.                                                 */
/*  This program is free software: you can redistribute it and/or modify    */
/*  it under the terms of the GNU Affero General Public License as          */
/*  published by the Free Software Foundation.                              */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful, but     */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of              */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    */
/*  See the GNU Affero General Public License for more details.             */
/*                                                                          */
/*  You should have received a copy of the GNU Affero General Public        */
/*  License along with this program.                                        */
/*  If not, see <http://www.gnu.org/licenses/>.                             */
/*                                                                          */
/*                             Contacts:                                    */
/*      Hakim H. hakim.hadj-djilani@inria.fr                                */
/*      Nicolas Bellot  : nicolas.bellot@inria.fr                           */
/*      Adrien Leman    : adrien.leman@inria.fr                             */
/*      Thomas Gautrais : thomas.gautrais@inria.fr                          */
/*      Luc Le Magoarou : luc.le-magoarou@inria.fr                          */
/*      Remi Gribonval  : remi.gribonval@inria.fr                           */
/*                                                                          */
/*                              References:                                 */
/*  [1] Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse       */
/*  approximations of matrices and applications", Journal of Selected       */
/*  Topics in Signal Processing, 2016.                                      */
/*  <https://hal.archives-ouvertes.fr/hal-01167948v1>                       */
/****************************************************************************/

#ifndef __FAUST_CU_REDUCE_H__
#define __FAUST_CU_REDUCE_H__

#include <thrust/detail/config.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/inner_product.h>
#include "Real.h"

/*template<typename faust_real> class faust_cu_vec;
template<typename faust_real> class faust_cu_matrix;

template<typename faust_real>
faust_real faust_cu_reduce(const faust_cu_vec<faust_real>& v);

template<typename faust_real>
faust_real faust_cu_reduce(const faust_cu_matrix<faust_real>& M);*/


template<typename FPP>
FPP faust_cu_sum(const FPP* data, const int nb_el);

template<typename T> __device__
T sum_abs(const T & a, const T &b){return abs(a)+abs(b);};

template<typename FPP>
Real<FPP> faust_cu_sum_abs(const FPP* data, const int nb_el);

template<typename FPP>
Real<FPP> faust_cu_sum_abs_real(const FPP* data, const int nb_el);

template<typename FPP>
Real<FPP> faust_cu_sum_abs_cplx(const FPP* data, const int nb_el);

template<typename T>
T faust_cu_min_max_cplx(const T* data, const int nb_el, const bool ext_is_max);

template<typename FPP>
FPP faust_cu_min(const FPP* data, const int nb_el);

template<typename FPP>
FPP faust_cu_max(const FPP* data, const int nb_el);

template<typename FPP>
FPP faust_cu_norm(const FPP* data, const int nb_el);



#endif
