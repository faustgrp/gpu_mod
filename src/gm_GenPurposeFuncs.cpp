#include "gm.h"

extern "C"
{
	__DYNLIB_ATTR__ void gm_GenPurpose_free_mat(void* cu_mat)
	{
		(static_cast<Mat*>(cu_mat))->destroy();
	}

	__DYNLIB_ATTR__ void gm_GenPurpose_set_dev(const int32_t dev_id)
	{
		set_dev(dev_id);
	}

	__DYNLIB_ATTR__ int32_t gm_GenPurpose_cur_dev()
	{
		return cur_dev();
	}

	__DYNLIB_ATTR__ int32_t gm_GenPurpose_dev_count()
	{
		return dev_count();
	}

	__DYNLIB_ATTR__ void* gm_GenPurpose_create_stream()
	{
		return create_stream();
	}
}
