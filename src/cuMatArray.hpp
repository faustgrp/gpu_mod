template<typename T>
cuMatArray<T>::cuMatArray() : del_mats(false)
{
}

template<typename T>
cuMatArray<T>::~cuMatArray()
{
	if(del_mats)
		for(auto m: cu_array)
			delete m;
}

template<typename T>
int32_t cuMatArray<T>::nrows() const
{
	return cu_array[0]->nrows;
}

template<typename T>
size_t cuMatArray<T>::get_total_nnz() const
{
	size_t nnz = 0;
	for(auto m: cu_array)
		nnz += m->get_nnz();
	return nnz;
}

template<typename T>
int32_t cuMatArray<T>::ncols() const
{
	return (*(cu_array.end()-1))->ncols;
}

template<typename T>
void cuMatArray<T>::display(gm_Op op)
{
	std::cout << to_string(op) << std::endl;
}

template<typename T>
std::string cuMatArray<T>::to_string(gm_Op op)
{
	std::string out_str="";
	std::vector<int> im(cu_array.size());
	for(int i=0;i<cu_array.size();i++) im[i] = i;
	if(op != OP_NOTRANSP)
		std::reverse(std::begin(im), std::end(im));
	for(int i : im)
	{
		std::string mat_type_str = cu_array[i]->is_sparse()? (cu_array[i]->is_bsr()? " BSR": " SPARSE"):" DENSE";
//#ifdef DEBUG
		out_str += std::string("- GPU FACTOR ") + std::to_string(op==OP_NOTRANSP?i:size()-i-1) + std::string((std::is_same<T,cuComplex>::value || std::is_same<T,cuDoubleComplex>::value)?" (complex)": (std::is_same<T, double>::value?" (double)":" (float)")) + mat_type_str + " size ";
		out_str += std::to_string(op == OP_NOTRANSP?cu_array[i]->nrows:cu_array[i]->ncols);
		out_str += std::string(" x ") + std::to_string(op == OP_NOTRANSP?cu_array[i]->ncols:cu_array[i]->nrows);
		out_str += std::string(", addr: "); 
//		std::to_string(static_cast<void*>(cu_array[i]));
		char addr[sizeof(void*)+1];
		std::sprintf(addr, "%p", cu_array[i]);
		out_str += std::string((const char*)addr);
		out_str += std::string(", density ") + std::to_string(((double)cu_array[i]->get_nnz())/cu_array[i]->nrows/cu_array[i]->ncols) + ", nnz " + std::to_string(cu_array[i]->get_nnz());
		out_str += "\r\n";
//#endif
	}
	return out_str;
}

template<typename T>
void cuMatArray<T>::mul(const T& scalar, int32_t id/*=-1*/)
{
	if(size() == 0)
		throw std::runtime_error("None matrix to multiply.");
	if(! is_one(scalar))
	{
		if(id < 0 || id >= size())
		{
			// no valid factor id selected to multiply the cuMatArray
			// choose the smallest factor to minimize the mul. cost
			int min_size_id = 0;
			std::vector<int> fact_ids(size());
			int i = -1;
			std::generate(fact_ids.begin(), fact_ids.end(), [&i](){return ++i;});
			std::vector<int>::iterator result = std::min_element(fact_ids.begin(), fact_ids.end(), [this](const int &a, const int &b){return cu_array[a]->get_nbytes() < cu_array[b]->get_nbytes();});
			min_size_id = std::distance(fact_ids.begin(), result);
			id = min_size_id;
		}
		cu_array[id]->mul(scalar);
	}
}

template<typename T>
void cuMatArray<T>::transpose()
{
	std::reverse(begin(cu_array), end(cu_array));
	for(auto M: cu_array)
		M->transpose();
}

template<typename T>
void cuMatArray<T>::adjoint()
{
	std::reverse(begin(cu_array), end(cu_array));
	for(auto M: cu_array)
		M->adjoint();
}

template<typename T>
cuMatDs<T>* cuMatArray<T>::chain_matmul_r2l(T alpha, gm_Op op, cuMatDs<T>* out /* = nullptr*/)
{
	std::vector<cuMat<T>*>& cl = this->cu_array;
//	std::cout << "chain_matmul_r2l<T>" << std::endl;
//	std::cout << "gm_Op=" << op << std::endl;
	cusparseStatus_t cusparse_status;
	cublasStatus_t cublas_status;
	int m, n, o, i, j;
	int32_t max_nrows = 0, max_ncols = 0, itmp;
	cuMatDs<T>* out_;
	cuMatDs<T>* ite_out, *ite_rfac, *tmp = nullptr;
	int nmats = cl.size();
	cuMatSp<T>* spmat;
	cuMatDs<T>* dsmat;
	cublasOperation_t cublas_op;
	cusparseOperation_t cusparse_op;
	std::vector<int> im(nmats); //index map (useful to multiply in reverse order, needed for op OP_TRANSP or OP_CONJTRANSP)
	//	this->display(op);
	// convert gm_Op to cusparse, cublas op
	cublas_op = gm_Op2cublas(op);
	//	cusparse_op = gm_Op2cusparse(op);
	T alpha_one;
	set_one(&alpha_one);
	T beta;
	//	beta = 0;
	bzero(&beta, sizeof(T));
	// compute the buffer size
	max_ncols = cl[nmats-1]->ncols;
	for(int i=0;i<nmats;i++)
	{
		if(max_nrows < cl[i]->nrows)
			max_nrows = cl[i]->nrows;
	}
	// init the two alternating buffers (out_ and tmp, for double-buffering) if nmats > 1 (else one buffer is enough)
	// the size of buffers is set to be enough large for any of the product to proceed
	// but this size can be larger than the final product matrix size
	if(out != nullptr)
	{
		if(out->buf_nrows*out->buf_ncols < max_nrows*max_ncols)
		{
			std::cerr << "out->buf_nrows: " << out->buf_nrows << " out->buf_ncols: " << out->buf_ncols;
			std::cerr << " max_nrows: " << max_nrows << " max_ncols: " << max_ncols << std::endl;
			throw std::runtime_error("The out buffer passed is too small");
		}
		else
			out_ = out;
	}
	else
		out_ = cuMatDs<T>::create((*(cl.begin()))->nrows, (*(cl.end()-1))->ncols, max_nrows, max_ncols);
	if(nmats > 1)
		tmp = cuMatDs<T>::create(out_->nrows, out_->ncols, out_->buf_nrows, out_->buf_ncols);
	//
	// TODO: test if necessary to have A/B and C to be different buffers, i.e. out_ and tmp buffers
	//TODO: add "copy(gm_Op)" virtual function in Mat*
	if(nmats == 1)
	{
		m = cl[0]->nrows;
		n = cl[0]->ncols;
		//TODO: check that op(factor) is not reversed when it's sparse (double op, see *l2r for good code in this case)
		// only one factor, just copy it in out_ buf
		if(cl[0]->is_csr())
		{
			cusparse_csr2dense(static_cast<cuMatSp<T>*>(cl[0]), out_, op);
		}
		else if(cl[0]->is_bsr())
		{
			dynamic_cast<cuMatBSR<T>*>(cl[0])->to_dense(*out_, op);
		}
		else //if(op == OP_NOTRANSP)
		{
			auto status = cublasTcopy(cuMatDs<T>::handle, m*n, static_cast<cuMatDs<T>*>(cl[0])->data, 1, out_->data, 1);
			check_cublas_error(status, "cuMatArray::chain_matmul_r2l cublasTcopy");
			out_->apply_op(op);
		}
		if(nullptr != tmp) delete tmp;
		return out_;
	}
	i = nmats-1; // first rfac
	if(nmats%2)
	{
		ite_out = tmp;
		if(cl[i]->is_sparse())
		{
			if(cl[i]->is_csr())
				cusparse_csr2dense(static_cast<cuMatSp<T>*>(cl[i]), out_);
			else if(cl[i]->is_bsr())
				dynamic_cast<cuMatBSR<T>*>(cl[i])->to_dense(*out_);
			ite_rfac = out_;
		}
		else
			ite_rfac = static_cast<cuMatDs<T>*>(cl[i]);
	}
	else
	{
		ite_out = out_;
		if(cl[i]->is_sparse())
		{
			if(cl[i]->is_csr())
				cusparse_csr2dense(static_cast<cuMatSp<T>*>(cl[i]), tmp);
			else if(cl[i]->is_bsr())
				dynamic_cast<cuMatBSR<T>*>(cl[i])->to_dense(*tmp);
			ite_rfac = tmp;
		}
		else
			ite_rfac = static_cast<cuMatDs<T>*>(cl[i]);
	}

	for(int i = nmats-2; i >= 0; i--)
	{
		//		cout << "matrix type:" << cusparseGetMatType(spmat.descr) << endl;
		m = cl[i]->nrows;
		n = cl[i]->ncols;
		o = cl[nmats-1]->ncols;
		ite_out->nrows = cl[i]->nrows;
		ite_out->ncols = ite_rfac->ncols;
//		std::cout << "loop i = "<< i << " m: " << m << " n:" << n << " o:" << o << std::endl;
		if(cl[i]->is_csr())
		{
			spmat = static_cast<cuMatSp<T>*>(cl[i]);
			//std::cout << " mat i is sparse nnz: " << spmat->nnz << std::endl;
			//std::cout << " cusparse handle : " << spmat->handle << std::endl;
			//std::cout << " pts values, rowptr, colids, rfac data, out_ data: " << spmat->values<< " " <<  spmat->rowptr<< " " <<  spmat->colids<< " " <<  ite_rfac->data<< " " <<  ite_out->data  << std::endl;
#if (CUDA_VERSION <= 9200)
			cusparse_status = cusparseTcsrmm2(spmat->handle, CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_NON_TRANSPOSE, m, o, n, spmat->nnz, i==0?&alpha:&alpha_one,
					spmat->descr /*cusparseMatDescr_t*/, spmat->values, spmat->rowptr, spmat->colids, ite_rfac->data, n, &beta, ite_out->data, m);
#else
			cusparse_status = helper_cusparseSpMM(*spmat, *ite_rfac,
					CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_NON_TRANSPOSE,
					i==0?alpha:alpha_one, beta,
					*ite_out,
					"cuMatArray<T>::chain_matmul_r2l");
#endif

			check_cusparse_error(cusparse_status, "chain_matmul > helper_cusparseSpMM");
		}
		else if(cl[i]->is_bsr())
		{
			auto bsrmat = dynamic_cast<cuMatBSR<T>*>(cl[i]);
			bsrmat->mul(*ite_rfac, ite_out);
		}
		else
		{
			dsmat = static_cast<cuMatDs<T>*>(cl[i]);
			auto status = cublasTgemm(dsmat->handle, CUBLAS_OP_N, CUBLAS_OP_N, m, o, n, i==0?&alpha:&alpha_one, dsmat->data,
					m, ite_rfac->data, n, &beta, ite_out->data, m);
			check_cublas_error(status, "cuMatArray::chain_matmul_r2l cublasTgemm");
		}

		if(ite_out == tmp)
		{
			ite_out = out_;
			ite_rfac = tmp;
		}
		else
		{
			ite_out = tmp;
			ite_rfac = out_;
		}
	}
	//	if(nmats > 1)
	delete tmp;
	out_->nrows = cl[0]->nrows;
	out_->ncols = (*(cl.end()-1))->ncols;
	//	std::cout << "end of chain_matmul_r2l out_: " << out_ << std::endl;
	if(op == OP_NOTRANSP)
		return out_;
	// the transpose or transconjugate has been asked
	// do the extra op on the result (simpler than computing the whole product factor by factor)
	out_->apply_op(op);
	return out_;
}

template<typename T>
cuMatDs<T>* cuMatArray<T>::chain_matmul_l2r(T alpha, gm_Op op, cuMatDs<T>* out /* = nullptr*/)
{
	std::vector<cuMat<T>*>& cl = this->cu_array;
//	std::cout << "chain_matmul_l2r<T>" << std::endl;
//	std::cout << "gm_Op=" << op << std::endl;
	cusparseStatus_t cusparse_status;
	cublasStatus_t cublas_status;
	int m, n, k, i, j;
#if (CUDA_VERSION <= 9200)
	int ldb;
#endif
	int32_t max_nrows = (*(cl.begin()))->nrows, max_ncols = (*(cl.end()-1))->ncols, itmp;
	cuMatDs<T>* out_;
	cuMatDs<T>* ite_out, *ite_rfac, *tmp = nullptr;
	int nmats = cl.size();
	cuMatSp<T>* spmat;
	cuMatDs<T>* dsmat;
	cublasOperation_t cublas_op;
	cusparseOperation_t cusparse_op;
	std::vector<int> im(nmats); //index map (useful to multiply in reverse order, needed for op OP_TRANSP or OP_CONJTRANSP)
	//	this->display(op);
	// convert gm_Op to cusparse, cublas op
	cublas_op = gm_Op2cublas(op);
	cublas_op = cublas_op==CUBLAS_OP_N?CUBLAS_OP_T:cublas_op;
	cusparse_op = gm_Op2cusparse(op);
	cusparse_op = cusparse_op==CUSPARSE_OPERATION_NON_TRANSPOSE?CUSPARSE_OPERATION_TRANSPOSE:cusparse_op;
	T alpha_one;
	set_one(&alpha_one);
	T beta;
	//	beta = 0;
	bzero(&beta, sizeof(T));
	// compute the buffer size
	max_ncols = cl[0]->nrows; // multiplying from left to right (transpose factors)
	for(int i=1;i<nmats;i++)
	{
		if(max_nrows < cl[i]->ncols)
			max_nrows = cl[i]->ncols;
	}
	//	std::cout << "max_nrows=" << max_nrows << " max_ncols=" << max_ncols << std::endl;
	//	std::cout << "cl[0] nrows=" << cl[0]->nrows << " cl[0] ncols=" << cl[0]->ncols << std::endl;
	// init the two alternating buffers (out_ and tmp, for double-buffering) if nmats > 1 (else one buffer is enough)
	// the size of buffers is set to be enough large for any of the product to proceed
	// but this size can be larger than the final product matrix size
	if(out != nullptr)
	{
		if(out->buf_nrows*out->buf_ncols < max_nrows*max_ncols)
		{
			std::cerr << "out->buf_nrows: " << out->buf_nrows << " out->buf_ncols: " << out->buf_ncols;
			std::cerr << " max_nrows: " << max_nrows << " max_ncols: " << max_ncols << std::endl;
			throw std::runtime_error("The out buffer passed is too small");
		}
		out_ = out;
	}
	else
		out_ = cuMatDs<T>::create((*(cl.end()-1))->ncols, (*(cl.begin()))->nrows, max_nrows, max_ncols);
	if(nmats > 1)
		tmp = cuMatDs<T>::create(out_->nrows, out_->ncols, out_->buf_nrows, out_->buf_ncols);

	// TODO: test if necessary to have A/B and C to be different buffers, i.e. out_ and tmp buffers
	if(nmats == 1)
	{
		// TODO: copy(Mat*) (throwing an exception if exec type is hMat)
		m = cl[0]->nrows;
		n = cl[0]->ncols;
		// only one factor, just copy it in out_ buf
		if(cl[0]->is_csr())
		{
			cusparse_csr2dense(static_cast<cuMatSp<T>*>(cl[0]), out_, op);
		}
		else if(cl[0]->is_bsr())
		{
			dynamic_cast<cuMatBSR<T>*>(cl[0])->to_dense(*out_, op);
		}
		else //if(op == OP_NOTRANSP)
		{
			auto status = cublasTcopy(cuMatDs<T>::handle, m*n, static_cast<cuMatDs<T>*>(cl[0])->data, 1, out_->data, 1);
			check_cublas_error(status, "cuMatArray::chain_matmul_l2r cublasTcopy");
			out_->apply_op(op);
		}
		if(nullptr != tmp) delete tmp;
		return out_;
	}
	i = 0; // first lfac
	if(nmats%2)
	{
		ite_out = tmp;
		if(cl[i]->is_sparse())
		{
			if(cl[i]->is_csr())
				cusparse_csr2dense(static_cast<cuMatSp<T>*>(cl[i]), out_);
			else if(cl[i]->is_bsr())
				dynamic_cast<cuMatBSR<T>*>(cl[i])->to_dense(*out_);
			ite_rfac = out_;
		}
		else
			ite_rfac = static_cast<cuMatDs<T>*>(cl[i]);
	}
	else
	{
		ite_out = out_;
		if(cl[i]->is_sparse())
		{
			if(cl[i]->is_csr())
				cusparse_csr2dense(static_cast<cuMatSp<T>*>(cl[i]), tmp);
			else if(cl[i]->is_bsr())
				dynamic_cast<cuMatBSR<T>*>(cl[i])->to_dense(*tmp);
			ite_rfac = tmp;
		}
		else
			ite_rfac = static_cast<cuMatDs<T>*>(cl[i]);
	}

	n = cl[0]->nrows;

	for(int i = 1; i < nmats; i++)
	{
		//		cout << "matrix type:" << cusparseGetMatType(spmat.descr) << endl;

		if(cl[i]->is_csr())
		{
//			std::cout << "loop i = "<< i << " m: " << m << " n:" << n << " k:" << k << std::endl;
			spmat = static_cast<cuMatSp<T>*>(cl[i]);
			//std::cout << " mat i is sparse nnz: " << spmat->nnz << std::endl;
			//std::cout << " cusparse handle : " << spmat->handle << std::endl;
			//std::cout << " pts values, rowptr, colids, rfac data, out_ data: " << spmat->values<< " " <<  spmat->rowptr<< " " <<  spmat->colids<< " " <<  ite_rfac->data<< " " <<  ite_out->data  << std::endl;
#if (CUDA_VERSION <= 9200)
			m = cl[i]->nrows;
			k = cl[i]->ncols;
			if(i>1) // op(ite_rfac) == ite_rfac
				if(cusparse_op == CUSPARSE_OPERATION_NON_TRANSPOSE)
					ldb = k;
				else
					ldb = m;
			else
				ldb = n;
			cusparse_status = cusparseTcsrmm2(spmat->handle, cusparse_op, i==1?cusparse_op:CUSPARSE_OPERATION_NON_TRANSPOSE, m, n, k, spmat->nnz, i==nmats-1?&alpha:&alpha_one,
					spmat->descr /*cusparseMatDescr_t*/, spmat->values, spmat->rowptr, spmat->colids, ite_rfac->data, ldb, &beta, ite_out->data, k /* ldc always the same */);
#else
			// cusparse_op is never CUSPARSE_OPERATION_NON_TRANSPOSE
			// SpMM only handles spmat non-transpose, so apply the op before calling SpMM
			spmat = spmat->clone(); // no memory leak (deleted below)
			if(cusparse_op == CUSPARSE_OPERATION_TRANSPOSE) // equiv. to op == OP_NOTRANSP || op == OP_TRANSP
				spmat->transpose();
			else // op == OP_CONJTRANSP
				spmat->adjoint();
			if(i == 1 && cusparse_op == CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE)
			{
				// SpMM doesn't handle OP_CONJTRANSP, so apply the op before calling
				ite_rfac = ite_rfac->clone(); // not a memory leak, it's deleted below
				ite_rfac->adjoint();
			}
			cusparse_status = helper_cusparseSpMM(*spmat, *ite_rfac,
					CUSPARSE_OPERATION_NON_TRANSPOSE, i==1&&cusparse_op==CUSPARSE_OPERATION_TRANSPOSE?cusparse_op:CUSPARSE_OPERATION_NON_TRANSPOSE,
					i==nmats-1?alpha:alpha_one, beta,
					*ite_out,
					"cuMatArray<T>::chain_matmul_l2r");
			delete spmat;
			if(i == 1 && cusparse_op == CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE)
				delete ite_rfac;
#endif
			check_cusparse_error(cusparse_status, "chain_matmul helper_cusparseSpMM");
		}
		else if(cl[i]->is_bsr())
		{
			// cusparse_op is never CUSPARSE_OPERATION_NON_TRANSPOSE
			auto bsrmat = static_cast<cuMatBSR<T>*>(cl[i]);
			bsrmat->mul(*ite_rfac, ite_out, op==OP_NOTRANSP?OP_TRANSP:op,
					i==1&&op == OP_NOTRANSP?OP_TRANSP:op,
					i==nmats-1?&alpha:&alpha_one, &beta);
		}
		else
		{

			m = cl[i]->ncols;
			k = cl[i]->nrows;
//			std::cout << "loop i = "<< i << " m: " << m << " n:" << n << " k:" << k << std::endl;
			dsmat = static_cast<cuMatDs<T>*>(cl[i]);
			auto status = cublasTgemm(dsmat->handle, cublas_op, i==1?cublas_op:CUBLAS_OP_N, m, n, k, i==nmats-1?&alpha:&alpha_one, dsmat->data,
					k, ite_rfac->data, i==1?cl[0]->nrows:cl[i-1]->ncols, &beta, ite_out->data, m);
			check_cublas_error(status, "cuMatArray::chain_matmul_l2r cublasTgemm");
		}

		if(ite_out == tmp)
		{
			ite_out = out_;
			ite_rfac = tmp;
		}
		else
		{
			ite_out = tmp;
			ite_rfac = out_;
		}
	}
	//	if(nmats > 1)
	delete tmp;
	out_->nrows = (*(cl.end()-1))->ncols;
	out_->ncols = cl[0]->nrows;
//	std::cout << "end of chain_matmul_l2r out_: " << out_ << std::endl;
	if(op != OP_NOTRANSP)
		return out_;
	// do the extra op on the result (simpler than computing the whole product factor by factor)
	out_->apply_op(op);
	return out_;
}

template<typename T>
cuMatDs<T>* cuMatArray<T>::sliced_chain_matmul(int rslice_start, int rslice_size, int cslice_start, int cslice_size, T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out)
{
	// detect disabled slicing on both dimensions and rely directly on chain_matmul
	if((rslice_start == -1 || rslice_size == 0) && (cslice_start == -1 || cslice_size == 0))
		return chain_matmul(alpha, op, M, dsm_out);
	// compute a sliced cuMatArray based on this, then compute the chain_matmul
	cuMatArray<T> sliced_marr;
	for(auto f: this->cu_array)
		sliced_marr.cu_array.push_back(f);
	if(rslice_start != -1 && rslice_size > 0)
	{
		auto rslice_spmat = cuMatSp<T>::create_zero(rslice_size, nrows());
		rslice_spmat->set_eyes();
		sliced_marr.cu_array.insert(sliced_marr.cu_array.begin(), rslice_spmat);

	}
	if(cslice_start != -1 && cslice_size > 0)
	{
		auto cslice_spmat = cuMatSp<T>::create_zero(ncols(), cslice_size);
		cslice_spmat->set_eyes();
		sliced_marr.cu_array.push_back(cslice_spmat);
	}
	return sliced_marr.chain_matmul(alpha, op, M, dsm_out);
}

template<typename T>
cuMatDs<T>* cuMatArray<T>::indexed_chain_matmul(size_t *ids[2], size_t id_lens[2], T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out)
{
	// detect disabled indexing on both dimensions and rely directly on chain_matmul
	if((ids[0] == nullptr || id_lens[0] == 0) && (ids[1] == nullptr || id_lens[1] == 0))
		return chain_matmul(alpha, op, M, dsm_out);
	// compute an indexed cuMatArray based on this, then compute the chain_matmul
	cuMatArray<T> id_marr;
	for(auto f: this->cu_array)
		id_marr.cu_array.push_back(f);
	if(ids[0] != nullptr && id_lens[0] > 0)
	{
		auto rid_spmat = cuMatSp<T>::create_zero(id_lens[0], nrows());
		rid_spmat->set_col_ids_to_one(ids[0], id_lens[0]);
		id_marr.cu_array.insert(id_marr.cu_array.begin(), rid_spmat);

	}
	if(ids[1] != nullptr && id_lens[1] > 0)
	{
		auto cid_spmat = cuMatSp<T>::create_zero(ncols(), id_lens[1]);
		cid_spmat->set_row_ids_to_one(ids[1], id_lens[1]);
		id_marr.cu_array.push_back(cid_spmat);
	}
	return id_marr.chain_matmul(alpha, op, M, dsm_out);
}

template<typename T>
cuMatDs<T>* cuMatArray<T>::chain_matmul(T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out)
{
	auto cl = this;
	//TODO: overload to multiply a CPU matrix
	//TODO: functions to size, push, insert and erase directly through cuMatArray obj
	int32_t i;
	bool r2l = true;
	if(op != OP_NOTRANSP)
	{
		M->apply_op(op); // M (on GPU memory) is altered // but the change is reversed on the function end
		i = 0;
		// multiply from l2r
		r2l = false; //TODO: remove this trick when optimal chain order will be implemented ?
	}
	else
		i = cl->cu_array.size();
	cl->cu_array.insert(cl->cu_array.begin()+i, M);
	cuMatDs<T>* res_mat;
	if(r2l)
		res_mat = this->chain_matmul_r2l(alpha, op, dsm_out);
	else
		res_mat = this->chain_matmul_l2r(alpha, op, dsm_out);
//	std::cout << "res_mat=" << res_mat << std::endl;
	cl->cu_array.erase(cl->cu_array.begin()+i);
	if(op != OP_NOTRANSP)
	{
		// revert the op //TODO: reverting should be an option
		M->apply_op(op);
	}
	return res_mat;
}

template<typename T>
Real<T> cuMatArray<T>::spectral_norm(const float threshold, const int32_t max_iter)
{
	auto FHF = new cuMatArray();
	bool adjoint_first;
	// this function copies the pointers of "this" matrices into FHF
	auto add_mats = [&FHF, this]()
	{
		for(auto mat: this->cu_array)
			FHF->cu_array.push_back(mat);
	};
	// this functions add this adjoint matrices of this (in reverse order) ; it computes "this" adjoint
	auto add_adjoint_mats = [&FHF, this]()
	{
		cuMatSp<T>* sp_mat;
		cuMatDs<T>* ds_mat;
		cuMatBSR<T>* bsr_mat;
		cuMat<T>* any_mat;
		for(int i=this->cu_array.size()-1;i>=0;i--)
		{
			cuMat<T>* mat = this->cu_array[i];
			if(ds_mat = dynamic_cast<cuMatDs<T>*>(mat))
			{
				auto ds_mat_h = cuMatDs<T>::create(mat->nrows, mat->ncols);
				ds_mat->copy(ds_mat_h);
				ds_mat_h->adjoint();
				any_mat = ds_mat_h;
			}
			else if(sp_mat = dynamic_cast<cuMatSp<T>*>(mat))
			{
				auto sp_mat_h = cuMatSp<T>::create_zero(mat->nrows, mat->ncols);
				sp_mat->copy(sp_mat_h);
				sp_mat_h->adjoint();
				any_mat = sp_mat_h;
			}
			else if(bsr_mat = dynamic_cast<cuMatBSR<T>*>(mat))
			{
				auto bsr_mat_h = bsr_mat->clone();
				bsr_mat_h->adjoint();
				any_mat = bsr_mat_h;
			}
			else
			{
				throw std::runtime_error("Unknown matrix type.");
			}
			FHF->cu_array.push_back(any_mat);
		}
	};
	// this function role is to delete a number of this->size() contiguous matrices in FHF
	// starting from the matrice of index i (the adjoint matrices because they are copies)
	auto del_adjoint_mats = [&FHF, this](int i)
	{
		for(auto ite = FHF->cu_array.begin()+i;ite < FHF->cu_array.begin()+i+this->size(); ite++)
		{
			delete *ite;
		}
	};
	// build this'*this or this*this', the choice is made to target the min size
	if(this->nrows() < this->ncols())
	{
		add_mats();
		add_adjoint_mats();
		adjoint_first = false;
	}
	else
	{
		add_adjoint_mats();
		add_mats();
		adjoint_first = true;
	}
	// compute the largest eigenvalue of FHF
	auto lambda = FHF->power_iteration(threshold, max_iter);
	auto norm = std::abs(gm_sqrt<T, STL<T>>(lambda));
	// free manually the adjoint matrices (copies of this matrices)
	if(adjoint_first)
		del_adjoint_mats(0);
	else
		del_adjoint_mats(this->size());
	// don't free this matrices (pointer copies)
	FHF->del_mats = false;
	delete FHF;
	return norm;
}

template<typename T>
T cuMatArray<T>::power_iteration(const float threshold, const int32_t max_iter)
{
	int32_t xk_nrows;
	int32_t xk_buf_sz = 0;
	int32_t F_nrows = cu_array[0]->nrows, F_ncols = (*(cu_array.end()-1))->ncols;
	int32_t i = 0;
	T old_lambda, lambda, one, lambda_diff;
	T zero;
	xk_nrows = F_nrows;
	for(auto M: cu_array)
		xk_buf_sz = xk_buf_sz>=M->nrows?xk_buf_sz:M->nrows;
	cuMatDs<T> xk(xk_nrows, 1, xk_buf_sz, 1);
	cuMatDs<T> xk_norm(xk_nrows, 1, xk_buf_sz, 1);
	xk.setOnes();
	set_one(&one);
	bzero(&zero, sizeof(T));
	old_lambda = one;
	lambda = zero;
	lambda_diff = old_lambda;
	while((abs(lambda_diff) > abs(threshold) || abs(lambda) <= abs(threshold)) && i < max_iter)
	{
		old_lambda = lambda;
		xk.copy(&xk_norm);
		xk_norm.normalize();
		chain_matmul(one, OP_NOTRANSP, &xk_norm, &xk);
		dsm_dot(&xk, &xk_norm, &lambda);
		lambda_diff = ::sub(old_lambda, lambda);
		i++;
	}
	return lambda;
}

template<typename T>
size_t cuMatArray<T>::size()
{
	return this->cu_array.size();
}

template<typename T>
void cuMatArray<T>::insert(cuMat<T>* m, const int32_t id)
{
	if(! m->is_cuda())
		throw std::runtime_error("Can't add non-gpu matrix to cuMatArray.");
	cu_array.insert(cu_array.begin()+id, m);
}

template<typename T>
void cuMatArray<T>::remove(int32_t id)
{
	if(id > size() || id < 0)
		throw std::runtime_error("index out of bounds.");
	cu_array.erase(cu_array.begin()+id);
}

/********************* non-member functions */
template<typename T>
size_t marr_size(gm_MatArray_t array)
{
//	std::cout << "marr_size() array addr:" << array << std::endl;
	return static_cast<cuMatArray<T>*>(array)->size();
}

template<typename T>
gm_DenseMat_t marr_chain_matmul_r2l(gm_MatArray_t array, T alpha, gm_Op op)
{
	return static_cast<gm_DenseMat_t>(static_cast<cuMatArray<T>*>(array)->chain_matmul_r2l(alpha, op));
}

template<typename T>
gm_DenseMat_t marr_chain_matmul(gm_MatArray_t array, T alpha, gm_Op op, gm_DenseMat_t M, gm_DenseMat_t dsm_out /*= nullptr */)
{
	auto p = static_cast<cuMatArray<T>*>(array)->chain_matmul(alpha, op, static_cast<cuMatDs<T>*>(M), static_cast<cuMatDs<T>*>(dsm_out));
	return static_cast<gm_DenseMat_t>(p);
}

template<typename T>
gm_DenseMat_t marr_sliced_chain_matmul(gm_MatArray_t array, int rslice_start, int rslice_size, int cslice_start, int cslice_size, T alpha, gm_Op op, gm_DenseMat_t M, gm_DenseMat_t dsm_out /*= nullptr */)
{
	auto p = static_cast<cuMatArray<T>*>(array)->sliced_chain_matmul(rslice_start, rslice_size, cslice_start, cslice_size, alpha, op, static_cast<cuMatDs<T>*>(M), static_cast<cuMatDs<T>*>(dsm_out));
	return static_cast<gm_DenseMat_t>(p);
}

template<typename T>
gm_DenseMat_t marr_indexed_chain_matmul(gm_MatArray_t array, size_t *ids[2], size_t id_lens[2], T &alpha, gm_Op op, gm_DenseMat_t M, gm_DenseMat_t dsm_out /*= nullptr */)
{
	auto p = static_cast<cuMatArray<T>*>(array)->indexed_chain_matmul(ids, id_lens, alpha, op, static_cast<cuMatDs<T>*>(M), static_cast<cuMatDs<T>*>(dsm_out));
	return static_cast<gm_DenseMat_t>(p);
}

template<typename T>
gm_DenseMat_t marr_chain_matmul(gm_MatArray_t array, T alpha, gm_Op op, T* mat_data, int32_t nrows, int32_t ncols)
{
	auto mat = cuMatDs<T>::create(nrows, ncols, mat_data);
	auto mat_res = marr_chain_matmul(array, alpha, op, mat);
	delete mat;
	return mat_res;
}

template<typename T>
void marr_chain_matmul(gm_MatArray_t array, T alpha, gm_Op op, T* mat_data, int32_t nrows, int32_t ncols, T* data_out)
{
	//copy to gpu the cpu mat to multiply
	auto mat = cuMatDs<T>::create(nrows, ncols, mat_data);
	// multiply
	auto mat_res = marr_chain_matmul(array, alpha, op, mat);
	delete mat;
	//copy the result to cpu
	dsm_tocpu(mat_res, data_out);
	delete static_cast<cuMatDs<T>*>(mat_res);
}

template<typename T>
gm_MatArray_t marr_create()
{
	//std::cout << "marr_create()" << std::endl;
	return new cuMatArray<T>();
}

template<typename T>
void marr_free(gm_MatArray_t array, const bool del_mats)
{
	static_cast<cuMatArray<T>*>(array)->del_mats = del_mats;
	delete static_cast<cuMatArray<T>*>(array);
}

template<typename T>
void marr_add_gpu_spm(gm_MatArray_t array, gm_SparseMat_t mat)
{
//	std::cout << "marr_add_gpu_spm mat: " << mat << " array:" << array << std::endl;
	static_cast<cuMatArray<T>*>(array)->cu_array.push_back(static_cast<cuMatSp<T>*>(mat));
}

template<typename T>
void marr_add_gpu_dsm(gm_MatArray_t array, gm_DenseMat_t mat)
{
//	std::cout << "marr_add_gpu_dsm mat: " << mat <<  " array:" << array << std::endl;
	static_cast<cuMatArray<T>*>(array)->cu_array.push_back(static_cast<cuMatDs<T>*>(mat));
}

template<typename T>
void marr_add_gpu_anymat(gm_MatArray_t array, void* any_mat)
{
	cuMat<T>* mat = static_cast<cuMat<T>*>(any_mat);
	if(dynamic_cast<cuMatDs<T>*>(mat) == nullptr && dynamic_cast<cuMatSp<T>*>(mat) == nullptr &&  dynamic_cast<cuMatBSR<T>*>(mat) == nullptr)
	{
		throw std::runtime_error("The matrix to add must be a valid sparse (CSR or BSR) or a dense matrix but is not.");
	}
	static_cast<cuMatArray<T>*>(array)->cu_array.push_back(mat);
}

template<typename T>
void marr_insert(gm_MatArray_t array, cuMat<T>* mat, int32_t id)
{
//	std::cout << "marr_insert, id=" << id << " size: " << marr_size<T>(array) << std::endl;
	static_cast<cuMatArray<T>*>(array)->insert(static_cast<cuMat<T>*>(mat), id);
//	std::cout << "marr_insert end  size: " << marr_size<T>(array) << std::endl;
}

template<typename T>
void marr_remove(gm_MatArray_t array, int32_t id)
{
	static_cast<cuMatArray<T>*>(array)->remove(id);
}

template<typename T>
gm_SparseMat_t marr_togpu_spm(gm_MatArray_t array, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values)
{
	auto spmat = static_cast<gm_SparseMat_t>(cuMatSp<T>::create(nrows, ncols, values, row_ptr, col_inds, nnz));
	marr_add_gpu_spm<T>(array, spmat);
	return spmat;
}

template<typename T>
gm_DenseMat_t marr_togpu_dsm(gm_MatArray_t array, int32_t nrows, int32_t ncols, T* data)
{
	gm_DenseMat_t dmat = static_cast<gm_DenseMat_t>(cuMatDs<T>::create(nrows, ncols, data));
	marr_add_gpu_dsm<T>(array, dmat);
	return dmat;
}

template<typename T>
gm_SparseMat_t marr_replace_spm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values, int32_t id)
{
	//TODO: bool to decide to free or not the replaced mat (the callee is responsible)
	auto spmat = static_cast<gm_SparseMat_t>(cuMatSp<T>::create(nrows, ncols, values, row_ptr, col_inds, nnz));
	static_cast<cuMatArray<T>*>(array)->cu_array[id] = static_cast<cuMat<T>*>(spmat);
	return spmat;
}

template<typename T>
gm_DenseMat_t marr_replace_dsm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, T* data, int32_t id)
{
	//TODO: bool to decide to free or not the replaced mat
	gm_DenseMat_t dmat = static_cast<gm_DenseMat_t>(cuMatDs<T>::create(nrows, ncols, data));
	static_cast<cuMatArray<T>*>(array)->cu_array[id] = static_cast<cuMat<T>*>(dmat);
	return dmat;
}

template<typename T>
gm_SparseMat_t marr_set_spm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values, int32_t id)
{
//	std::cout << "marr_set_spm_at id:" << id <<  " array size: " << static_cast<cuMatArray<T>*>(array)->cu_array.size() << " static_cast<cuMatArray<T>*>(array)->cu_array[id]:" << static_cast<cuMatArray<T>*>(array)->cu_array[id] << " array:" << array <<  std::endl;
	// Contrary to dsm, spm buffers can change even if mat dims don't
	int32_t mat_sz[2] = {nrows, ncols};
	int32_t *dm_rowptr, *dm_colids;
	T *dm_values;
	cuMatSp<T>* mat;
	assert(marr_size<T>(array) > id);
//	std::cout << "marr_set_spm_at cur gpu mat is sparse: " << marr_is_mat_sparse<T>(array, id) << std::endl;
//	std::cout << "static_cast<cuMatArray<T>*>(array)->cu_array[id]" << std::endl;
	if(!(mat = dynamic_cast<cuMatSp<T>*>(static_cast<cuMatArray<T>*>(array)->cu_array[id]))) throw std::runtime_error("Error: a gpu sparse matrix is only assignable by a host sparse matrix.");
//	std::cout << "after dynamic_cast" << std::endl;
	if(nrows != mat->nrows || ncols != mat->ncols)
		throw std::runtime_error("Error: host matrix doesn't match gpu matrix dimensions.");
//	std::cout << "nnz:" << nnz << std::endl;
//	alloc_dbuf(nrows+1, &dm_rowptr); // nrows stays the same, no need to reallocate
	if(nnz != mat->nnz)
	{
		free_dbuf(mat->values);
		free_dbuf(mat->colids);
		alloc_dbuf(nnz, &dm_values);
		alloc_dbuf(nnz, &dm_colids);
	}
	else
	{
		dm_values = mat->values;
		dm_colids = mat->colids;
	}
	dm_rowptr = mat->rowptr;
//	std::cout << "copy_hbuf2dbuf(nnz, values, dm_values)" << std::endl;
	copy_hbuf2dbuf(nnz, values, dm_values);
//	std::cout << "copy_hbuf2dbuf(nrows+1, row_ptr, dm_rowptr)" << std::endl;
	copy_hbuf2dbuf(nrows+1, row_ptr, dm_rowptr);
//	std::cout << "copy_hbuf2dbuf(nnz, col_inds, dm_colids);" << std::endl;
	copy_hbuf2dbuf(nnz, col_inds, dm_colids);
	mat->values = dm_values;
	mat->rowptr = dm_rowptr;
	mat->colids = dm_colids;
	mat->nnz = nnz;
	return mat;
}

template<typename T>
gm_DenseMat_t marr_set_dsm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, T* data, int32_t id)
{
//	std::cout << "marr_set_dsm_at array addr: " << array <<  " size:" << marr_size<T>(array) << std::endl;
	cuMatDs<T>* mat;
//	std::cout << "marr_set_dsm_at cur gpu mat is sparse: " << marr_is_mat_sparse<T>(array, id) << std::endl;
	if(!(mat = dynamic_cast<cuMatDs<T>*>(static_cast<cuMatArray<T>*>(array)->cu_array[id]))) throw std::runtime_error("Error: a gpu dense matrix is only assignable by a host dense matrix.");
	if(nrows != mat->nrows || ncols != mat->ncols)
		throw std::runtime_error("Error: host matrix doesn't match gpu matrix dimensions.");
	copy_hbuf2dbuf(nrows*ncols, data, mat->data);
	return mat;
}

template<typename T>
bool marr_is_mat_sparse(gm_MatArray_t array, u_int32_t index)
{
	//std::cout << "marr_is_mat_sparse ==" << static_cast<cuMat<T>*>(marr_get_spm<T>(array, index))->is_csr() << " mat ptr: " << static_cast<cuMat<T>*>(marr_get_spm<T>(array, index)) << std::endl;
	return static_cast<cuMat<T>*>(marr_get_spm<T>(array, index))->is_csr();
}

template<typename T>
void marr_erase_at(gm_MatArray_t array, u_int32_t index, const bool free)
{
//	std::cout << "marr_erase_at, id=" << index << " size: " << marr_size<T>(array) << std::endl;
	std::vector<cuMat<T>*>& _cu_array = static_cast<cuMatArray<T>*>(array)->cu_array;
	if(free)
		delete *(_cu_array.begin()+index);
	_cu_array.erase(_cu_array.begin()+index);
//	std::cout << "marr_erase_at end size: " << marr_size<T>(array) << std::endl;
}

template<typename T>
void marr_tocpu_spm(gm_MatArray_t array, u_int32_t index, int32_t *nrows, int32_t *ncols, int32_t *nnz, int32_t *rowptr, int32_t *colids, T* values)
{
	spm_tocpu(marr_get_spm<T>(array, index), rowptr, colids, values);
}

template<typename T>
void marr_tocpu_dsm(gm_MatArray_t array, u_int32_t index, int32_t *nrows, int32_t *ncols, T * data)
{
	dsm_tocpu<T>(marr_get_dsm<T>(array, index), data);
}

template<typename T>
gm_SparseMat_t marr_get_spm(gm_MatArray_t array, u_int32_t index)
{
	//std::cout << "marr_get_spm" << std::endl;
	return static_cast<cuMatArray<T>*>(array)->cu_array[index];
}

template<typename T>
gm_DenseMat_t marr_get_dsm(gm_MatArray_t array, u_int32_t index)
{
	//std::cout << "marr_get_dsm" << std::endl;
	return static_cast<cuMatArray<T>*>(array)->cu_array[index];
}


