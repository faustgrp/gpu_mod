#include "proximity_ops.h"
#include "kernels.h"

template<>
void prox_pos<float>(float* data, int32_t dlen)
{
	kernel_positive_matrix(data, dlen);
}

template<>
void prox_pos<double>(double* data, int32_t dlen)
{
	kernel_positive_matrix(data, dlen);
}


template<>
void prox_pos<float2>(float2* data, int32_t dlen)
{
}

template<>
void prox_pos<double2>(double2* data, int32_t dlen)
{
}
