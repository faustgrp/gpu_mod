#ifndef __CU_MAT_BSR__
#define __CU_MAT_BSR__
#include "cuMatDs.h"
#include "cuMat.h"
#include "cuda_utils.h"
template<typename T>
struct cuMatBSR : cuMat<T>
{
	T* bdata;
	int32_t *bcolids;
	int32_t *browptr;
	cusparseMatDescr_t descr;
	int32_t bnnz;
	int32_t bnrows;
	int32_t bncols;
	int32_t nblocks_per_row_dim;
	int32_t nblocks_per_col_dim;
	static cusparseHandle_t handle;
	int32_t dev_id;
	void* stream;

	size_t get_nnz() const;
	void transpose();
	void adjoint();
	void conjugate();
	void mul(const T& scalar);
	Real<T> norm_frob() const;
	size_t get_nbytes() const;
	size_t get_bnrows() const;
	size_t get_bncols() const;
	size_t get_bnnz() const;
	void destroy();
	static void init_desc(cusparseMatDescr_t& desc);
	void set_zeros();


	cuMatBSR();
	cuMatBSR(int32_t nrows, int32_t ncols, int32_t bnrows, int32_t bncols, T* dm_bdata, int32_t* dm_browptr, int32_t* dm_bcolids, int32_t bnnz, cusparseMatDescr_t descr, int dev_id=-1, void* stream=nullptr);

	cuMatBSR(const cuMatBSR<T>&);
	cuMatBSR(cuMatBSR<T>&&);
	cuMatBSR<T>& operator=(const cuMatBSR<T>&);
	cuMatBSR<T>& operator=(cuMatBSR<T>&&);
	void copy_to(cuMatBSR<T>& dest) const;
	bool is_sparse() const {return true;}
	bool is_csr() const  {return false;}
	bool is_bsr() const  {return true;}
	bool is_dense() const  {return false;}
	~cuMatBSR();

	static cuMatBSR<T>* create(int32_t nrows, int32_t ncols, int32_t bnrows, int32_t bncols, const T* bdata, const int32_t* browptr, const int32_t* bcolids, int32_t bnnz, int32_t dev_id=-1, void* stream=nullptr);

	cuMatBSR<T>* clone(const int32_t dev_id=-1, const void* stream=nullptr);

	static cuMatBSR<T>* csr2bsr(cuMatSp<T>& mat_sp, int32_t bsize, int dev_id=-1, void* stream=nullptr);

	cuMatDs<T>* mul(cuMatDs<T> & other, cuMatDs<T>* output, const gm_Op op_this = OP_NOTRANSP, const gm_Op op_other = OP_NOTRANSP, 	const T* alpha = nullptr,
		const T* beta = nullptr);

	void bsr_get_info(gm_BSRMat_t cu_mat, int32_t *nrows, int32_t *ncols, int32_t *bnrows, int32_t *bncols, int32_t *bnnz);
 	void bsr_tocpu(gm_BSRMat_t cu_mat, int32_t *browptr, int32_t *bcolids, T* bdata, int32_t *nrows, int32_t *ncols, int32_t *bnrows, int32_t *bncols, int32_t *bnnz);
	void to_dense(cuMatDs<T>& out, gm_Op op = OP_NOTRANSP);
	cuMatDs<T> to_dense(gm_Op op = OP_NOTRANSP);
	static cuMatDs<T>* bsr2dense(cuMatBSR<T>& bsr, gm_Op op = OP_NOTRANSP);

	private:
	void copy_attrs(const cuMatBSR<T>& other, bool including_dev_id=true);

	void free_bufs();
};
#include "cuMatBSR.hpp"
#endif
