#ifndef __GPU_MOD_HMATDS__
#define __GPU_MOD_HMATDS__
#include "Mat.h"
template <typename T>
struct hMatDs : Mat
{
	T* data;
	hMatDs(int32_t, int32_t, T*);
	bool is_cuda() const {return false;}
	bool is_cpu() const {return true;}
	bool is_sparse() const {return false;}
	bool is_csr() const {return false;}
	bool is_bsr() const {return false;}
	bool is_dense() const {return true;};
	Scalar scalar_type() const {return type2Scalar<T>();}
};
#include "hMatDs.hpp"
#endif
