
template<typename T>
Scalar type2Scalar()
{
	if(std::is_floating_point<T>::value)
	{
		if(std::is_same<T, float>::value)
			return GM_FLOAT;
		else if(std::is_same<T, double>::value)
			return GM_DOUBLE;
		else
			throw std::runtime_error("type2Scalar unrecognized floating type.");
	}
	if(std::is_same<T, std::complex<float>>::value || std::is_same<T, cuComplex>::value)
		return GM_CPLX_FLOAT;
	else if(std::is_same<T, std::complex<double>>::value || std::is_same<T, cuDoubleComplex>::value)
		return GM_CPLX_DOUBLE;
	else
		throw std::runtime_error("type2Scalar unrecognized type.");
}
