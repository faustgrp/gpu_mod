#ifndef __GM_INTERF__
#define __GM_INTERF__
#include <cstdlib>
#include <iostream>
#include <cstdint>
#include <cstring>
#ifdef _MSC_VER
#include <windows.h>
#define u_int32_t uint32_t //TODO: should use uint32_t directly from cstdint
#define bzero(addr, bsize) (memset((addr), '\0', (bsize))) //TODO: consider moving to more appropriate header
#else
#include <dlfcn.h>
#endif
#include "gm_Op.h"


#ifdef __GM_LOADER__
// here is the case where user code needs headers to use the library (which should have been compiled already)
// this user code is named a GM loader, hence the variable
// the user code doesn't have to know anything about the cuda devkit (e.g. the cu*Complex types)
#include "gm_cuComplex.h"
#else
// here is the case where we compile the library that knows about the cuda devkit
#include "cuComplex.h"
#endif

#include "gm_interf_types.h"

#ifdef __GM_LOADER__
void* gm_load_lib(const char* libpath, const bool silent=false);

#ifdef _MSC_VER
#define check_dlsym_err(sym, func) \
	if(func == NULL)\
	{ \
		error = "error loading symbol."; \
		std::cerr << "error code: " << GetLastError() << std::endl; \
		std::cerr << "symbol to load: " << sym << std::endl; \
		exit(1); \
	}
#else
#define check_dlsym_err(sym, func) \
    if ((error = dlerror()) != NULL)  { \
        std::cerr << error << std::endl; \
        std::cerr << "symbol to load: " << sym << std::endl; \
        exit(1); \
    }
#endif

void* gm_load_func_check_err(const void *gm_handle, const char* fn);
void gm_close_lib(void* handle);



#include "gm_interf_GenPurposeFuncs.h"
#include "gm_interf.hpp"
#endif
#include "gm_interf_float.h"
#include "gm_interf_double.h"
#include "gm_interf_cuComplex.h"
#include "gm_interf_cuDoubleComplex.h"


#endif
