#ifndef __GM_INTERF_TYPES__
#define __GM_INTERF_TYPES__
typedef void* gm_SparseMat_t;
typedef void* gm_DenseMat_t;
typedef void* gm_MatArray_t;
typedef void* gm_BSRMat_t;
#include "gm_Op.h"
#endif
