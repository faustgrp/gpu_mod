template<typename T>
hMatSp<T>::hMatSp(int32_t nrows, int32_t ncols, T* values, int32_t *rowptr, int32_t* colids, int32_t nnz) : Mat::Mat(nrows, ncols), values(values), rowptr(rowptr), colids(colids), nnz(nnz)
{
}
