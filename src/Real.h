#ifndef __REAL__
#define __REAL__
#include <complex>
#include <string>

template<typename T>
struct _Real
{
	using value_type = T;
};

template<typename U>
using Real = typename _Real<U>::value_type;

template<typename T>
Real<T> real(const T& val);

template<typename T>
std::string to_string (const T& val);

#include "Real.hpp"
#endif
