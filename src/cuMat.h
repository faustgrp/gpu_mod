#ifndef __CU_MAT__
#define __CU_MAT__
#include "Mat.h"
/**
 *	Cuda matrix class.
 */
template<typename T>
struct cuMat : Mat
{
	virtual size_t get_nnz() const=0;
	cuMat(int32_t, int32_t);
	cuMat() {};
	virtual void transpose()=0;
	virtual void mul(const T& scalar)=0;
	virtual size_t get_nbytes() const =0;
	virtual ~cuMat(){}
	bool is_cuda() const {return true;}
	bool is_cpu() const {return false;}
	Scalar scalar_type() const {return type2Scalar<T>();}
};
#include "cuMat.hpp"
#endif
