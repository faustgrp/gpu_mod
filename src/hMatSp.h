#ifndef __GPU_MOD_HMATSP__
#define __GPU_MOD_HMATSP__
#include "Mat.h"
template <typename T>
struct hMatSp : Mat
{
	T* values;
	int32_t* rowptr;
	int32_t* colids;
	int32_t nnz;
	hMatSp(int32_t, int32_t, T*, int32_t*, int32_t*, int32_t);
	bool is_cuda() const {return false;}
	bool is_cpu() const {return true;}
	bool is_sparse() const {return true;}
	bool is_csr() const {return true;}
	bool is_bsr() const {return false;}
	bool is_dense() const {return false;};
	Scalar scalar_type() const {return type2Scalar<T>();}
};
#include "hMatSp.hpp"
#endif
