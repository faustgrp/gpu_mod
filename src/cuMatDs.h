#ifndef __GPU_MOD_CUMATDS__
#define __GPU_MOD_CUMATDS__
#include "gm_interf.h"
#include "Real.h"
#include "STL_scalar_types.h"
#include <cublas_v2.h>
#include <cusparse.h>
#include "cuda_utils.h"
#include "gm_Op.h"
#include "gm_Op_conv.h"
#include "hMatDs.h"
#include "cuMat.h"
#include "hMatDs.h"
#include "cuMatSp.h"
#include "kernels.h"
#include "faust_reduce_gpu.h"
#include "proximity_ops.h" // prox_sp, etc.
#define NOMINMAX // avoids VS min/max issue with std::min/max.
#include <algorithm>

//TODO: new type cuMat -[> Mat to derive from in cuMatDs/Sp

template <typename T>
struct cuMatDs : cuMat<T>
{
	//TODO: encapsulation
	T* data;
	int32_t buf_nrows;
	int32_t buf_ncols; // in GPU memory
	int32_t dev_id;
	void* stream;
	static cublasHandle_t handle;
	cuMatDs(int32_t, int32_t, T*, int32_t buf_nrows=-1, int32_t buf_ncols=-1);
	cuMatDs(int32_t nrows, int32_t ncols, int32_t buf_nrows=-1, int32_t buf_ncols=-1, T* data=nullptr, int32_t dev_id=-1);
	cuMatDs();
	cuMatDs(cuMatDs<T> &);
	cuMatDs(cuMatDs<T> &&);
	cuMatDs<T>& operator=(const cuMatDs<T>&);
	cuMatDs<T>& operator=(const cuMatDs<T>&&);
	void copy_attrs(const cuMatDs<T>& other, bool including_dev_id=true);
	void add(cuMatDs<T>&);
	void add(cuMatDs<T>&, const T&);
	void add(cuMatSp<T>&);
	void add(hMatDs<T>&);
	void add(T* data, int32_t nrows, int32_t ncols);
	void add(hMatSp<T>&);
	void add(int32_t nrows, int32_t ncols, int32_t nnz, const int32_t* row_ptr, const int32_t* col_inds, const T* values);
	void sub(cuMatDs<T>&);
	void sub(cuMatSp<T>&);
	void sub(hMatDs<T>&);
	void sub(T* data, int32_t nrows, int32_t ncols);
	void sub(hMatSp<T>&);
	void sub(int32_t nrows, int32_t ncols, int32_t nnz, const int32_t* row_ptr, const int32_t* col_inds, const T* values);
	cuMatDs<T>* mul(cuMatDs<T>&, cuMatDs<T>* output = nullptr, gm_Op op_this = OP_NOTRANSP, gm_Op op_other = OP_NOTRANSP);
	void mul(cuMatDs<T>&, hMatDs<T>& output, gm_Op op_this = OP_NOTRANSP, gm_Op op_other = OP_NOTRANSP);
	void mul(cuMatDs<T>&, T* output, gm_Op op_this = OP_NOTRANSP, gm_Op op_other = OP_NOTRANSP);
	cuMatDs<T>* mul(cuMatSp<T>&, cuMatDs<T>* output = nullptr, gm_Op op_this = OP_NOTRANSP, gm_Op op_other = OP_NOTRANSP);
	void mul(const T& scalar);
	void elt_wise_mul(const cuMatDs<T>&, const int32_t *ids = nullptr);
	void elt_wise_div(const cuMatDs<T>&);
	void apply_op(const gm_Op op);
	static cuMatDs<T>* apply_op(const cuMatDs<T>* in, gm_Op op, cuMatDs<T>* out = nullptr);
	void transpose();
	void adjoint();
	void conjugate();
	/**
	 * Normalize the matrix according to its Frobenius norm.
	 */
	void normalize();
	/**
	 * Computes the Frobenius norm.
	 */
	Real<T> norm_frob();
	/**
	 *
	 */
	Real<T> norm_l1();
	/**
	 * Peforms the power iteration algorithm on this (greatest eigen value).
	 * This should be definite positive.
	 */
	T power_iteration(const float threshold, const int32_t max_iter);
	/**
	 * Computes the spectral norm (l2 operator norm) on this.
	 */
	Real<T> spectral_norm(const float threshold, const int32_t max_iter);
	void setOnes();
	void set_zeros();
	void set_eyes();
	void set_val(const T& val);
	/**
	 * Edit the GPU matrix by specifying the new data in CPU buffer.
	 */
	void cpu_set(const T* data, int32_t nrows, int32_t ncols);
	cuMatDs<T>* clone(int32_t dev=-1);
	void copy(cuMatDs<T>*) const;
	void resize(const int32_t nrows, const int32_t ncols);
	T trace();
	T coeff_max();
	T coeff_min();
	T coeff(const int32_t i, const int32_t j) const;
	void coeff(const int32_t i, const int32_t j, T& c) const;
	void set_coeff(const int32_t i, const int32_t j, const T & c) const;
	T sum();
	T mean();
	T mean_relerr(const cuMatDs<T>&);
	void abs();
	void destroy();
	void set_stream(const void* stream);
	void mv_to_gpu(int32_t dev_id);
	size_t get_nnz() const;
	size_t get_nbytes() const;
	size_t numel() const;
	void real(cuMatDs<Real<T>>& out_real);
	bool is_sparse() const {return false;}
	bool is_csr() const  {return false;}
	bool is_bsr() const  {return false;}
	bool is_dense() const  {return true;}

	~cuMatDs();
	static cuMatDs<T>* create(hMatDs<T> &, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatDs<T>* create(int32_t, int32_t, int32_t buf_nrows=-1, int32_t buf_ncols=-1, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatDs<T>* create(int32_t, int32_t, T*, int32_t buf_nrows=-1, int32_t buf_ncols=-1, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatDs<T>* create(cuMatSp<T>& sp_mat, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatDs<T>* create(hMatSp<T>&, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatDs<T>* createEye(int32_t, int32_t, int32_t buf_nrows=-1, int32_t buf_ncols=-1, int32_t dev_id=-1, void* stream=nullptr);
	static void destroy(cuMatDs<T>*);
	static void set_buf_nrows_ncols(int32_t &buf_nrows, int32_t &buf_ncols, const int32_t nrows, const int32_t ncols, const std::string caller = "");
};

/********************* non-member functions */
template<typename T>
void dsm_dot(cuMatDs<T> x, cuMatDs<T> y, T* alpha);

template<typename T>
gm_DenseMat_t dsm_create(int32_t nrows, int32_t ncols, T* data, int32_t buf_nrows=-1, int32_t buf_ncols=-1, const void* stream=nullptr);

template<typename T>
gm_DenseMat_t dsm_create(int32_t nrows, int32_t ncols, int32_t buf_nrows=-1, int32_t buf_ncols=-1);

template<typename T>
void dsm_tocpu(gm_DenseMat_t cu_mat, int32_t *nrows, int32_t *ncols, T* data);

/**
 * Copies cu_mat buffer according to the offset and size arguments (wrt to T type) into the CPU buffer data.
 */
template<typename T>
void dsm_tocpu(gm_DenseMat_t cu_mat, T* data, uint32_t offset=0, int32_t size=-1);

template<typename T>
void dsm_get_info(gm_DenseMat_t cu_mat, int32_t *nrows, int32_t *ncols);


template<typename T>
void dsm_gemm(const gm_DenseMat_t vA, const gm_DenseMat_t vB, gm_DenseMat_t vC, const T &alpha, const T &beta, const gm_Op op_A, const gm_Op op_B);

template<typename T>
void dsm_gemm(const cuMatDs<T>* A, const cuMatDs<T>* B, cuMatDs<T>* C, const T &alpha, const T &beta, const gm_Op op_A, const gm_Op op_B);

#if (CUDA_VERSION > 9200)
/**
 * This function creates a CUSPARSE dense matrix descriptor (cusparseDnMatDescr_t) corresponding to a cuMatDs.
 *
 * \warn: the descriptor must be freed after use calling the function cusparseDestroyDnMat.
 */
template<typename T>
cusparseStatus_t dsm_mat2desc(const cuMatDs<T>& A, cusparseDnMatDescr_t& dnMatDescr);

#endif

template<typename T>
void dsm_butterfly_diag_prod(cuMatDs<T>& X, cuMatDs<T>& D1, cuMatDs<T>& D2, const int32_t *ids);

/**
 *  Computes the SVD of a batch of matrices As.
 *
 *  Warning: CUDA limits the batched SVD to matrices of size at most 32 x 32.
 *
 *  \param As: an horizontal concatenation of matrices to compute the SVD of.
 *  \param nbatches: the size of the batch, i.e. the number of matrices in As.
 *  \param Us: the left singular vectors of each SVD concatened horizontally.
 *  \param Vs: the right singular vectors of each SVD concatened horizontally.
 *  \param Ss: the singular values of each SVD concatened horizontally.
 *  \param rank: the approximation rank asked for the SVD, the size of Us, Vs, and Ss must be consistent with the rank asked.
 *  For example if rank == 1, just one singular vector per matrix of As is returned in Us and Vs and a single singular value is returned in Ss.
 */
template<typename T>
void dsm_batched_svd(const cuMatDs<T>& As, const uint32_t nbatches, cuMatDs<T>& Us, cuMatDs<T>& Vs, cuMatDs<Real<T>>& Ss, const uint32_t rank);

#include "cuMatDs.hpp"
#endif
