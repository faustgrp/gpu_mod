#include "cuda_utils.h"

using namespace std;

map<void*,size_t> MemoryMonitor::memory;
size_t MemoryMonitor::alloc_memory = 0;

#ifndef _MSC_VER
template<>
bool operator==(const double2& a, const double2& b)
{
	return a.x == b.x && a.y == a.y;
}

template<>
bool operator==(const float2& a, const float2& b)
{
	return a.x == b.x && a.y == a.y;
}
#endif

template<>
bool is_one(const float2& a)
{
	return a.x == 1.f && a.y == 0.f;
}

template<>
bool is_one(const double2& a)
{
	return a.x == 1. && a.y == 0.;
}

template<>
bool is_one(const float& a)
{
	return a == 1.f;
}

template<>
bool is_one(const double& a)
{
	return a == 1.;
}

template<>
double2 operator/(const double2 &c, const double& d)
{
	double2 r;
	r.x = c.x/d;
	r.y = c.y/d;
	return r;
}

template<>
float2 operator/(const float2& c, const float& d)
{
	float2 r;
	r.x = c.x/d;
	r.y = c.y/d;
	return r;
}

	template<>
double inv_scal(double scal)
{
	return 1./scal;
}

template<>
float inv_scal(float scal)
{
	return (float)1/scal;
}

template<>
float sub(float a, float b)
{
	return a-b;
}

template<>
double sub(double a, double b)
{
	return a-b;
}

template<>
float2 sub(float2 a, float2 b)
{
	float2 res;
	res.x = a.x-b.x;
	res.y = a.y-b.y;
	return res;
}

template<>
double2 sub(double2 a, double2 b)
{
	double2 res;
	res.x = a.x-b.x;
	res.y = a.y-b.y;
	return res;
}

template<>
cuComplex inv_scal(cuComplex scal)
{
	cuComplex inv;
	float x2 = scal.x*scal.x;
	float y2 = scal.y*scal.y;
	inv.x = scal.x / (x2 + y2);
	inv.y = scal.y / (x2 + y2);
	return inv;
}

template<>
cuDoubleComplex inv_scal(cuDoubleComplex scal)
{
	cuDoubleComplex inv;
	double x2 = scal.x*scal.x;
	double y2 = scal.y*scal.y;
	inv.x = scal.x / (x2 + y2);
	inv.y = scal.y / (x2 + y2);
	return inv;
}

	template<>
double minus_scal(double scal)
{
	return - scal;
}

template<>
float minus_scal(float scal)
{
	return - scal;
}

template<>
cuComplex minus_scal(cuComplex scal)
{
	cuComplex opp_scal;
	opp_scal.x = - scal.x;
	opp_scal.y = - scal.y;
	return opp_scal;
}

template<>
cuDoubleComplex minus_scal(cuDoubleComplex scal)
{
	cuDoubleComplex opp_scal;
	opp_scal.x = - scal.x;
	opp_scal.y = - scal.y;
	return opp_scal;
}

	template<>
void set_one<float>(float* scal)
{
	*scal = 1.f;
}

template<>
void set_one<double>(double* scal)
{
	*scal = 1.;
}

template<>
void set_one<cuComplex>(cuComplex* scal)
{
	scal->x = 1.f;
	scal->y = 0;
}

template<>
void set_one<cuDoubleComplex>(cuDoubleComplex* scal)
{
	scal->x = 1.;
	scal->y = 0;
}

template<>
cublasStatus_t cublasTscal2(cublasHandle_t handle, int n,
		const float *alpha, cuComplex *x, int incx)
{
	cuComplex calpha;
	calpha.x = *alpha;
	calpha.y = 0;
	return cublasTscal(handle, n, &calpha, x, incx);
}

template<>
cublasStatus_t cublasTscal2(cublasHandle_t handle, int n,
		const double *alpha, cuDoubleComplex *x, int incx)
{
	cuDoubleComplex calpha;
	calpha.x = *alpha;
	calpha.y = 0;
	return cublasTscal(handle, n, &calpha, x, incx);
}

template<>
cublasStatus_t cublasTscal2(cublasHandle_t handle, int n,
		const double *alpha, double *x, int incx)
{
	return cublasTscal(handle, n, alpha, x, incx);
}

	template<>
cublasStatus_t cublasTscal2(cublasHandle_t handle, int n,
		const float *alpha, float *x, int incx)
{
	return cublasTscal(handle, n, alpha, x, incx);
}

	template<>
float abs(float& a)
{
	return std::abs(a);
}

	template<>
double abs(double& a)
{
	return std::abs(a);
}

	template<>
float abs(float2& a)
{
	return std::sqrt(a.x*a.x+a.y*a.y);
}

	template<>
double abs(double2& a)
{
	return std::sqrt(a.x*a.x+a.y*a.y);
}

template<>
double gm_sqrt<double,double>(double& a)
{
	return sqrt(a);
}

template<>
float gm_sqrt<float,float>(float& a)
{
	return sqrt(a);
}

template<>
complex<float> gm_sqrt<float2,complex<float>>(float2& a)
{
	complex<float> a_(a.x, a.y);
	return sqrt(a_);
}

template<>
complex<double> gm_sqrt<double2,complex<double>>(double2& a)
{
	complex<double> a_(a.x, a.y);
	return sqrt(a_);
}

void set_dev(int32_t dev_id)
{
	if(dev_id != -1)
	{
		// secure device change by verifying the id
		if(!is_valid_dev_id(dev_id))
			//invalid dev_id //TODO: warning message or exception
			return;
		auto ret = cudaSetDevice(dev_id);
		if(ret != cudaSuccess)
			throw std::runtime_error(std::string("!!!! cudaSetDevice error: ")+cuda_error_int2str(ret));
	}
	// -1 means stay on the same device
}

bool is_valid_dev_id(int32_t dev_id)
{
	auto ndevs = dev_count();
	return dev_id < ndevs && dev_id >= 0;
}

int32_t cur_dev()
{
	int32_t dev_id;
	auto ret = cudaGetDevice(&dev_id);
	if(ret != cudaSuccess)
		throw std::runtime_error(std::string("!!!! cudaGetDevice error: ")+cuda_error_int2str(ret));
	return dev_id;
}

int32_t dev_count()
{
	int32_t count;
	auto ret = cudaGetDeviceCount(&count);
	if(ret != cudaSuccess)
		throw std::runtime_error(std::string("!!!! cudaGetDeviceCount error: ")+cuda_error_int2str(ret));
	return count;
}

std::function<void()> switch_dev(int32_t dev_id)
{
	auto cur_dev_id = cur_dev();
	if(dev_id != cur_dev_id && dev_id != -1)
	{
		set_dev(dev_id);
		auto cb = [=](){set_dev(cur_dev_id);};
		return cb;
	}
	else
	{
		return [](){};
	}
}

void* create_stream()
{
	cudaStream_t stream;
	cudaStreamCreate(&stream);
	return stream;
}

string cublas_error_int2str(cublasStatus_t status)
{
	std::string status_str;
	switch(status)
	{
		//cf. cublas_api.h
		case CUBLAS_STATUS_SUCCESS: /* 0 */
			status_str = "CUBLAS_STATUS_SUCCESS";
			break;
		case CUBLAS_STATUS_NOT_INITIALIZED: /* 1 */
			status_str = "CUBLAS_STATUS_NOT_INITIALIZED";
			break;
		case 2:
		case 4:
		case 5:
		case 6:
		case 9:
		case 10:
		case 12:
			status_str = "cuBLAS undocumented error.";
			break;
		case CUBLAS_STATUS_ALLOC_FAILED: /* 3 */
			status_str = "CUBLAS_STATUS_ALLOC_FAILED";
			break;
		case CUBLAS_STATUS_INVALID_VALUE: /* 7 */
			status_str = "CUBLAS_STATUS_INVALID_VALUE";
			break;
		case CUBLAS_STATUS_ARCH_MISMATCH: /* 8 */
			status_str = "CUBLAS_STATUS_ARCH_MISMATCH";
			break;
		case CUBLAS_STATUS_MAPPING_ERROR: /* 11 */
			status_str = "CUBLAS_STATUS_MAPPING_ERROR";
			break;
		case CUBLAS_STATUS_EXECUTION_FAILED: /* 13 */
			status_str = "CUBLAS_STATUS_EXECUTION_FAILED";
			break;
		case CUBLAS_STATUS_INTERNAL_ERROR: /* 14 */
			status_str = "CUBLAS_STATUS_INTERNAL_ERROR";
			break;
		case CUBLAS_STATUS_NOT_SUPPORTED: /* 15 */
			status_str = "CUBLAS_STATUS_NOT_SUPPORTED";
			break;
		case CUBLAS_STATUS_LICENSE_ERROR: /* 16 */
			status_str = "CUBLAS_STATUS_LICENSE_ERROR";
			break;
	}
	return status_str;
}

string cusparse_error_int2str(int32_t status)
{
	std::string status_str;
	switch(status)
	{
		case CUSPARSE_STATUS_SUCCESS: /* 0 */
			status_str = "CUSPARSE_STATUS_SUCCESS";
			break;
		case CUSPARSE_STATUS_NOT_INITIALIZED: /* 1 */
			status_str = "CUSPARSE_STATUS_NOT_INITIALIZED";
			break;
		case CUSPARSE_STATUS_ALLOC_FAILED: /* 2 */
			status_str = "CUSPARSE_STATUS_ALLOC_FAILED";
			break;
		case CUSPARSE_STATUS_INVALID_VALUE: /* 3 */
			status_str = "CUSPARSE_STATUS_INVALID_VALUE";
			break;
		case CUSPARSE_STATUS_ARCH_MISMATCH: /* 4 */
			status_str = "CUSPARSE_STATUS_ARCH_MISMATCH";
			break;
		case CUSPARSE_STATUS_MAPPING_ERROR: /* 5 */
			status_str = "CUSPARSE_STATUS_MAPPING_ERROR";
			break;
		case CUSPARSE_STATUS_EXECUTION_FAILED: /* 6 */
			status_str = "CUSPARSE_STATUS_EXECUTION_FAILED";
			break;
		case CUSPARSE_STATUS_INTERNAL_ERROR: /* 7 */
			status_str = "CUSPARSE_STATUS_INTERNAL_ERROR";
			break;
		case CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED: /* 8 */
			status_str = "CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED";
			break;
		case CUSPARSE_STATUS_ZERO_PIVOT: /* 9 */
			status_str = "CUSPARSE_STATUS_ZERO_PIVOT";
			break;
#if (CUDA_VERSION >= 11000)
		case CUSPARSE_STATUS_NOT_SUPPORTED: /* 10 */
			status_str = "CUSPARSE_STATUS_NOT_SUPPORTED";
			break;
		case CUSPARSE_STATUS_INSUFFICIENT_RESOURCES: /* 11 */
			status_str = "CUSPARSE_STATUS_INSUFFICIENT_RESOURCES";
			break;
#endif
		default:
			status_str = "Not a valid cuSparse error";
			break;
	}
	return status_str;
}

string cuda_error_int2str(int32_t status)
{
	// all the ids come from cuda-<version>/include/driver_types.h
	// (NOTE: this file is by the way a good doc for the errors)
	std::string status_str;
	switch(status)
	{
		case cudaSuccess: /* 0 */
			status_str = "cudaSuccess";
			break;

		case cudaErrorMissingConfiguration: /* 1 */
			status_str = "cudaErrorMissingConfiguration";
			break;

		case cudaErrorMemoryAllocation: /* 2 */
			status_str = "cudaErrorMemoryAllocation";
			break;

		case cudaErrorInitializationError: /* 3 */
			status_str = "cudaErrorInitializationError";
			break;

		case cudaErrorLaunchFailure: /* 4 */
			status_str = "cudaErrorLaunchFailure";
			break;

		case cudaErrorPriorLaunchFailure: /* 5 */
			status_str = "cudaErrorPriorLaunchFailure";
			break;

		case cudaErrorLaunchTimeout: /* 6 */
			status_str = "cudaErrorLaunchTimeout";
			break;

		case cudaErrorLaunchOutOfResources: /* 7 */
			status_str = "cudaErrorLaunchOutOfResources";
			break;

		case cudaErrorInvalidDeviceFunction: /* 8 */
			status_str = "cudaErrorInvalidDeviceFunction";
			break;

		case cudaErrorInvalidConfiguration: /* 9 */
			status_str = "cudaErrorInvalidConfiguration";
			break;

		case cudaErrorInvalidDevice: /* 10 */
			status_str = "cudaErrorInvalidDevice";
			break;

		case cudaErrorInvalidValue: /* 11 */
			status_str = "cudaErrorInvalidValue";
			break;

		case cudaErrorInvalidPitchValue: /* 12 */
			status_str = "cudaErrorInvalidPitchValue";
			break;

		case cudaErrorInvalidSymbol: /* 13 */
			status_str = "cudaErrorInvalidSymbol";
			break;

		case cudaErrorMapBufferObjectFailed: /* 14 */
			status_str = "cudaErrorMapBufferObjectFailed";
			break;

		case cudaErrorUnmapBufferObjectFailed: /* 15 */
			status_str = "cudaErrorUnmapBufferObjectFailed";
			break;

		case cudaErrorInvalidHostPointer: /* 16 */
			status_str = "cudaErrorInvalidHostPointer";
			break;

		case cudaErrorInvalidDevicePointer: /* 17 */
			status_str = "cudaErrorInvalidDevicePointer";
			break;

		case cudaErrorInvalidTexture: /* 18 */
			status_str = "cudaErrorInvalidTexture";
			break;

		case cudaErrorInvalidTextureBinding: /* 19 */
			status_str = "cudaErrorInvalidTextureBinding";
			break;

		case cudaErrorInvalidChannelDescriptor: /* 20 */
			status_str = "cudaErrorInvalidChannelDescriptor";
			break;

		case cudaErrorInvalidMemcpyDirection: /* 21 */
			status_str = "cudaErrorInvalidMemcpyDirection";
			break;

		case cudaErrorAddressOfConstant: /* 22 */
			status_str = "cudaErrorAddressOfConstant";
			break;

		case cudaErrorTextureFetchFailed: /* 23 */
			status_str = "cudaErrorTextureFetchFailed";
			break;

		case cudaErrorTextureNotBound: /* 24 */
			status_str = "cudaErrorTextureNotBound";
			break;

		case cudaErrorSynchronizationError: /* 25 */
			status_str = "cudaErrorSynchronizationError";
			break;

		case cudaErrorInvalidFilterSetting: /* 26 */
			status_str = "cudaErrorInvalidFilterSetting";
			break;

		case cudaErrorInvalidNormSetting: /* 27 */
			status_str = "cudaErrorInvalidNormSetting";
			break;

		case cudaErrorMixedDeviceExecution: /* 28 */
			status_str = "cudaErrorMixedDeviceExecution";
			break;

		case cudaErrorCudartUnloading: /* 29 */
			status_str = "cudaErrorCudartUnloading";
			break;

		case cudaErrorUnknown: /* 30 */
			status_str = "cudaErrorUnknown";
			break;

		case cudaErrorNotYetImplemented: /* 31 */
			status_str = "cudaErrorNotYetImplemented";
			break;

		case cudaErrorMemoryValueTooLarge: /* 32 */
			status_str = "cudaErrorMemoryValueTooLarge";
			break;

		case cudaErrorInvalidResourceHandle: /* 33 */
			status_str = "cudaErrorInvalidResourceHandle";
			break;

		case cudaErrorNotReady: /* 34 */
			status_str = "cudaErrorNotReady";
			break;

		case cudaErrorInsufficientDriver: /* 35 */
			status_str = "cudaErrorInsufficientDriver";
			break;

		case cudaErrorSetOnActiveProcess: /* 36 */
			status_str = "cudaErrorSetOnActiveProcess";
			break;

		case cudaErrorInvalidSurface: /* 37 */
			status_str = "cudaErrorInvalidSurface";
			break;

		case cudaErrorNoDevice: /* 38 */
			status_str = "cudaErrorNoDevice";
			break;

		case cudaErrorECCUncorrectable: /* 39 */
			status_str = "cudaErrorECCUncorrectable";
			break;

		case cudaErrorSharedObjectSymbolNotFound: /* 40 */
			status_str = "cudaErrorSharedObjectSymbolNotFound";
			break;

		case cudaErrorSharedObjectInitFailed: /* 41 */
			status_str = "cudaErrorSharedObjectInitFailed";
			break;

		case cudaErrorUnsupportedLimit: /* 42 */
			status_str = "cudaErrorUnsupportedLimit";
			break;

		case cudaErrorDuplicateVariableName: /* 43 */
			status_str = "cudaErrorDuplicateVariableName";
			break;

		case cudaErrorDuplicateTextureName: /* 44 */
			status_str = "cudaErrorDuplicateTextureName";
			break;

		case cudaErrorDuplicateSurfaceName: /* 45 */
			status_str = "cudaErrorDuplicateSurfaceName";
			break;

		case cudaErrorDevicesUnavailable: /* 46 */
			status_str = "cudaErrorDevicesUnavailable";
			break;

		case cudaErrorInvalidKernelImage: /* 47 */
			status_str = "cudaErrorInvalidKernelImage";
			break;

		case cudaErrorNoKernelImageForDevice: /* 48 */
			status_str = "cudaErrorNoKernelImageForDevice";
			break;

		case cudaErrorIncompatibleDriverContext: /* 49 */
			status_str = "cudaErrorIncompatibleDriverContext";
			break;

		case cudaErrorPeerAccessAlreadyEnabled: /* 50 */
			status_str = "cudaErrorPeerAccessAlreadyEnabled";
			break;

		case cudaErrorPeerAccessNotEnabled: /* 51 */
			status_str = "cudaErrorPeerAccessNotEnabled";
			break;

		case cudaErrorDeviceAlreadyInUse: /* 54 */
			status_str = "cudaErrorDeviceAlreadyInUse";
			break;

		case cudaErrorProfilerDisabled: /* 55 */
			status_str = "cudaErrorProfilerDisabled";
			break;

		case cudaErrorProfilerNotInitialized: /* 56 */
			status_str = "cudaErrorProfilerNotInitialized";
			break;

		case cudaErrorProfilerAlreadyStarted: /* 57 */
			status_str = "cudaErrorProfilerAlreadyStarted";
			break;

		case cudaErrorProfilerAlreadyStopped: /* 58 */
			status_str = "cudaErrorProfilerAlreadyStopped";
			break;

		case cudaErrorAssert: /* 59 */
			status_str = "cudaErrorAssert";
			break;

		case cudaErrorTooManyPeers: /* 60 */
			status_str = "cudaErrorTooManyPeers";
			break;

		case cudaErrorHostMemoryAlreadyRegistered: /* 61 */
			status_str = "cudaErrorHostMemoryAlreadyRegistered";
			break;

		case cudaErrorHostMemoryNotRegistered: /* 62 */
			status_str = "cudaErrorHostMemoryNotRegistered";
			break;

		case cudaErrorOperatingSystem: /* 63 */
			status_str = "cudaErrorOperatingSystem";
			break;

		case cudaErrorPeerAccessUnsupported: /* 64 */
			status_str = "cudaErrorPeerAccessUnsupported";
			break;

		case cudaErrorLaunchMaxDepthExceeded: /* 65 */
			status_str = "cudaErrorLaunchMaxDepthExceeded";
			break;

		case cudaErrorLaunchFileScopedTex: /* 66 */
			status_str = "cudaErrorLaunchFileScopedTex";
			break;

		case cudaErrorLaunchFileScopedSurf: /* 67 */
			status_str = "cudaErrorLaunchFileScopedSurf";
			break;

		case cudaErrorSyncDepthExceeded: /* 68 */
			status_str = "cudaErrorSyncDepthExceeded";
			break;

		case cudaErrorLaunchPendingCountExceeded: /* 69 */
			status_str = "cudaErrorLaunchPendingCountExceeded";
			break;

		case cudaErrorNotPermitted: /* 70 */
			status_str = "cudaErrorNotPermitted";
			break;

		case cudaErrorNotSupported: /* 71 */
			status_str = "cudaErrorNotSupported";
			break;

		case cudaErrorHardwareStackError: /* 72 */
			status_str = "cudaErrorHardwareStackError";
			break;

		case cudaErrorIllegalInstruction: /* 73 */
			status_str = "cudaErrorIllegalInstruction";
			break;

		case cudaErrorMisalignedAddress: /* 74 */
			status_str = "cudaErrorMisalignedAddress";
			break;

		case cudaErrorInvalidAddressSpace: /* 75 */
			status_str = "cudaErrorInvalidAddressSpace";
			break;

		case cudaErrorInvalidPc: /* 76 */
			status_str = "cudaErrorInvalidPc";
			break;

		case cudaErrorIllegalAddress: /* 77 */
			status_str = "cudaErrorIllegalAddress";
			break;

		case cudaErrorInvalidPtx: /* 78 */
			status_str = "cudaErrorInvalidPtx";
			break;

		case cudaErrorInvalidGraphicsContext: /* 79 */
			status_str = "cudaErrorInvalidGraphicsContext";
			break;

		case cudaErrorNvlinkUncorrectable: /* 80 */
			status_str = "cudaErrorNvlinkUncorrectable";
			break;

		case cudaErrorJitCompilerNotFound: /* 81 */
			status_str = "cudaErrorJitCompilerNotFound";
			break;

		case cudaErrorCooperativeLaunchTooLarge: /* 82 */
			status_str = "cudaErrorCooperativeLaunchTooLarge";
			break;
	}
	return status_str;
}
