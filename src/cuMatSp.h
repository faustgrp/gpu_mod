#ifndef __GPU_MOD_CUMATSP__
#define __GPU_MOD_CUMATSP__
#include "gm_Op.h"
#include "gm_Op_conv.h"
#include "cuda_utils.h"
#include "hMatSp.h"
#include "cuMat.h"
#include "cuMatDs.h"
#include "cuMatBSR.h"
#include "Real.h"

template <typename T>
struct cuMatDs;

template <typename T>
struct cuMatSp : cuMat<T>
{
	//TODO: encapsulation
	int32_t* rowptr;
	int32_t* colids;
	T* values;
	int32_t nnz;
	int32_t dev_id;
	void* stream;
	cusparseMatDescr_t descr;
	static cusparseHandle_t handle;
	cuMatSp(int32_t nrows, int32_t ncols, int32_t dev_id=-1); // zero
	cuMatSp(int32_t, int32_t, T*, int32_t*, int32_t*, int32_t, int32_t dev_id=-1, void* stream=nullptr);
	cuMatDs<T>* mul(cuMatDs<T>&, cuMatDs<T>* output = nullptr, gm_Op op_this = OP_NOTRANSP, gm_Op op_other = OP_NOTRANSP, const T* alpha = nullptr, const T* beta = nullptr);
	void mul(const T& scalar);
	/**
	 * Adds a scalar value ONLY to nonzeros (zeros stay zeros).
	 */
	void add(const T& scalar);
	/**
	 * Subtracts a scalar value ONLY to nonzeros (zeros stay zeros).
	 */
	void sub(const T& scalar);
	void mv_to_gpu(int32_t dev_id);
	void set_stream(const void* stream);
	void transpose();
	void conjugate();
	void adjoint();
	Real<T> norm_frob();
	size_t get_nnz() const;
	size_t get_nbytes() const;
	void resize(int32_t nnz, int32_t nrows, int32_t ncols);
	cuMatSp<T>* clone();
	/*
	 * Returns the real part copy of this matrix as a cuMatSp<Real<T>> matrix.
	 */
	cuMatSp<Real<T>>* real();
	/**
	 * Copies the values of this cuMatSp into the values device buffer which must be allocated prior to the call.
	 */
	void real_values(Real<T>* values);
	/**
	 * Copies this matrix to another (passed as argument).
	 */
	void copy(cuMatSp<T>*);
	void destroy();
	void set_eyes();
	void set_zeros();
	/**
	 * Fill each column of this by ones such that for for all j this[one_row_ids[j], j] == 1.
	 *
	 * \param one_row_ids: the row indices to set ones (the first one is for the first column and so on). WARNING: one_row_ids is sorted in this function (pass a copy if it matters).
	 * \param id_len: the length of one_row_ids.
	 */
	void set_row_ids_to_one(size_t *one_row_ids, size_t id_len);

	/**
	 * Fill each row of this by ones such that for all i this[i, one_col_ids[i]] == 1.
	 *
	 */
	void set_col_ids_to_one(size_t *one_col_ids, size_t id_len);

	/**
	 * Edits the GPU matrix by specifying the new data in CPU buffers.
	 */
	void cpu_set(const int32_t nnz, const int32_t nrows, const int32_t ncols, const T* values, const int32_t* rowptr, const int32_t* colids);
	/**
	 * Warning: Two matrices are equal means they share the same buffers and descriptor otherwise they are not
	 * (even if they have the same nnz, dimensions, coordinates and values).
	 */
	bool is_equal(const cuMatSp<T>*) const;
	bool is_sparse() const {return true;}
	bool is_csr() const  {return true;}
	bool is_bsr() const  {return false;}
	bool is_dense() const  {return false;}
	~cuMatSp();
	static cuMatSp<T>* create(hMatSp<T> &, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatSp<T>* create(int32_t, int32_t, const T*, const int32_t*, const int32_t*, int32_t, int32_t dev_id=-1, void* stream=nullptr);
	static cuMatSp<T>* create_zero(int32_t nrows, int32_t ncols, int32_t dev_id=-1);
	static void destroy(cuMatSp<T>*);

	static cuMatSp<T>* bsr2csr(cuMatBSR<T>& bsr_mat, int32_t dev_id=-1, void* stream=nullptr);

	private:
	void init_desc();
};

/********************* non-member functions */
template<typename T>
void spm_get_info(gm_SparseMat_t cu_mat, int32_t *nrows, int32_t *ncols, int32_t *nnz);

template<typename T>
gm_SparseMat_t spm_create(int32_t nrows, int32_t ncols, int32_t nnz, int32_t* rowptr, int32_t colids, T* values, const void* stream=nullptr);

template<typename T>
void spm_tocpu(gm_SparseMat_t cu_mat, int32_t *nrows, int32_t *ncols, int32_t *nnz, int32_t *rowptr, int32_t *colids, T* values);

template<typename T>
void cusparse_csr2dense(cuMatSp<T>* sp_mat, cuMatDs<T> *out, gm_Op op = OP_NOTRANSP);

#if (CUDA_VERSION > 9200)
/**
 * This function creates a CUSPARSE sparse matrix descriptor (cusparseSpMatDescr_t) corresponding to a cuMatSp.
 *
 * \warn: the descriptor must be freed after use calling the function cusparseDestroySpMat.
 */
template<typename T>
cusparseStatus_t spm_mat2spdesc(const cuMatSp<T>& A, cusparseSpMatDescr_t& spMatDescr);
#endif

#include "cuMatSp.hpp"
#endif
