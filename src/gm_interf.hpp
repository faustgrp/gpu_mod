void* gm_load_lib(const char* libpath, const bool silent /*=false*/)
{
	if(!silent)
		std::cout << "Loading libgm (" << libpath << ")";
#ifdef _MSC_VER
	HMODULE handle = LoadLibraryA(libpath);
	if (!handle)
	{
		if(!silent)
		{
			std::cout << " [FAILED]"<< std::endl;
			std::cerr << "LoadLibraryA Error code: " << GetLastError() << std::endl;
		}
		return nullptr;
	}
#else
    void* handle = dlopen(libpath, RTLD_LAZY);
	if (!handle) {
		if(!silent)
		{
			std::cout << " [FAILED]"<< std::endl;
			std::cerr << dlerror() << std::endl;
		}
		return nullptr;
	}
#endif
	else if(!silent) std::cout << " [OK]"<< std::endl;
	return handle;
}

void* gm_load_func_check_err(void *gm_handle, const char* fn_sym)
{
#ifdef DEBUG
	 std::cout << "gm_load_func_check_err tring to load:" << fn_sym << std::endl;
#endif
	char *error;
	if(gm_handle == nullptr)
	{
#ifdef GM_SYM_ERR_OUTPUT
		std::cerr << "library handle is null, can't load any symbol." << std::endl;
#endif
		return nullptr;
	}
#ifdef _MSC_VER
	void* func = GetProcAddress((HMODULE) gm_handle, fn_sym);
#else
	void* func = dlsym(gm_handle, fn_sym);
#endif
	check_dlsym_err(fn_sym, func);
	return func;
}

void gm_close_lib(void* handle)
{
#ifdef _MSC_VER
	FreeLibrary((HMODULE) handle);
#else
	dlclose(handle);
#endif
}

void load_gp_funcs(void *gm_handle, gm_GenPurposeFunc *gp_funcs)
{
	gp_funcs->free_mat = (gm_GenPurpose_free_mat_ptr) gm_load_func_check_err(gm_handle, "gm_GenPurpose_free_mat");
	gp_funcs->cur_dev = (gm_GenPurpose_cur_dev_ptr) gm_load_func_check_err(gm_handle, "gm_GenPurpose_cur_dev");
	gp_funcs->dev_count = (gm_GenPurpose_dev_count_ptr) gm_load_func_check_err(gm_handle, "gm_GenPurpose_dev_count");
	gp_funcs->set_dev = (gm_GenPurpose_set_dev_ptr) gm_load_func_check_err(gm_handle, "gm_GenPurpose_set_dev");
	gp_funcs->create_stream = (gm_GenPurpose_create_stream_ptr) gm_load_func_check_err(gm_handle, "gm_GenPurpose_create_stream");
}
