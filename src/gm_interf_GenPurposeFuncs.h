/** General Purpose function pointers ********************************************************************/
typedef void (*gm_GenPurpose_free_mat_ptr)(void* cu_mat);
typedef void (*gm_GenPurpose_set_dev_ptr)(const int32_t dev_id);
typedef int32_t (*gm_GenPurpose_cur_dev_ptr)();
typedef void* (*gm_GenPurpose_create_stream_ptr)();
typedef int32_t (*gm_GenPurpose_dev_count_ptr)();

struct gm_GenPurposeFunc
{
	gm_GenPurpose_free_mat_ptr free_mat;
	gm_GenPurpose_set_dev_ptr set_dev;
	gm_GenPurpose_cur_dev_ptr cur_dev;
	gm_GenPurpose_dev_count_ptr dev_count;
	gm_GenPurpose_create_stream_ptr create_stream;
	//TODO: add is_mat_sparse ?
};
