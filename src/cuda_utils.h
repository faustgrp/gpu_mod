#ifndef __CUDA__UTILS__
#define __CUDA__UTILS__
#include <cublas_v2.h>
#include <cusparse.h>
#include <cusolverDn.h>
#include <cuda.h>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdexcept>
#include <iostream>
#include <string>
#include <complex>
#include "Real.h"
#include <functional>

#include <cusolverDn.h> // SVD

//#include <helper_cuda.h>

/** decl needed from cuMatSp.h and cuMatDs.h (not included to avoid loop dep) */
template <typename T> struct cuMatSp;
template <typename T> struct cuMatDs;
#if (CUDA_VERSION > 9200)
template<typename T>
cusparseStatus_t spm_mat2spdesc(const cuMatSp<T>& A, cusparseSpMatDescr_t& spMatDescr);
template<typename T>
cusparseStatus_t dsm_mat2desc(const cuMatDs<T>& A, cusparseDnMatDescr_t& dnMatDescr);
#endif


#define check_cublas_error(ret, func) \
	if(ret != CUBLAS_STATUS_SUCCESS) \
		throw std::runtime_error(std::string(func)+" failed. status: "+std::to_string(ret)+" at "+__FILE__+":"+std::to_string(__LINE__))

#define check_cuda_error(ret, func) \
	if(ret != cudaSuccess) \
		throw std::runtime_error("!!!! "+std::string(func)+" error: "+cuda_error_int2str(ret)+" at "+__FILE__+":"+std::to_string(__LINE__))

// TODO: mimic above check_ functions
#define CUSOLVER_CHECK(err)                                                                        \
	do {                                                                                           \
		cusolverStatus_t err_ = (err);                                                             \
		if (err_ != CUSOLVER_STATUS_SUCCESS) {                                                     \
			printf("cusolver error %d at %s:%d\n", err_, __FILE__, __LINE__);                      \
			throw std::runtime_error("cusolver error");                                            \
		}                                                                                          \
	} while (0)


#define check_cusparse_error(ret, func) \
	if(ret != CUSPARSE_STATUS_SUCCESS) \
		throw std::runtime_error("!!!! "+std::string(func)+" error: "+cusparse_error_int2str(ret)+" at "+__FILE__+":"+std::to_string(__LINE__))

#include <map>

struct MemoryMonitor
{
	static std::map<void*, size_t> memory;
	static size_t alloc_memory;
};
//TODO: document

/**
 * \brief Changes current device used.
 *
 * \param dev_id: the device id.
 *
 * \throw std::runtime_error if cudaSetDevice fails.
 */
void set_dev(int dev_id);

/**
 * \brief This function switches to another device and returns a lambda to switch back to the previous device.
 */
std::function<void()> switch_dev(int32_t dev_id);

/**
 *
 * \brief Returns the number of devices available.
 *
 * \throw std::runtime_error if cudaGetDeviceCount fails.
 */
int32_t dev_count();

/**
 * \brief Gets current device used.
 *
 * \return the device id currently used.
 *
 * \throw std::runtime_error if cudaGetDevice fails.
 */
int32_t cur_dev();

/**
 * Returns true if dev_id is inclusively between 0 and dev_count()-1 false otherwise.
 *
 */
bool is_valid_dev_id(int32_t dev_id);

/**
 * \brief Returns the cuda error name from the corresponding error value.
 *
 * \return the error name as a std::string.
 */
std::string cuda_error_int2str(int32_t error_id);

/**
 * \brief Returns the cusparse error name from the corresponding error value.
 *
 * \return the error name as a std::string.
 */
std::string cusparse_error_int2str(int32_t error_id);

/**
 * \brief Returns the cublas error name from the corresponding error value.
 *
 * \return the error name as a std::string.
 */
std::string cublas_error_int2str(int32_t error_id);

/**
 * \brief Creates a stream.
 *
 * \return the created stream.
 */
void* create_stream();

/** \brief Allocates a buffer on a device.
 *
 * \param buf_len: number of elements of type T to allocate.
 * \param buf: the pointer where to store the pointer to the allocated memory area.
 * \param dev_id: the device on which the memory must be allocated.
 *
 * \throw std::runtime_error if cudaMalloc fails to allocate memory.
**/
template<typename T>
void alloc_dbuf(int32_t buf_len, T** buf, int32_t dev_id=-1);

/** \brief Frees a buffer on a device.
 *
 * \param dbuf: a pointer of a buffer allocated on the current device.
*/
template<typename T>
void free_dbuf(T *dbuf);

/**
 * \brief Copies a host buffer to a device buffer.
 *
 * \param size: buffer length.
 * \param hbuf: host source buffer.
 * \param dbuf: device destination buffer.
 * \param dev_id: device.
 * \param stream: the stream to use for the copy (nullptr is the default stream).
 *
 */template<typename T>
void copy_hbuf2dbuf(const int32_t size, const T* hbuf, T *dbuf, const int32_t dev_id=-1, void* stream_id=nullptr);

/**
 * \brief Copies a device buffer to another one.
 *
 * \param dbuf_len: buffer length.
 * \param src_dbuf: source buffer.
 * \param dst_dbuf: destination buffer.
 * \param src_dev_id: source device.
 * \param dst_dev_id: destination device.
 * \param stream: the stream to use for the copy (nullptr is the default stream).
 *
 */
template<typename T>
void copy_dbuf2dbuf(const int32_t dbuf_len, const T* src_dbuf, T* dst_dbuf, int32_t src_dev_id=-1, int32_t dst_dev_id=-1, void* stream_id=nullptr);

/**
 * \brief Copies a device buffer to a host buffer.
 *
 * \param dbuf_len: buffer length.
 * \param dbuf: device source buffer.
 * \param hbuf: host destination buffer.
 * \param dev_id: device.
 * \param stream: the stream to use for the copy (nullptr is the default stream).
 *
 */
template<typename T>
void copy_dbuf2hbuf(const int32_t dbuf_len, const T* dbuf, T* hbuf, const int32_t dev_id=-1, void* stream_id=nullptr);

template<typename T>
void set_one(T* scal);

template<typename T>
cusparseStatus_t
cusparseTcsrmm2(cusparseHandle_t         handle,
                cusparseOperation_t      transA,
                cusparseOperation_t      transB,
                int                      m,
                int                      n,
                int                      k,
                int                      nnz,
                const T              *alpha,
                const cusparseMatDescr_t descrA,
                const T              *csrValA,
                const int                *csrRowPtrA,
                const int                *csrColIndA,
                const T              *B,
                int                      ldb,
                const T              *beta,
                T                    *C,
                int                      ldc);

template<typename T>
cusparseStatus_t cusparseTcsr2csc(cusparseHandle_t handle, int m, int n, int nnz,
		const T *csrVal, const int *csrRowPtr, 
		const int *csrColInd, T           *cscVal,
		int *cscRowInd, int *cscColPtr, 
		cusparseAction_t copyValues, 
		cusparseIndexBase_t idxBase);

template<typename T>
cublasStatus_t cublasTcopy(cublasHandle_t handle, int n,
		const T           *x, int incx,
		T                 *y, int incy);



template<typename T>
cublasStatus_t cublasTgemm(cublasHandle_t handle,
		cublasOperation_t transa, cublasOperation_t transb,
		int m, int n, int k,
		const T           *alpha,
		const T           *A, int lda,
		const T           *B, int ldb,
		const T           *beta,
		T           *C, int ldc);

template<typename T>
cublasStatus_t cublasTgeam(cublasHandle_t handle,
		cublasOperation_t transa, cublasOperation_t transb,
		int m, int n,
		const T           *alpha,
		const T           *A, int lda,
		const T           *beta,
		const T           *B, int ldb,
		T           *C, int ldc);

template<typename T, typename U>
cublasStatus_t cublasTnrm2(cublasHandle_t handle, int n,
		const T *x, int incx, U *result);

template<typename T>
cublasStatus_t cublasTdot(cublasHandle_t handle, int n,
		const T *x, int incx, const T *y, int incy,
		T *result);

template<typename T, typename U>
cublasStatus_t cublasTscal2(cublasHandle_t handle, int n,
		const U *alpha, T *x, int incx);

template<typename T>
cublasStatus_t cublasTscal(cublasHandle_t handle, int n,
		const T *alpha, T *x, int incx);


template<typename T>
T inv_scal(T scal);

template<typename T>
T minus_scal(T scal);

template<typename T>
T sub(T a, T b);

template<typename T>
Real<T> abs(T& a);

template<typename T, typename U>
U gm_sqrt(T& a);

template<typename T>
T operator/(const T& c, const Real<T>& d);

#ifndef _MSC_VER // it seems to hide some STL overrides on VS14 (try to re-enable when gpu_mod will have its own namespace) // https://gitlab.inria.fr/faustgrp/faust/-/jobs/878972
//No need to instantiate types for float and double in cuda_utils.cpp
// only float2 and double2 need a definition
template<typename T>
bool operator==(const T& a, const T& b);

template<typename T>
bool operator!=(const T& a, const T& b);
#endif
template<typename T>
bool is_one(const T& a);

#if (CUDA_VERSION > 9200)
template<typename T>
cudaDataType type2cudaDataType(const T& v);

/**
 * This function helps to use cusparseSpMM function. Which calculates out = alpha*op(spm)*op(dsm)+beta*out.
 * It manages the sparse and dense matrix descriptors creation (and destruction) which are mandatory to call the cusparse function.
 *
 * \param spm: the sparse matrix.
 * \param dsm: the dense matrix.
 * \param sp_op: the cusparse operation on spm.
 * \param ds_op: the cusparse operation on dsm.
 * \param alpha: the T scalar to multiply op(spm)*op(dsm) by.
 * \param beta: the T scalar to multiply out by.
 * \param out: the dense matrix output (also an input).
 * \param err_tag: an information tag passed from the callee and integrated in error messages.
 */
template<typename T>
cusparseStatus_t helper_cusparseSpMM(cuMatSp<T> &spm, cuMatDs<T> &dsm,
		cusparseOperation_t sp_op, cusparseOperation_t ds_op,
		const T& alpha, const T& beta,
		cuMatDs<T> &out,
		const std::string& err_tag);
#endif

template<typename T>
cusparseStatus_t
cusparseTbsrmm(cusparseHandle_t         handle,
		cusparseDirection_t      dirA,
		cusparseOperation_t      transA,
		cusparseOperation_t      transB,
		int                      mb,
		int                      n,
		int                      kb,
		int                      nnzb,
		const T*             alpha,
		const cusparseMatDescr_t descrA,
		const T*             bsrValA,
		const int*               bsrRowPtrA,
		const int*               bsrColIndA,
		int                      blockDim,
		const T*             B,
		int                      ldb,
		const T*             beta,
		T*                   C,
		int                      ldc);

template<typename T>
cusparseStatus_t
cusparseTbsrmv(cusparseHandle_t         handle,
               cusparseDirection_t      dir,
               cusparseOperation_t      trans,
               int                      mb,
               int                      nb,
               int                      nnzb,
               const T*             alpha,
               const cusparseMatDescr_t descr,
               const T*             bsrVal,
               const int*               bsrRowPtr,
               const int*               bsrColInd,
               int                      blockDim,
               const T*             x,
               const T*             beta,
               T*                   y);

template<typename T>
cusparseStatus_t
cusparseTbsr2csr(cusparseHandle_t         handle,
                 cusparseDirection_t      dir,
                 int                      mb,
                 int                      nb,
                 const cusparseMatDescr_t descrA,
                 const T*             	  bsrValA,
                 const int*               bsrRowPtrA,
                 const int*               bsrColIndA,
                 int                      blockDim,
                 const cusparseMatDescr_t descrC,
                 T*                   csrValC,
                 int*                     csrRowPtrC,
                 int*                     csrColIndC);

template<typename T>
cusparseStatus_t
cusparseTcsr2bsr(cusparseHandle_t         handle,
                 cusparseDirection_t      dir,
                 int                      m,
                 int                      n,
                 const cusparseMatDescr_t descrA,
                 const T*             	  csrValA,
                 const int*               csrRowPtrA,
                 const int*               csrColIndA,
                 int                      blockDim,
                 const cusparseMatDescr_t descrC,
                 T*                       bsrValC,
                 int*                     bsrRowPtrC,
                 int*                     bsrColIndC);


template<typename T>
cusolverStatus_t
cusolverDnTgesvdjBatched_bufferSize(
    cusolverDnHandle_t handle,
    cusolverEigMode_t jobz,
    int m,
    int n,
    const T *A,
    int lda,
    const Real<T> *S,
    const T *U,
    int ldu,
    const T *V,
    int ldv,
    int *lwork,
    gesvdjInfo_t params,
    int batchSize);

template<typename T>
cusolverStatus_t
cusolverDnTgesvdjBatched(
    cusolverDnHandle_t handle,
    cusolverEigMode_t jobz,
    int m,
    int n,
    T *A,
    int lda,
    Real<T> *S,
    T *U,
    int ldu,
    T *V,
    int ldv,
    T *work,
    int lwork,
    int *info,
    gesvdjInfo_t params,
    int batchSize);

#include "cuda_utils.hpp"
#endif
