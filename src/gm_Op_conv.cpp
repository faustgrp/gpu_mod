#include "gm_Op_conv.h"
#include <iostream>

using namespace std;

cublasOperation_t gm_Op2cublas(gm_Op op)
{
	switch(op)
	{
		case OP_TRANSP:
			return CUBLAS_OP_T;
		case OP_NOTRANSP:
			return CUBLAS_OP_N;
		case OP_CONJTRANSP:
			return CUBLAS_OP_C;
	}
	return CUBLAS_OP_N; // never reached except if gm_Op was modified in the meantime
}

cusparseOperation_t gm_Op2cusparse(gm_Op op)
{
	switch(op)
	{
		case OP_TRANSP:
			return CUSPARSE_OPERATION_TRANSPOSE;
		case OP_NOTRANSP:
			return CUSPARSE_OPERATION_NON_TRANSPOSE;
		case OP_CONJTRANSP:
			return CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE;
	}
	return CUSPARSE_OPERATION_NON_TRANSPOSE;// never reached except if gm_Op was modified in the meantime
}

void display_op(gm_Op op)
{
        switch(op)
        {
                case OP_TRANSP:
                        cout << "OP_TRANSP" << endl;
						break;
                case OP_NOTRANSP:
                        cout << "OP_NOTRANSP" << endl;
						break;
                case OP_CONJTRANSP:
                        cout << "OP_CONJTRANSP" << endl;
						break;
        }
}

