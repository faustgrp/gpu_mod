#include <algorithm>
#include <numeric>

template<typename T>
void cuMatSp<T>::destroy()
{
//	std::cout << "void cuMatSp<T>::destroy()" << std::endl;
	cuMatSp<T>::destroy(this); // could call directly delete on this but prefer rely on already established code (in case of modif. on the latter)
}

template<typename T>
cuMatSp<T>::~cuMatSp<T>()
{
	auto switch_back = switch_dev(dev_id);
	if(values != nullptr)
		free_dbuf(values);
	if(colids != nullptr)
		free_dbuf(colids);
	if(rowptr != nullptr)
		free_dbuf(rowptr);
	switch_back();
}

template<typename T>
void cuMatSp<T>::destroy(cuMatSp<T> * inst)
{
//	std::cout << "void cuMatSp<T>::destroy(cuMatSp<T> * inst)" << std::endl;
	delete inst;
}

template<typename T> cusparseHandle_t cuMatSp<T>::handle = 0;

template<typename T>
void cuMatSp<T>::init_desc()
{
	/* create and setup matrix descriptor */
	auto status = cusparseCreateMatDescr(&descr);
	check_cusparse_error(status, "cuMatSp<T>::cuMatSp() > cusparseCreateMatDescr");
	cusparseSetMatType(descr,CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descr,CUSPARSE_INDEX_BASE_ZERO);
}

template<typename T>
cuMatSp<T>::cuMatSp(int32_t nrows, int32_t ncols, T* values, int32_t *rowptr, int32_t* colids, int32_t nnz, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/) : cuMat<T>::cuMat(nrows, ncols), values(values), rowptr(rowptr), colids(colids), nnz(nnz), dev_id(dev_id), stream(stream)
{
	init_desc();
	if(dev_id == -1)
		this->dev_id = cur_dev();
	//std::cout << "cuMatSp() is_sparse :" << is_sparse() << std::endl;
	//std::cout << "cuMatSp() cuMat is_sparse: " << ((cuMat*) this)->is_sparse() << std::endl;
	if(! handle) cusparseCreate(&handle);
}

template<typename T>
cuMatSp<T>::cuMatSp(int32_t nrows, int32_t ncols, int32_t dev_id/*=-1*/)
{
	//This ctor is only to create zero matrix
	rowptr = nullptr;
	colids = nullptr;
	values = nullptr;
	if(nrows != 0)
	{
		alloc_dbuf(nrows+1, &rowptr, dev_id);
	}
	if(dev_id == -1)
		this->dev_id = cur_dev();
	else
		this->dev_id = dev_id;
	this->nnz = 0;
	this->nrows = nrows;
	this->ncols = ncols;
	this->stream = nullptr;
	if(! handle) cusparseCreate(&handle);
	init_desc();
}

template<typename T>
cuMatSp<T>* cuMatSp<T>::create(hMatSp<T> &hmat, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	return cuMatSp<T>::create(hmat.nrows, hmat.ncols, hmat.values, hmat.rowptr, hmat.colids, hmat.nnz, dev_id, stream);
}

template<typename T>
cuMatSp<T>* cuMatSp<T>::create_zero(int32_t nrows, int32_t ncols, int32_t dev_id/*=-1*/)
{
	return new cuMatSp<T>(nrows, ncols, dev_id);
}

template<typename T>
cuMatSp<T>* cuMatSp<T>::create(int32_t nrows, int32_t ncols, const T* values, const int32_t* rowptr, const int32_t* colids, int32_t nnz, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
//	std::cout << "cuMatSp<T>::create nrows: " << nrows << " ncols: " << ncols << " values:" << values << " rowptr: " << rowptr << " colids: " << colids << " nnz:" << nnz << std::endl;
	cuMatSp<T>* cu_mat = nullptr;
	int32_t *dm_rowptr, *dm_colids;
	T *dm_values;
//	std::cout << "nnz=" << nnz << std::endl;
	alloc_dbuf(nrows+1, &dm_rowptr, dev_id);
	if(nnz > 0)
	{
		alloc_dbuf(nnz, &dm_values, dev_id);
		alloc_dbuf(nnz, &dm_colids, dev_id);
		copy_hbuf2dbuf(nnz, values, dm_values, dev_id, stream);
		copy_hbuf2dbuf(nnz, colids, dm_colids, dev_id, stream);
	}
	else
	{
		dm_values = nullptr;
		dm_colids = nullptr;
	}
	copy_hbuf2dbuf(nrows+1, rowptr, dm_rowptr, dev_id, stream);
	Scalar type = type2Scalar<T>();
	cu_mat = new cuMatSp<T>(nrows, ncols, dm_values, dm_rowptr, dm_colids, nnz, dev_id, stream);
	return cu_mat;
}

template<typename T>
cuMatSp<T>* cuMatSp<T>::bsr2csr(cuMatBSR<T>& bsr_mat, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	int32_t nrows = bsr_mat.nrows;
	int32_t ncols = bsr_mat.ncols;
	cuMatSp<T>* cu_mat = nullptr;
	int32_t *dm_rowptr, *dm_colids;
	T *dm_values;
	int32_t nnz;

	if(bsr_mat.bnnz == 0)
		return cuMatSp<T>::create_zero(nrows, ncols);

	nnz = bsr_mat.bnnz*bsr_mat.bnrows*bsr_mat.bncols;

	alloc_dbuf(nrows+1, &dm_rowptr, dev_id);
	alloc_dbuf(nnz, &dm_values, dev_id);
	alloc_dbuf(nnz, &dm_colids, dev_id);

	cusparseStatus_t status;

	Scalar type = type2Scalar<T>();
	auto csr_mat = new cuMatSp<T>(nrows, ncols, dm_values, dm_rowptr, dm_colids, nnz, dev_id, stream);

	// the matrix buffers are empty, now initialize them through cusparse (copy from bsr matrix)
	status = cusparseTbsr2csr(bsr_mat.handle,
			CUSPARSE_DIRECTION_COLUMN,
			bsr_mat.nblocks_per_row_dim,
			bsr_mat.nblocks_per_col_dim,
			bsr_mat.descr,
			bsr_mat.bdata,
			bsr_mat.browptr,
			bsr_mat.bcolids,
			bsr_mat.bnrows, /* == bncols */
			csr_mat->descr,
			csr_mat->values,
			csr_mat->rowptr,
			csr_mat->colids);

	check_cusparse_error(status, "cuMatSp::create cusparseTbsr2csr");

	return csr_mat;
}

#if (CUDA_VERSION <= 9200)
template<typename T>
cuMatDs<T>* cuMatSp<T>::mul(cuMatDs<T> & other, cuMatDs<T>* output, gm_Op op_this, gm_Op op_other, const T* alpha_ /*=nullptr*/, const T* beta_ /*=nullptr*/)
{
	//TODO: it should be possible that other and output are the same
	auto switch_back = switch_dev(dev_id);
	cuMatDs<T>* extra_other = nullptr;
	int m, n, k;
	int ldb, ldc;
	int output_nrows, output_ncols;
	T alpha, beta;
	auto cusparse_op_this = gm_Op2cusparse(op_this);
	auto cusparse_op_other = gm_Op2cusparse(op_other);
	if(alpha_)
		alpha = *alpha_;
	else
		set_one(&alpha);
	if(beta_)
		beta = *beta_;
	else
		bzero(&beta, sizeof(T));
	if(op_this == OP_NOTRANSP)
		output_nrows = this->nrows;
	else
		output_ncols = this->ncols;
	if(op_other == OP_NOTRANSP)
		output_ncols = other.ncols;
	else
		output_ncols = other.nrows;
	if(nullptr == output)
		output = cuMatDs<T>::create(output_nrows, output_ncols);
	else
		if(output->nrows != output_nrows || output->ncols != output_ncols)
			throw std::runtime_error("error: cuMatSp<T>::mul(cuMatDs<T>), dimensions must agree.");
	m = this->nrows;
	k = this->ncols;
	if(op_other == OP_TRANSP)
	{
		n = other.nrows;
		ldb = n;
	}
	else
	{
		if(op_other == OP_CONJTRANSP)
		{
			// cusparseTcsrmm2 doesn't handle op_other == OP_CONJTRANSP
			// https://docs.nvidia.com/cuda/archive/9.2/cusparse/index.html
			extra_other = other.clone();
			extra_other->adjoint();
			cusparse_op_other = CUSPARSE_OPERATION_NON_TRANSPOSE;
			n = extra_other->ncols;
		}
		else
			n = other.ncols;
		if(op_this == OP_NOTRANSP)
			ldb = k;
		else
			ldb = m;
	}

	if(op_this == OP_NOTRANSP)
		ldc = m;
	else
		ldc = k;

	cusparseStatus_t status = cusparseTcsrmm2(this->handle,
			cusparse_op_this, cusparse_op_other,
			m, n, k,
			this->nnz,
			&alpha,
			this->descr, this->values, this->rowptr, this->colids,
			extra_other->data, ldb,
			&beta, output->data, ldc);

	if(op_other == OP_CONJTRANSP)
		delete extra_other;

	check_cusparse_error(status, "cuMatSp::mul(cuMatDs, cuMatDs, gm_Op, gm_Op) > cusparseTcsrmm2");
	switch_back();
	return output;
}
#else
template<typename T>
cuMatDs<T>* cuMatSp<T>::mul(cuMatDs<T> & other, cuMatDs<T>* output, gm_Op op_this, gm_Op op_other, const T* alpha_ /*=nullptr*/, const T* beta_ /*=nullptr*/)
{
	//TODO: it should be possible that other and output are the same
	auto switch_back = switch_dev(dev_id);
	cusparseDnMatDescr_t dense_mat_desc, output_mat_desc;
	cusparseSpMatDescr_t sp_mat_desc;
	int output_nrows, output_ncols;
	T alpha, beta;
	auto cusparse_op_this = gm_Op2cusparse(op_this);
	auto cusparse_op_other = gm_Op2cusparse(op_other);
	size_t bufferSize;
	if(alpha_)
		alpha = *alpha_;
	else
		set_one(&alpha);
	if(beta_)
		beta = *beta_;
	else
		bzero(&beta, sizeof(T));
	auto sp_mat = this;
	auto ds_mat = &other;
	cuMat<T>* extra_mat_ds = nullptr, *extra_mat_sp = nullptr;

	if(op_this == OP_NOTRANSP)
		output_nrows = this->nrows;
	else
	{
		output_nrows = this->ncols;
		extra_mat_sp = sp_mat = this->clone();
		if(op_this == OP_TRANSP)
			sp_mat->transpose();
		else
			sp_mat->adjoint();
		cusparse_op_this = CUSPARSE_OPERATION_NON_TRANSPOSE;
	}
	if(op_other == OP_NOTRANSP)
		output_ncols = other.ncols;
	else
	{
		output_ncols = other.nrows;
		if(op_other == OP_CONJTRANSP)
		{
			extra_mat_ds = ds_mat = other.clone();
			ds_mat->adjoint();
			cusparse_op_other = CUSPARSE_OPERATION_NON_TRANSPOSE;
		}
	}
	if(nullptr == output)
		output = cuMatDs<T>::create(output_nrows, output_ncols);
	else
		if(output->nrows != output_nrows || output->ncols != output_ncols)
			throw std::runtime_error("error: cuMatSp<T>::mul(cuMatDs<T>), dimensions must agree.");
	// cusparseSpMM only handle op_this == OP_NOTRANSP
	// https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-generic-function-spmm
	// cusparseSpMM doesn't handle op_other == OP_CONJTRANSP
	helper_cusparseSpMM(*sp_mat, *ds_mat, cusparse_op_this, cusparse_op_other, alpha, beta, *output, "cuMatSp::mul(cuMatDs)");
	if(extra_mat_ds)
		delete extra_mat_ds;
	if(extra_mat_sp)
		delete extra_mat_sp;
	switch_back();
	return output;
}
#endif

template<typename T>
	void cuMatSp<T>::set_stream(const void* stream)
{
	this->stream = const_cast<void*>(stream);
}

template<typename T>
	void cuMatSp<T>::mv_to_gpu(int32_t dev_id)
{
	if(this->dev_id != dev_id)
	{
		T* dm_values;
		int32_t *dm_colids;
		int32_t *dm_rowptr;
		alloc_dbuf(this->nrows+1, &dm_rowptr, dev_id);
		alloc_dbuf(nnz, &dm_values, dev_id);
		alloc_dbuf(nnz, &dm_colids, dev_id);
		copy_dbuf2dbuf(this->nrows+1, rowptr, dm_rowptr, this->dev_id, dev_id, stream);
		copy_dbuf2dbuf(nnz, values, dm_values, this->dev_id, dev_id, stream);
		copy_dbuf2dbuf(nnz, colids, dm_colids, this->dev_id, dev_id, stream);
		auto switch_back = switch_dev(this->dev_id); // just in case
		free_dbuf(this->values);
		free_dbuf(this->rowptr);
		free_dbuf(this->colids);
		switch_back();
		this->colids = dm_colids;
		this->rowptr = dm_rowptr;
		this->values = dm_values;
		this->dev_id = dev_id;
	}
}

template<typename T>
Real<T> cuMatSp<T>::norm_frob()
{
	cuMatDs<T> val_vec(nnz, 1, /*buf_nrows*/ -1, /*buf_ncols*/ -1, values, dev_id);
	auto n = val_vec.norm_frob();
	val_vec.data = nullptr; // avoid to free values
	return n;
}

template<typename T>
void cuMatSp<T>::conjugate()
{
	//TODO: a cuda kernel to directly conjugate in-place the values
	if(sizeof(Real<T>) != sizeof(T))
	{
		// T is complex
		T* val_copy;
		alloc_dbuf(nnz, &val_copy, dev_id);
		copy_dbuf2dbuf(nnz, values, val_copy, dev_id, dev_id, stream);
		cuMatDs<T> val_vec(nnz, 1, /*buf_nrows*/ -1, /*buf_ncols*/ -1, val_copy, dev_id);
		val_vec.conjugate();
		free_dbuf(this->values);
		this->values = val_copy;
		val_vec.data = nullptr; // otherwise val_copy is freed out of this scope
	}
	// else T is real, nothing to do
}

template<typename T>
void cuMatSp<T>::adjoint()
{
	this->transpose();
	this->conjugate();
}

template<typename T>
void cuMatSp<T>::transpose()
{
	T* cscVal = nullptr;
	int32_t *cscRowInd = nullptr;
	int32_t *cscColPtr = nullptr;
	int32_t tmp;
	alloc_dbuf(nnz, &cscVal);
	alloc_dbuf(nnz, &cscRowInd);
	alloc_dbuf(this->ncols+1, &cscColPtr);

	auto status = cusparseTcsr2csc(handle, this->nrows, this->ncols, nnz,
			values,	rowptr, colids,
			cscVal, cscRowInd, cscColPtr,
			CUSPARSE_ACTION_NUMERIC, //TODO: CUSPARSE_ACTION_SYMBOLIC
			CUSPARSE_INDEX_BASE_ZERO);
	check_cusparse_error(status, "cuMatSp::transpose");
	free_dbuf(this->values);
	free_dbuf(this->colids);
	free_dbuf(this->rowptr);
	this->values = cscVal;
	this->colids = cscRowInd;
	this->rowptr = cscColPtr;
	tmp = this->nrows;
	this->nrows = this->ncols;
	this->ncols = tmp;
}

/********************* non-member functions */
template<typename T>
void spm_get_info(gm_SparseMat_t cu_mat, int32_t *nrows, int32_t *ncols, int32_t *nnz)
{
	//std::cout << "spm_get_info" << std::endl;
	cuMatSp<T>* cu_mat_sp = static_cast<cuMatSp<T>*>(cu_mat);
	auto switch_back = switch_dev(cu_mat_sp->dev_id);
	if(! cu_mat_sp->is_csr() || ! cu_mat_sp->is_cuda())
		throw std::runtime_error("spm_get_info error: matrix is not CSR or not cuda");
	if(nrows != nullptr)
		*nrows = cu_mat_sp->nrows;
	if(ncols != nullptr)
		*ncols = cu_mat_sp->ncols;
	if(nnz != nullptr)
		*nnz = cu_mat_sp->nnz;
	switch_back();
}

template<typename T>
gm_SparseMat_t spm_create(int32_t nrows, int32_t ncols,
		int32_t nnz, const int32_t* rowptr, const int32_t* colids, const T* values, const void* stream/*=nullptr*/)
{
	return cuMatSp<T>::create(nrows, ncols, values, rowptr, colids, nnz, /*dev_id*/ -1, const_cast<void*>(stream));
}

template<typename T>
void spm_tocpu(gm_SparseMat_t cu_mat, int32_t *rowptr, int32_t *colids, T* values)
{
	cuMatSp<T>* cu_mat_sp = static_cast<cuMatSp<T>*>(cu_mat);
	if(!cu_mat_sp->is_csr() || ! cu_mat_sp->is_cuda())
		throw std::runtime_error("spm_tocpu error: matrix is not CSR or not cuda");
//	std::cout << "spm_tocpu nnz=" << cu_mat_sp->nnz << std::endl;
	copy_dbuf2hbuf(cu_mat_sp->nnz, cu_mat_sp->values, values, cu_mat_sp->dev_id, cu_mat_sp->stream);
	copy_dbuf2hbuf(cu_mat_sp->nrows+1, cu_mat_sp->rowptr, rowptr, cu_mat_sp->dev_id, cu_mat_sp->stream);
	copy_dbuf2hbuf(cu_mat_sp->nnz, cu_mat_sp->colids, colids, cu_mat_sp->dev_id, cu_mat_sp->stream);
}

#if (CUDA_VERSION <= 9200)
template<typename T>
void cusparse_csr2dense(cuMatSp<T>* sp_mat, cuMatDs<T> *out, gm_Op op /* = OP_NOTRANSP*/)
{
	// out is initialized?
	if(out == nullptr)
		throw std::runtime_error("out matrix ptr is nullptr");
	// verify buf_nrows, buf_ncols larger enough to receive spmat copy
	if(out->buf_nrows*out->buf_ncols < sp_mat->nrows*sp_mat->ncols)
		throw std::runtime_error("cusparse_csr2dense: out dense matrix buffer is not large enough to receive a copy of sparse matrix.");
	//TODO: create identity using cuMatDs<T>:createEye
	auto switch_back = switch_dev(sp_mat->dev_id);
	cusparseHandle_t handle = sp_mat->handle;
	cusparseOperation_t cusparse_op = gm_Op2cusparse(op);
	T *id_hmat, *id_dmat;
	int im_sz[2], is, is2; // id mat size
	int ldb, ldc;
	if(op == OP_NOTRANSP)
	{
		is = sp_mat->ncols;
		ldb = sp_mat->ncols;
		ldc = sp_mat->nrows;
		out->nrows = sp_mat->nrows;
		out->ncols = sp_mat->ncols;
	}
	else
	{
		is = sp_mat->nrows;
		ldb = sp_mat->nrows;
		ldc = sp_mat->ncols;
		out->nrows = sp_mat->ncols;
		out->ncols = sp_mat->nrows;
	}
	is2 = is*is;
	alloc_dbuf(is2, &id_dmat);
	id_hmat = new T[is2];
	bzero(id_hmat, sizeof(T)*is2);
	for(int i=0;i < is2; i+=is+1) set_one(id_hmat+i);
	copy_hbuf2dbuf(is2, id_hmat, id_dmat);
	T alpha, beta;
	set_one(&alpha);
	// beta = 0
	bzero(&beta, sizeof(T));
	cusparseStatus_t status = cusparseTcsrmm2(handle, cusparse_op, CUSPARSE_OPERATION_NON_TRANSPOSE, sp_mat->nrows, is, sp_mat->ncols, sp_mat->nnz, &alpha,
			sp_mat->descr /*cusparseMatDescr_t*/, sp_mat->values, sp_mat->rowptr, sp_mat->colids, id_dmat, ldb, &beta, out->data, ldc);
	free_dbuf(id_dmat);
	delete[] id_hmat;
	check_cusparse_error(status, "cusparseTcsrmm2 in cusparse_csr2dense");
	switch_back();
}
#else
template<typename T>
void cusparse_csr2dense(cuMatSp<T>* sp_mat, cuMatDs<T> *out, gm_Op op /* = OP_NOTRANSP*/)
{
	// out is initialized?
	if(out == nullptr)
		throw std::runtime_error("out matrix ptr is nullptr");
	// verify buf_nrows, buf_ncols larger enough to receive spmat copy
	if(out->buf_nrows*out->buf_ncols < sp_mat->nrows*sp_mat->ncols)
		throw std::runtime_error("cusparse_csr2dense: out dense matrix buffer is not large enough to receive a copy of sparse matrix.");
	auto switch_back = switch_dev(sp_mat->dev_id);
	out->nrows = sp_mat->nrows;
	out->ncols = sp_mat->ncols;
	cusparseHandle_t handle = sp_mat->handle;
	//https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-generic-function-sparse2dense
	cusparseDnMatDescr_t output_mat_desc = nullptr;
	cusparseSpMatDescr_t sp_mat_desc = nullptr;
	size_t bufferSize;
	void* externalBuffer;
	cusparseStatus_t status = dsm_mat2desc(*out, output_mat_desc);
	check_cusparse_error(status, "cuMatSp::cusparse_csr2dense() dsm_mat2desc");
	status = spm_mat2spdesc(*sp_mat, sp_mat_desc);
	check_cusparse_error(status, "cuMatSp::cusparse_csr2dense() spm_mat2desc");
	status = cusparseSparseToDense_bufferSize(handle, sp_mat_desc, output_mat_desc, CUSPARSE_SPARSETODENSE_ALG_DEFAULT, &bufferSize);
	check_cusparse_error(status, "cuMatSp::cusparse_csr2dense() > cusparseSparseToDense_bufferSize");
	auto ret = cudaMalloc(reinterpret_cast<void **>(&externalBuffer), bufferSize);
	check_cuda_error(ret, "cusparse_csr2dense cudaMalloc");
	status = cusparseSparseToDense(handle, sp_mat_desc, output_mat_desc, CUSPARSE_SPARSETODENSE_ALG_DEFAULT, externalBuffer);
	check_cusparse_error(status, "cuMatSp::cusparse_csr2dense() > cusparseSparseToDense");
	cudaFree(externalBuffer);
	if(op == OP_TRANSP)
		out->transpose();
	else if(op == OP_CONJTRANSP)
		out->adjoint();
	cusparseDestroySpMat(sp_mat_desc);
	cusparseDestroyDnMat(output_mat_desc);
	switch_back();
}
#endif

template<typename T>
size_t cuMatSp<T>::get_nnz() const
{
	return this->nnz<0?0:this->nnz;
}

template<typename T>
size_t cuMatSp<T>::get_nbytes() const
{
	return this->get_nnz()*(sizeof(T)+sizeof(int32_t))+(this->nrows+1)*sizeof(int32_t);
}

template<typename T>
void cuMatSp<T>::resize(int32_t nnz, int32_t nrows, int32_t ncols)
{
	auto switch_back = switch_dev(this->dev_id);
	this->ncols = ncols;
	if(this->nrows == nrows && this->nnz == nnz)
	{
		// the number of cols doesn't change the buffer sizes
		return;
	}
	if(nnz != this->nnz)
	{
		// realloc the matrix buffers
		T* cu_values;
		int32_t* cu_colids;
		alloc_dbuf(nnz, &cu_values, dev_id);
		alloc_dbuf(nnz, &cu_colids, dev_id);
		// free old bufs
		if(this->values != nullptr)
			free_dbuf(this->values);
		if(this->colids != nullptr)
			free_dbuf(this->colids);
		// save new ones
		this->values = cu_values;
		this->colids= cu_colids;
		this->nnz = nnz;
	}
	if(nnz == 0)
	{
		if(this->values != nullptr)
			free_dbuf(this->values);
		if(this->colids != nullptr)
			free_dbuf(this->colids);
		this->values = nullptr;
		this->colids = nullptr;
	}
	if(nrows != this->nrows)
	{
		int32_t* cu_rowptr;
		alloc_dbuf(nrows+1, &cu_rowptr, dev_id);
		// free old buf
		if(this->rowptr != nullptr)
			free_dbuf(this->rowptr);
		// save new one
		this->rowptr = cu_rowptr;
		this->nrows = nrows;
	}
	switch_back();
}

template<typename T>
void cuMatSp<T>::copy(cuMatSp<T>* cmat)
{
	cmat->resize(nnz, this->nrows, this->ncols);
	copy_dbuf2dbuf(nnz, values, cmat->values, dev_id, cmat->dev_id, stream);
	copy_dbuf2dbuf(nnz, colids, cmat->colids, dev_id, cmat->dev_id, stream);
	copy_dbuf2dbuf(this->nrows+1, rowptr, cmat->rowptr, dev_id, cmat->dev_id, stream);
}

template<typename T>
cuMatSp<T>* cuMatSp<T>::clone()
{
	T* values;
	int32_t* colids;
	int32_t* rowptr;
	auto nrows_plus_1 = this->nrows+1;
	alloc_dbuf(nnz, &values, dev_id);
	alloc_dbuf(nnz, &colids, dev_id);
	auto clone_mat = new cuMatSp<T>(this->nrows, this->ncols);
	clone_mat->values = values;
	clone_mat->colids = colids;
	clone_mat->nnz = nnz;
	clone_mat->dev_id = dev_id;
	clone_mat->stream = stream;
	copy(clone_mat);
	return clone_mat;
}

template<typename T>
cuMatSp<Real<T>>* cuMatSp<T>::real()
{

	// T is complex or real
	Real<T>* values;
	int32_t* colids;
	int32_t* rowptr;
	auto nrows_plus_1 = this->nrows+1;
	alloc_dbuf(nnz, &values, dev_id);
	alloc_dbuf(nnz, &colids, dev_id);
	auto real_mat = new cuMatSp<Real<T>>(this->nrows, this->ncols);
	real_mat->colids = colids;
	real_mat->nnz = nnz;
	real_mat->dev_id = dev_id;
	real_mat->stream = stream;
	copy_dbuf2dbuf(nnz, this->colids, real_mat->colids, dev_id, real_mat->dev_id, stream);
	copy_dbuf2dbuf(this->nrows+1, this->rowptr, real_mat->rowptr, dev_id, real_mat->dev_id, stream);
	real_values(values);
	real_mat->values = values;
	return real_mat;
}

//template<typename T>
//void cuMatSp<T>::real_values(Real<T>* values)
//{
//	copy_dbuf2dbuf(nnz, this->values, values, dev_id, dev_id, stream);
//}

template<typename T>
void cuMatSp<T>::set_eyes()
{
	auto switch_back = switch_dev(this->dev_id);
	int32_t dlen = this->nrows<this->ncols?this->nrows:this->ncols;
	if(nnz != dlen)
	{
		if(colids != nullptr) free_dbuf(colids);
		if(values != nullptr) free_dbuf(values);
		colids = nullptr;
		values = nullptr;
		nnz = dlen;
	}
	if(colids == nullptr)
		alloc_dbuf(nnz, &colids, dev_id);
	if(values == nullptr)
		alloc_dbuf(nnz, &values, dev_id);
	//TODO: a kernel to put a sequence 0, ..., dlen+1 directly in rowptr and colids
	auto nrowptr = new int32_t[this->nrows+1];
	auto ncolids = new int32_t[dlen];
	nrowptr[0] = 0;
	for(int i=0; i < dlen; i++)
	{
		nrowptr[i+1] = i+1;
		ncolids[i] = i;
	}
	for(int i=dlen;i<this->nrows+1; i++)
	{
		nrowptr[i] = nrowptr[dlen];
	}
	cuMatDs<T> ones(dlen, 1);
	ones.setOnes();
	copy_dbuf2dbuf(dlen, ones.data, values, dev_id, dev_id, stream);
	copy_hbuf2dbuf(dlen, ncolids, colids, dev_id, stream);
	free_dbuf(rowptr);
	alloc_dbuf(this->nrows+1, &rowptr, dev_id);
	copy_hbuf2dbuf(this->nrows+1, nrowptr, rowptr, dev_id, stream);
	delete[] nrowptr;
	delete[] ncolids;
	switch_back();
}

template<typename T>
void cuMatSp<T>::set_row_ids_to_one(size_t *one_row_ids, size_t id_len)
{
	auto switch_back = switch_dev(this->dev_id);
	if(nnz != id_len)
	{
		if(colids != nullptr) free_dbuf(colids);
		if(values != nullptr) free_dbuf(values);
		colids = nullptr;
		values = nullptr;
		nnz = id_len;
	}
	this->ncols = id_len;
	if(colids == nullptr)
		alloc_dbuf(nnz, &colids, dev_id);
	if(values == nullptr)
		alloc_dbuf(nnz, &values, dev_id);
	auto nrowptr = new int32_t[this->nrows+1];
	auto ncolids = new int32_t[id_len];
	// one_row_ids come in column natural order (0 to id_len-1)
	std::iota(ncolids, ncolids+id_len, 0);
	// sort the column order to fill the buffer in the row order
	std::sort(ncolids, ncolids+id_len, [&one_row_ids](int32_t a, int32_t b)
			{
				return one_row_ids[a] < one_row_ids[b];
			});
	// sort row indices in ascending order
	std::sort(one_row_ids, one_row_ids+id_len);
	nrowptr[0] = 0;
	int j = 0;
	for(int i=1; i < this->nrows+1; i++)
	{
		nrowptr[i] = nrowptr[i-1];
		while(i-1 == one_row_ids[j])
		{
			nrowptr[i]++;
			j++;
		}
	}
	cuMatDs<T> ones(id_len, 1);
	ones.setOnes();
	copy_dbuf2dbuf(id_len, ones.data, values, dev_id, dev_id, stream);
	copy_hbuf2dbuf(id_len, ncolids, colids, dev_id, stream);
	free_dbuf(rowptr);
	alloc_dbuf(this->nrows+1, &rowptr, dev_id);
	copy_hbuf2dbuf(this->nrows+1, nrowptr, rowptr, dev_id, stream);
	delete[] nrowptr;
	delete[] ncolids;
	switch_back();
}

template<typename T>
void cuMatSp<T>::set_col_ids_to_one(size_t *one_col_ids, size_t id_len)
{
	auto switch_back = switch_dev(this->dev_id);
	if(nnz != id_len)
	{
		if(colids != nullptr) free_dbuf(colids);
		if(values != nullptr) free_dbuf(values);
		colids = nullptr;
		values = nullptr;
		nnz = id_len;
	}
	this->nrows = id_len;
	if(colids == nullptr)
		alloc_dbuf(nnz, &colids, dev_id);
	if(values == nullptr)
		alloc_dbuf(nnz, &values, dev_id);
	auto nrowptr = new int32_t[this->nrows+1];
	auto ncolids = new int32_t[nnz];
	nrowptr[0] = 0;
	for(int i=1; i < this->nrows+1; i++)
	{
		nrowptr[i] = nrowptr[i-1] + 1;
		ncolids[i-1] = one_col_ids[i];
	}
	cuMatDs<T> ones(id_len, 1);
	ones.setOnes();
	copy_dbuf2dbuf(id_len, ones.data, values, dev_id, dev_id, stream);
	copy_hbuf2dbuf(id_len, ncolids, colids, dev_id, stream);
	free_dbuf(rowptr);
	alloc_dbuf(this->nrows+1, &rowptr, dev_id);
	copy_hbuf2dbuf(this->nrows+1, nrowptr, rowptr, dev_id, stream);
	delete[] nrowptr;
	delete[] ncolids;
	switch_back();
}

template<typename T>
void cuMatSp<T>::set_zeros()
{
	//TODO: do something cleaner
	auto switch_back = switch_dev(this->dev_id);
//	if(nnz < this->nrows+1)
//	{
	if(nnz > 0)
	{
		cuMatDs<T> zeros(nnz, 1);
		zeros.set_zeros();
		copy_dbuf2dbuf(nnz, zeros.data, values, dev_id, dev_id);
	}
//	}
//	else // can't do this because (at least) it fails the norm
//	{
//		int32_t* zeros = new int32_t[this->nrows+1];
//		bzero(zeros, this->nrows+1*sizeof(int32_t));
//		copy_hbuf2dbuf(this->nrows+1, zeros, rowptr, dev_id, stream);
//		delete zeros;
//	}
	switch_back();
}

template<typename T>
void cuMatSp<T>::mul(const T& scalar)
{
	// rely on dense mat to mul nonzeros by a scalar
	cuMatDs<T> dsm(nnz, 1, values);
	dsm.mul(scalar);
	dsm.data = nullptr;
}


template<typename T>
bool cuMatSp<T>::is_equal(const cuMatSp<T>* o/*ther*/) const
{
	return rowptr == o->rowptr && colids == o->colids && values == o->values \
				   && nnz == o->nnz && this->nrows == o->nrows && this->ncols == o->ncols \
				   && descr == o->descr;
}

template<typename T>
void cuMatSp<T>::cpu_set(const int32_t nnz, const int32_t nrows, const int32_t ncols, const T* values, const int32_t* rowptr, const int32_t* colids)
{
	if(this->nnz != nnz || this->nrows != nrows || this->ncols != ncols)
		resize(nnz, nrows, ncols);
	copy_hbuf2dbuf(nnz, values, this->values, dev_id, stream);
	copy_hbuf2dbuf(nnz, colids, this->colids, dev_id, stream);
	copy_hbuf2dbuf(nrows+1, rowptr, this->rowptr, dev_id, stream);
}

#if (CUDA_VERSION > 9200)
template<typename T>
cusparseStatus_t spm_mat2spdesc(const cuMatSp<T>& A, cusparseSpMatDescr_t& spMatDescr)
{
	cusparseStatus_t status = cusparseCreateCsr(&spMatDescr,
			A.nrows,
			A.ncols,
			A.nnz,
			(void*)A.rowptr,
			(void*)A.colids,
			(void*)A.values,
			CUSPARSE_INDEX_32I, // cuda-11.4/targets/x86_64-linux/include/cusparse.h
			CUSPARSE_INDEX_32I,
			CUSPARSE_INDEX_BASE_ZERO,
			type2cudaDataType(*A.values));
	return status;
}
#endif

