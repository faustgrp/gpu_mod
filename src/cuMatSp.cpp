#include "cuMatDs.h"
#include "cuMatSp.h"
#include "kernels.h"

template<>
void cuMatSp<float>::add(const float& scalar)
{
	kernel_add_const(values, scalar, nnz);
}

template<>
void cuMatSp<double>::add(const double& scalar)
{
	kernel_add_const(values, scalar, nnz);
}

template<>
void cuMatSp<float2>::add(const float2& scalar)
{
	kernel_add_const_cplx(values, scalar, nnz);
}

template<>
void cuMatSp<double2>::add(const double2& scalar)
{
	kernel_add_const_cplx(values, scalar, nnz);
}

template<>
void cuMatSp<float>::sub(const float& scalar)
{
	kernel_sub_const(values, scalar, nnz);
}

template<>
void cuMatSp<double>::sub(const double& scalar)
{
	kernel_sub_const(values, scalar, nnz);
}

template<>
void cuMatSp<float2>::sub(const float2& scalar)
{
	kernel_sub_const_cplx(values, scalar, nnz);
}

template<>
void cuMatSp<double2>::sub(const double2& scalar)
{
	kernel_sub_const_cplx(values, scalar, nnz);
}

template<>
void cuMatSp<double2>::real_values(double* values)
{
	kernel_cplx2real(this->values, values, this->get_nnz());
}

template<>
void cuMatSp<float2>::real_values(float* values)
{
	kernel_cplx2real(this->values, values, this->get_nnz());
}

template<>
void cuMatSp<float>::real_values(float* values)
{
	copy_dbuf2dbuf(nnz, this->values, values, dev_id, dev_id, stream);
}

template<>
void cuMatSp<double>::real_values(double* values)
{
	copy_dbuf2dbuf(nnz, this->values, values, dev_id, dev_id, stream);
}
