#ifndef __COMPLEX__
#define __COMPLEX__
#include <complex>
template<typename T>
struct _STL
{
};
template<typename U>
using STL = typename _STL<U>::stl_type;
#include "STL_scalar_types.hpp"
#endif
