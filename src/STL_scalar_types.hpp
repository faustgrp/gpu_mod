template<>
struct _STL<float>
{
	using stl_type = float;
};


template<>
struct _STL<double>
{
	using stl_type = double;
};


template<>
struct _STL<float2>
{
	using stl_type = std::complex<float>;
};


template<>
struct _STL<double2>
{
	using stl_type = std::complex<double>;
};
