template<typename T>
void alloc_dbuf(int32_t buf_len, T** dbuf, int32_t dev_id/*=-1*/)
{
#ifdef DEBUG
		std::cout << "alloc_dbuf size: " << buf_len*sizeof(T) << std::endl;
#endif
	auto switch_back = switch_dev(dev_id);
	auto ret = cudaMalloc(reinterpret_cast<void **>(dbuf), (buf_len * sizeof(T)));
	if (ret != cudaSuccess)
		throw std::runtime_error("!!!! (in alloc_dbuf) cudaMalloc error: "+cuda_error_int2str(ret));
#ifdef DEBUG
	std::cout << "(" << *dbuf << ")" << std::endl;
#endif
#ifdef DEBUG
	MemoryMonitor::memory[*dbuf] = buf_len*sizeof(T);
	MemoryMonitor::alloc_memory += buf_len*sizeof(T);
	std::cout << "MemoryMonitor::alloc_memory, total allocated MemoryMonitor::memory: " << MemoryMonitor::alloc_memory << "(old amount is: " << MemoryMonitor::alloc_memory-buf_len*sizeof(T) << ") ptr:" << *dbuf  << " size:" << buf_len*sizeof(T) << std::endl;
#endif
	// restore current device
	switch_back();
}

template<typename T>
void free_dbuf(T *dmat)
{
	auto ret = cudaFree(dmat); // according to the documentation, dmat == 0/nullptr leads to a no-operation // https://docs.nvidia.com/cuda/archive/9.2/cuda-runtime-api/group__CUDART__MEMORY.html
//	check_cuda_error(ret, "cudaFree");
#ifdef DEBUG
	if(MemoryMonitor::memory.find(dmat) != MemoryMonitor::memory.end())
	{
		MemoryMonitor::alloc_memory -= MemoryMonitor::memory[dmat];
		std::cout << "free_dbuf, total allocated MemoryMonitor::memory: " << MemoryMonitor::alloc_memory << " (freed: " << dmat << ", " << MemoryMonitor::memory[dmat] << ")" << std::endl;
		MemoryMonitor::memory.erase(dmat);
		std::cout << "buffers left to deallocate: " << MemoryMonitor::memory.size() << std::endl;
		if(MemoryMonitor::memory.size() < 100)
			for(auto e: MemoryMonitor::memory)
				std::cout << e.first << " ";
		std::cout << std::endl;
	}
#endif
}

template<typename T>
void copy_hbuf2dbuf(const int32_t hbuf_len, const T* hbuf, T *dbuf, const int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	// use cudaMemcpyAsync instead of cublasSetMatrix or cublasSetVector (which doesn't allow to select a stream)
	auto switch_back = switch_dev(dev_id);
//	std::cout << "stream: " << stream << " cur_dev " << cur_dev() << std::endl;
	auto ret = cudaMemcpyAsync(dbuf, hbuf, sizeof(T)*hbuf_len, cudaMemcpyHostToDevice, static_cast<cudaStream_t>(stream));
	check_cuda_error(ret, "cudaMemcpyAsync");
	// restore current device
	switch_back();
}

template<typename T>
void copy_dbuf2dbuf(const int32_t dbuf_len, const T* src_dbuf, T* dst_dbuf, int32_t src_dev_id/*=-1*/, int32_t dst_dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	if(src_dev_id == -1)
		src_dev_id = cur_dev();
	if(dst_dev_id == -1)
		dst_dev_id = cur_dev();
	auto ret = cudaMemcpyPeerAsync(dst_dbuf, dst_dev_id, src_dbuf, src_dev_id, dbuf_len*sizeof(T), static_cast<cudaStream_t>(stream));
	check_cuda_error(ret, "cudaMemcpyPeerAsync");
}

template<typename T>
void copy_dbuf2hbuf(const int32_t dbuf_len, const T* dbuf, T* hbuf, const int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	//use cudaMemcpyAsync instead of cublasGetVector (the latter doesn't allow to select a stream)
	auto switch_back = switch_dev(dev_id);
	auto ret = cudaMemcpyAsync(hbuf, dbuf, sizeof(T)*dbuf_len, cudaMemcpyDeviceToHost, static_cast<cudaStream_t>(stream));
	check_cuda_error(ret, "cudaMemcpyAsync");
	// restore current device
	switch_back();
}

// see less tricky functions defined by template specialization in *.cpp.in
//template<typename T>
//void set_one(T* scal)
//{
//	float scal_one_f = 1.f;
//	double scal_one_d = 1.d;
//	size_t half_size = sizeof(T)>>1;
//	bzero(scal, sizeof(T));
//	if(std::is_same<T,float>::value)
//	{
//		memcpy(scal, &scal_one_f, sizeof(T));
//		return;
//	}
//	else if(std::is_same<T,double>::value)
//	{
//		memcpy(scal, &scal_one_d, sizeof(T));
//		return;
//	}
//	/*else */if(std::is_same<cuComplex, T>::value)
//	{
//		memcpy(scal, &scal_one_f, half_size);
//	}
//	else if(std::is_same<cuDoubleComplex, T>::value)
//		memcpy(scal, &scal_one_d, half_size);
////	bzero(scal+half_size, half_size);
//}
#ifndef _MSC_VER
template<typename T>
bool operator!=(const T& a, const T& b)
{
	return ! (a == b);
}
#endif

#if (CUDA_VERSION > 9200)
template<typename T>
cusparseStatus_t helper_cusparseSpMM(cuMatSp<T> &spm, cuMatDs<T> &dsm,
		cusparseOperation_t sp_op, cusparseOperation_t ds_op,
		const T& alpha, const T& beta, cuMatDs<T> &out, const std::string& err_tag)
{

	// cusparseSpMM doesn't handle ds_op == CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE, and manage only sp_op CUSPARSE_OPERATION_NON_TRANSPOSE // an error will be raised
	cusparseDnMatDescr_t dense_mat_desc = nullptr, output_mat_desc = nullptr;
	cusparseSpMatDescr_t sp_mat_desc = nullptr;
	cusparseStatus_t status = dsm_mat2desc(dsm, dense_mat_desc);
	size_t bufferSize;
	void* externalBuffer;
	if(CUSPARSE_STATUS_SUCCESS != status)
		std::cerr << "callee: "+err_tag+" helper_cusparseSpMM dsm_mat2desc error (dense_mat_desc)." << std::endl;
	status = spm_mat2spdesc(spm, sp_mat_desc);
	if(CUSPARSE_STATUS_SUCCESS != status)
		std::cerr << "callee: "+err_tag+" helper_cusparseSpMM spm_mat2desc error (sp_mat_desc)." << std::endl;
	status = dsm_mat2desc(out, output_mat_desc);
	if(CUSPARSE_STATUS_SUCCESS != status)
		std::cerr << "callee: "+err_tag+" helper_cusparseSpMM dsm_mat2desc error (output_mat_desc)." << std::endl;
	status = cusparseSpMM_bufferSize(spm.handle, sp_op, ds_op, (void*)&alpha, sp_mat_desc, dense_mat_desc, (void*)&beta, output_mat_desc, type2cudaDataType(alpha), CUSPARSE_SPMM_CSR_ALG2, &bufferSize);
	if(status != CUSPARSE_STATUS_SUCCESS)
		throw std::runtime_error("callee: "+err_tag+" helper_cusparseSpMM > cusparseSpMM_bufferSize error: " + std::to_string(status));
	auto ret = cudaMalloc(reinterpret_cast<void **>(&externalBuffer), bufferSize);
	if (ret != cudaSuccess)
		throw std::runtime_error("!!!! callee: "+err_tag+" (helper_cusparseSpMM) cudaMalloc error: "+cuda_error_int2str(ret));
	status = cusparseSpMM(spm.handle, sp_op, ds_op, (void*)&alpha, sp_mat_desc, dense_mat_desc, (void*)&beta, output_mat_desc, type2cudaDataType(alpha), CUSPARSE_SPMM_CSR_ALG2, externalBuffer);
	cudaFree(externalBuffer);
	if(status != CUSPARSE_STATUS_SUCCESS)
	{
		throw std::runtime_error("cuMatArray<T>::chain_matmul_l2r status=" + std::to_string(status));
	}
	cusparseDestroyDnMat(dense_mat_desc);
	cusparseDestroyDnMat(output_mat_desc);
	cusparseDestroySpMat(sp_mat_desc);
	return status; // only if cudaSuccess
}
#endif
