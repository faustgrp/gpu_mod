#ifndef __PROXIMITY_OP_H__
#define __PROXIMITY_OP_H__
#include <thrust/detail/config.h>
#include <thrust/device_ptr.h>
#include <thrust/transform.h>
#include <thrust/sort.h>
#include <thrust/iterator/counting_iterator.h>

template<typename T>
void prox_sp(T* data, int32_t dlen, int32_t k, int32_t dev_id=-1, cudaStream_t stream=nullptr, bool verbose=false);

template<typename T>
void prox_sp_copy(T* in_data, T* out_data, int *indices, int k);

template<typename T>
void prox_spcol(T* data, int32_t dlen, int32_t ncols, int32_t k, int32_t dev_id=-1, bool verbose=false);

template<typename T>
void prox_pos(T* data, int32_t dlen);

#endif
