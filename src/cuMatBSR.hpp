template<typename T> cusparseHandle_t cuMatBSR<T>::handle = 0;

template<typename T>
size_t cuMatBSR<T>::get_nnz() const
{
	return bnnz*bnrows*bncols;
}

template<typename T>
size_t cuMatBSR<T>::get_nbytes() const
{

	return bnrows*bncols*bnnz*sizeof(T) /* data byte size */ + sizeof(int32_t) * (bnnz + nblocks_per_row_dim+1) /* indices byte size */;
}

template<typename T>
size_t cuMatBSR<T>::get_bnrows() const
{

	return bnrows;
}

template<typename T>
size_t cuMatBSR<T>::get_bncols() const
{

	return bncols;
}

template<typename T>
size_t cuMatBSR<T>::get_bnnz() const
{

	return bnnz;
}

template<typename T>
void cuMatBSR<T>::transpose()
{
	// TODO: maybe a dedicated kernel operating the transposition without conversion would be faster
	auto mat_sp = cuMatSp<T>::bsr2csr(*this);
	mat_sp->transpose();
	auto tbsr = cuMatBSR<T>::csr2bsr(*mat_sp, this->bnrows);
	this->operator=(std::move(*tbsr));
	delete tbsr;
	delete mat_sp;
}

template<typename T>
void cuMatBSR<T>::to_dense(cuMatDs<T>& out, gm_Op op /*= OP_NOTRANSP*/)
{
	auto mat_sp = cuMatSp<T>::bsr2csr(*this);
	cusparse_csr2dense(mat_sp, &out, op);
	delete mat_sp;
}

template<typename T>
cuMatDs<T> cuMatBSR<T>::to_dense(gm_Op op /*= OP_NOTRANSP*/)
{
	int32_t out_nrows, out_ncols;
	if(op == OP_NOTRANSP)
	{
		out_nrows = this->nrows;
		out_ncols = this->ncols;
	}
	else
	{
		out_nrows = this->ncols;
		out_ncols = this->nrows;
	}
	cuMatDs<T> out(out_nrows, out_ncols);
	to_dense(out, op);
	return std::move(out);
}

template<typename T>
cuMatDs<T>* cuMatBSR<T>::bsr2dense(cuMatBSR<T>& bsr, gm_Op op /*= OP_NOTRANSP*/)
{
	auto ds_mat = new cuMatDs<T>(bsr.nrows, bsr.ncols);
	bsr.to_dense(*ds_mat, op);
	return ds_mat;
}

template<typename T>
void cuMatBSR<T>::adjoint()
{
	// TODO: maybe a dedicated kernel operating the transconjugate without conversion would be faster
//	auto mat_sp = cuMatSp<T>::bsr2csr(*this);
//	mat_sp->adjoint()
//	auto absr = cuMatBSR<T>::csr2bsr(*mat_sp, this->bnrows);
//	this->operator=(std::move(*absr));
//	delete absr;
//	delete mat_sp; // this code fails for complex matrices when copying to cpu (copy_dbuf2hbuf cuda error)
	this->conjugate();
	this->transpose();
}

template<typename T>
void cuMatBSR<T>::conjugate()
{
	cuMatDs<T> ds_mat(this->bnrows, this->bncols*this->bnnz, bdata);
	ds_mat.conjugate();
	ds_mat.data = nullptr; // avoid freeing when out of scope
}

template<typename T>
void cuMatBSR<T>::mul(const T& scalar)
{
	cuMatDs<T> ds_mat(this->bnrows, this->bncols*this->bnnz, bdata);
	ds_mat.mul(scalar);
	ds_mat.data = nullptr; // avoid freeing when out of scope
}



template<typename T>
cuMatDs<T>* cuMatBSR<T>::mul(cuMatDs<T> & other,
		cuMatDs<T>* output, const gm_Op op_this /*= OP_NOTRANSP*/,
		const gm_Op op_other /*= OP_NOTRANSP*/,
		const T* alpha/*= nullptr*/,
		const T* beta/*= nullptr*/)
{
#if (CUDA_VERSION <= 11000)
	throw std::runtime_error("cuMatBSR::mul is not supported on CUDA version < 11");
#else
	auto cusparse_op_this = gm_Op2cusparse(op_this);
	auto cusparse_op_other = gm_Op2cusparse(op_other);

	// do the transpose/adjoint for this separately because cusparseTbsrmm doesn't support it (exception raised)
	if(op_this != OP_NOTRANSP)
	{
		auto new_this = this->clone();
		if(op_this == OP_TRANSP)
			new_this->transpose();
		else // op_this == OP_CONJTRANSP
			new_this->adjoint();
		output = new_this->mul(other, output, OP_NOTRANSP, op_other, alpha, beta);
		delete new_this;
		return output;
	}
	// start from here op_this == OP_NOTRANSP

	// do the adjoint separately for other because cusparseTbsrmm doesn't support it (exception raised)
	// edit: contrary to what it's said in the doc (https://docs.nvidia.com/cuda/archive/11.4.2/cusparse/index.html)
	// edit: OP_TRANSP doesn't work as well for other (or maybe there is an error here, but doing the transpose before
	// cusparseTbsrmm fixes the discrepancies).
	if(op_other != OP_NOTRANSP)
	{
		auto new_other = other.clone();
		if(op_other == OP_TRANSP)
			new_other->transpose();
		else // op_other == OP_CONJTRANSP
			new_other->adjoint();
		output = mul(*new_other, output, OP_NOTRANSP, OP_NOTRANSP, alpha, beta);
		delete new_other;
		return output;
	}

	int n, ldb, ldc;
	T _alpha, _beta;
	int output_nrows, output_ncols;

	if(alpha == nullptr)
		set_one(&_alpha);
	else
		memcpy(&_alpha, alpha, sizeof(T));
	if(beta == nullptr)
		bzero(&_beta, sizeof(T));
	else
		memcpy(&_beta, beta, sizeof(T));

	if (op_other == OP_NOTRANSP)
	{
		n = other.ncols;
		ldb = other.nrows;
	}
	else
	{
		n = other.nrows;
		ldb = other.ncols;
	}
	if(op_this == OP_NOTRANSP)
		ldc  = this->nrows;
	else
		ldc = this->ncols;

	if(op_this == OP_NOTRANSP)
		output_nrows = this->nrows;
	else
		output_ncols = this->ncols;

	if(op_other == OP_NOTRANSP)
		output_ncols = other.ncols;
	else
		output_ncols = other.nrows;

	if(nullptr == output)
		output = cuMatDs<T>::create(output_nrows, output_ncols);
	else
		if(output->nrows != output_nrows || output->ncols != output_ncols)
			throw std::runtime_error("error: cuMatSp<T>::mul(cuMatDs<T>), dimensions must agree.");

	cusparseStatus_t status;
	if(bnrows == 1 && bncols == 1)
		// display a more comprehensive message for this error (cuSPARSE is not so well documented)
		throw std::runtime_error("cuMatBSR::mul(cuMatDs, cuMatDs, gm_Op, gm_Op) > cusparseTbsrmm/v error: 1x1 blocksize is not supported.");
	if(op_other == OP_NOTRANSP && other.ncols == 1)
	{
		// BSR-vec mul
		status = cusparseTbsrmv(this->handle, CUSPARSE_DIRECTION_COLUMN, cusparse_op_this, nblocks_per_row_dim, nblocks_per_col_dim, bnnz, &_alpha, descr, bdata, browptr, bcolids, bnrows/* == bncols*/, other.data, &_beta, output->data);
	}
	else
	{
		// BSR-mat mul
		status = cusparseTbsrmm(this->handle, CUSPARSE_DIRECTION_COLUMN, cusparse_op_this, cusparse_op_other,
				nblocks_per_row_dim, n, nblocks_per_col_dim,  bnnz, &_alpha, descr, bdata, browptr, bcolids,
				bnrows /* == bncols*/, other.data, ldb, &_beta, output->data, ldc);
	}

	check_cusparse_error(status, "cuMatBSR::mul(cuMatDs, cuMatDs, gm_Op, gm_Op) > cusparseTbsrmm/v");
	return output;
#endif
}

template<typename T>
cuMatBSR<T>::cuMatBSR(): cuMat<T>(), bnrows(0), bncols(0), bdata(nullptr), browptr(nullptr), bcolids(nullptr), stream(nullptr), bnnz(0), dev_id(-1)
{
}

template<typename T>
cuMatBSR<T>::cuMatBSR(int32_t nrows, int32_t ncols, int32_t bnrows, int32_t bncols, T* dm_bdata, int32_t* dm_browptr, int32_t* dm_bcolids, int32_t bnnz, cusparseMatDescr_t descr, int dev_id/*=-1*/, void* stream/*=nullptr*/) : cuMatBSR()
{
	this->nrows = nrows;
	this->ncols = ncols;
	this->bnrows = bnrows;
	this->bncols = bncols;
	this->bdata = dm_bdata;
	this->browptr = dm_browptr;
	this->bcolids = dm_bcolids;
	this->bnnz = bnnz;
	this->descr = descr;
	this->nblocks_per_row_dim = nrows/bnrows;
	this->nblocks_per_col_dim = ncols/bncols;
	if(bnrows != bncols)
		throw std::runtime_error("CUDA handles only square data block.");
	if(! handle)
		cusparseCreate(&handle);
	this->dev_id = dev_id;
	this->stream = stream;
}

template<typename T>
void cuMatBSR<T>::init_desc(cusparseMatDescr_t &desc)
{
	//TODO: refactor with cuMatSp::init_desc
	/* create and setup matrix descriptor */
	auto status = cusparseCreateMatDescr(&desc);
	check_cusparse_error(status, "cuMatBSR<T>::init_desc cusparseCreateMatDescr");
	cusparseSetMatType(desc,CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(desc,CUSPARSE_INDEX_BASE_ZERO);
}

template<typename T>
cuMatBSR<T>* cuMatBSR<T>::create(int32_t nrows, int32_t ncols, int32_t bnrows, int32_t bncols, const T* bdata, const int32_t* browptr, const int32_t* bcolids, int32_t bnnz, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
//	std::cout << "cuMatBSR<T>::create nrows: " << nrows << " ncols: " << ncols << " values:" << values << " rowptr: " << rowptr << " colids: " << colids << " nnz:" << nnz << std::endl;
	cuMatBSR<T>* cu_mat = nullptr;
	int32_t *dm_browptr, *dm_bcolids;
	T *dm_bdata;
	cusparseMatDescr_t descr;
//	std::cout << "nnz=" << nnz << std::endl;
	int32_t nblocks_per_row_dim = nrows/bnrows;
	int32_t nnz = bnnz*bnrows*bncols;
	if(nnz > 0)
	{
		alloc_dbuf(nnz, &dm_bdata, dev_id);
		alloc_dbuf(bnnz, &dm_bcolids, dev_id);
		copy_hbuf2dbuf(nnz, bdata , dm_bdata, dev_id, stream);
		copy_hbuf2dbuf(bnnz, bcolids, dm_bcolids, dev_id, stream);
	}
	else
	{
		dm_bdata = nullptr;
		dm_bcolids = nullptr;
	}
	alloc_dbuf(nblocks_per_row_dim+1, &dm_browptr, dev_id);
	copy_hbuf2dbuf(nblocks_per_row_dim+1, browptr, dm_browptr, dev_id, stream);
	Scalar type = type2Scalar<T>();
	init_desc(descr);
	cu_mat = new cuMatBSR<T>(nrows, ncols, bnrows, bncols, dm_bdata, dm_browptr, dm_bcolids, bnnz, descr, dev_id, stream);
	return cu_mat;
}

template<typename T>
void bsr_tocpu(gm_BSRMat_t cu_mat, int32_t *browptr, int32_t *bcolids, T* bdata, int32_t *nrows, int32_t *ncols, int32_t *bnrows, int32_t *bncols, int32_t *bnnz)
{
	cuMatBSR<T>* cu_bsr = static_cast<cuMatBSR<T>*>(cu_mat);
	//TODO: check matrix type
//	if(!cu_bsr->is_sparse || ! cu_bsr->is_cuda)
//		throw std::runtime_error("spm_tocpu error: matrix is not sparse or not cuda");
//	std::cout << "size=" << cu_bsr->nblocks_per_row_dim+1 << " cu_bsr->browptr: " << cu_bsr->browptr << " browptr" << browptr << "dev_id:" << cu_bsr->dev_id << " stream:" << cu_bsr->stream <<std::endl;
	copy_dbuf2hbuf(cu_bsr->nblocks_per_row_dim+1, cu_bsr->browptr, browptr, cu_bsr->dev_id, cu_bsr->stream);
	copy_dbuf2hbuf(cu_bsr->bnnz, cu_bsr->bcolids, bcolids, cu_bsr->dev_id, cu_bsr->stream);
	copy_dbuf2hbuf(cu_bsr->bnnz*cu_bsr->bnrows*cu_bsr->bncols, cu_bsr->bdata, bdata, cu_bsr->dev_id, cu_bsr->stream);
	if(nullptr != nrows)
		*nrows = cu_bsr->nrows;
	if(nullptr != ncols)
		*ncols = cu_bsr->ncols;
	if(nullptr != bnrows)
		*bnrows = cu_bsr->bnrows;
	if(nullptr != bncols)
		*bncols = cu_bsr->bncols;
	if(nullptr != bnnz)
		*bnnz = cu_bsr->bnnz;
}

template<typename T>
void bsr_get_info(gm_BSRMat_t cu_mat, int32_t *nrows, int32_t *ncols, int32_t *bnrows, int32_t *bncols, int32_t *bnnz)
{
	cuMatBSR<T>* cu_bsr = static_cast<cuMatBSR<T>*>(cu_mat);
	//TODO: check matrix type
//	if(!cu_bsr->is_sparse || ! cu_bsr->is_cuda)
//		throw std::runtime_error("spm_tocpu error: matrix is not sparse or not cuda");
	if(nullptr != nrows)
		*nrows = cu_bsr->nrows;
	if(nullptr != ncols)
		*ncols = cu_bsr->ncols;
	if(nullptr != bnrows)
		*bnrows = cu_bsr->bnrows;
	if(nullptr != bncols)
		*bncols = cu_bsr->bncols;
	if(nullptr != bnnz)
		*bnnz = cu_bsr->bnnz;
}

template<typename T>
void cuMatBSR<T>::destroy()
{
	//TODO
}


template<typename T>
cuMatBSR<T>* cuMatBSR<T>::csr2bsr(cuMatSp<T>& mat_sp, int32_t bsize, int dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	auto csrRowPtrA = mat_sp.rowptr;
	auto csrColIndA = mat_sp.colids;
	auto csrValA = mat_sp.values;
	auto descrA = mat_sp.descr;
	auto m = mat_sp.nrows;
	auto n = mat_sp.ncols;
	int blockDim = bsize;
	if(dev_id == -1)
		dev_id = mat_sp.dev_id;
	if(stream == nullptr)
		stream = mat_sp.stream;

	int32_t *bsrRowPtrC, *bsrColIndC;
	T *bsrValC;
	cusparseMatDescr_t descrC;
	init_desc(descrC);

	//cf. https://docs.nvidia.com/cuda/archive/11.4.4/cusparse/index.html#csr2bsr
	// Given CSR format (csrRowPtrA, csrcolIndA, csrValA) and
	// blocks of BSR format are stored in column-major order.
	cusparseDirection_t dir = CUSPARSE_DIRECTION_COLUMN;
	int base, nnzb;
	int mb = (m + blockDim-1)/blockDim;
//	cudaMalloc((void**)&bsrRowPtrC, sizeof(int) *(mb+1));
	alloc_dbuf(mb+1, &bsrRowPtrC, dev_id);
	// nnzTotalDevHostPtr points to host memory
	int *nnzTotalDevHostPtr = &nnzb;
	cusparseXcsr2bsrNnz(handle, dir, m, n,
			descrA, csrRowPtrA, csrColIndA,
			blockDim,
			descrC, bsrRowPtrC,
			nnzTotalDevHostPtr);
	if (NULL != nnzTotalDevHostPtr){
		nnzb = *nnzTotalDevHostPtr;
	}else{
//		cudaMemcpy(&nnzb, bsrRowPtrC+mb, sizeof(int), cudaMemcpyDeviceToHost);
		copy_dbuf2hbuf(1, bsrRowPtrC+mb, &nnzb, dev_id, stream);
//		cudaMemcpy(&base, bsrRowPtrC, sizeof(int), cudaMemcpyDeviceToHost);
		copy_dbuf2hbuf(1, bsrRowPtrC, &base, dev_id, stream);
		nnzb -= base;
	}
//	cudaMalloc((void**)&bsrColIndC, sizeof(int)*nnzb);
	alloc_dbuf(nnzb, &bsrColIndC, dev_id);
//	cudaMalloc((void**)&bsrValC, sizeof(float)*(blockDim*blockDim)*nnzb);
	alloc_dbuf(nnzb*blockDim*blockDim, &bsrValC, dev_id);
	auto status = cusparseTcsr2bsr(handle, dir, m, n,
			descrA,
			csrValA, csrRowPtrA, csrColIndA,
			blockDim,
			descrC,
			bsrValC, bsrRowPtrC, bsrColIndC);

	check_cusparse_error(status, "cuMatBSR::csr2bsr(cuMatSp, cuMatBSR) > cusparseTcsr2bsr");

	auto bsr_mat = new cuMatBSR<T>(m, n, blockDim, blockDim, bsrValC, bsrRowPtrC, bsrColIndC, nnzb, descrC, dev_id, stream);
	return bsr_mat;
}

template<typename T>
	cuMatBSR<T>::cuMatBSR(const cuMatBSR<T>& other)
{
	*this = other; 
}

template<typename T>
	cuMatBSR<T>::cuMatBSR(cuMatBSR<T>&& other)
{
	*this = std::move(other);
}

template<typename T>
void cuMatBSR<T>::copy_attrs(const cuMatBSR<T>& other, bool including_dev_id/*=true*/)
{
	descr = other.descr;
	bnnz = other.bnnz;
	bnrows = other.bnrows;
	bncols = other.bncols;
	nblocks_per_row_dim = other.nblocks_per_row_dim;
	nblocks_per_col_dim = other.nblocks_per_col_dim;
	handle = other.handle;
	if(including_dev_id)
		dev_id = other.dev_id;
	stream = other.stream;
	Mat::operator=(other);
}

template<typename T>
	void cuMatBSR<T>::copy_to(cuMatBSR<T>& dest) const
{
	dest.copy_attrs(*this, /* keep the device of dest */ false);
	dest.free_bufs();
	int32_t nnz = bnnz*bnrows*bncols;
	alloc_dbuf(nnz, &dest.bdata, dest.dev_id);
	alloc_dbuf(bnnz, &dest.bcolids, dest.dev_id);
	alloc_dbuf(nblocks_per_row_dim+1, &dest.browptr, dest.dev_id);
	copy_dbuf2dbuf(nnz, bdata, dest.bdata, dev_id, dest.dev_id, dest.stream);
	copy_dbuf2dbuf(bnnz, bcolids, dest.bcolids, dev_id, dest.dev_id, dest.stream);
	copy_dbuf2dbuf(nblocks_per_row_dim+1, browptr, dest.browptr, dev_id, dest.dev_id, dest.stream);
}

template<typename T>
	cuMatBSR<T>& cuMatBSR<T>::operator=(const cuMatBSR<T>& other)
{
	other.copy_to(*this);
	return *this;
}

template<typename T>
	cuMatBSR<T>& cuMatBSR<T>::operator=(cuMatBSR<T>&& other)
{
	copy_attrs(other);
	free_bufs();
	browptr = other.browptr;
	bcolids = other.bcolids;
	bdata = other.bdata;
	other.bcolids = other.browptr = nullptr;
	other.bdata = nullptr;
	return *this;
}

template<typename T>
Real<T> cuMatBSR<T>::norm_frob() const
{
	cuMatDs<T> ds_mat(this->bnrows, this->bncols*this->bnnz, bdata);
	auto norm = ds_mat.norm_frob();
	ds_mat.data = nullptr; // avoid freeing when out of scope
	return norm;
}

template<typename T>
void cuMatBSR<T>::set_zeros()
{
	//	this way causes errors when using other functions
	//	auto zero = new cuMatBSR();
	//	zero->nrows = this->nrows;
	//	zero->ncols = this->ncols;
	//	zero->bnrows = this->bnrows;
	//	zero->bncols = this->bncols;
	//	this->operator=(std::move(*zero));
	//	delete zero;
	this->bnnz = 0;
}



template<typename T>
cuMatBSR<T>* cuMatBSR<T>::clone(const int32_t dev_id/*=-1*/, const void* stream/*=nullptr*/)
{
	auto mat_bsr = new cuMatBSR<T>();
	mat_bsr->dev_id = dev_id;
	mat_bsr->stream = const_cast<void*>(stream);
    this->copy_to(*mat_bsr);
	return mat_bsr;
}

template<typename T>
void cuMatBSR<T>::free_bufs()
{
	if(browptr != nullptr)
		free_dbuf(browptr);
	if(bcolids != nullptr)
		free_dbuf(bcolids);
	if(bdata != nullptr)
		free_dbuf(bdata);
	browptr = bcolids = nullptr;
	bdata  = nullptr;
}

template<typename T>
cuMatBSR<T>::~cuMatBSR()
{
	free_bufs();
}
