#ifndef __GPU_MOD_MAT__
#define __GPU_MOD_MAT__
#include "ScalarType.h"

/**
 *	General matrix class.
 */
struct Mat
{
	int32_t nrows;
	int32_t ncols;
	Mat(int32_t, int32_t);
	Mat(): nrows(0), ncols(0) {};
	virtual void destroy() {}
	virtual ~Mat(){}
	virtual Mat& operator=(const Mat&);
	virtual bool is_sparse() const=0;
	virtual bool is_dense() const=0;
	virtual bool is_csr() const=0;
	virtual bool is_bsr() const=0;
	virtual bool is_cuda() const=0;
	virtual bool is_cpu() const=0;
	virtual Scalar scalar_type() const=0;
};

#endif
