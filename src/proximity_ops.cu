#include "proximity_ops.h"
#include <functional>
#include "kernels.cu"
#include <thrust/detail/config.h>
#include <thrust/device_ptr.h>
#include <thrust/transform.h>
#include <thrust/copy.h>
#include <thrust/sort.h>
#include <thrust/iterator/counting_iterator.h>
#include "Real.h"
#include "kernels.h"
#include <iomanip>

template <typename T>
struct greater_abs
{
	bool __device__ operator()(const T& lhs, const T& rhs)
	{
		if (abs(lhs) > abs(rhs)) return true;
		return false;
	}
};

template <>
struct greater_abs<double2>
{
	bool __device__ operator()(const double2& lhs, const double2& rhs)
	{
		if (abs(lhs).x > abs(rhs).x) return true;
		return false;
	}
};

template <>
struct greater_abs<float2>
{
	bool __device__ operator()(const float2& lhs, const float2& rhs)
	{
		if (abs(lhs).x > abs(rhs).x) return true;
		return false;
	}
};

template<typename T>
struct copy_at
{
	const T* in_data;
	T* out_data;
	int* indices;
	__host__ __device__
		// even if the bool ret. type is useless here, that's the thrust contract
		int operator()(int i)
		{
			// trick the transform operator by assigning the output buffer manually
			// and returning a useless 0 in indices output buffer
			out_data[indices[i]] = in_data[i];
			return 0;
		}
};

template<typename T>
void prox_sp_copy(T* in_data, T* out_data, int *indices,  int k)
{
	thrust::counting_iterator<int> ite_indices(0); // virtual list of indices from 0 to k
	copy_at<T> f;
	f.in_data = in_data;
	f.out_data = out_data;
	f.indices = indices;
	thrust::transform(thrust::device, ite_indices, ite_indices + k, indices, f /* for CUDA the lambda fails if reference vars are in closure [&in_data, &out_data](int i){ out_data[i] = in_data[i];}*/);
	//NOTE: don't use a lambda as the one commented because of this error: (I didn't try extended lambda)
	//			//error: The closure type for a lambda ("lambda [](int)->void", defined at /
	//			gpu_mod/src/proximity_ops.cu:59) cannot be used in the template argument type of a __global__ function template instantiation, unless the l
	//			ambda is defined within a __device__ or __global__ function, or the lambda is an 'extended lambda' and the flag --expt-extended-lambda is specified
}


template<typename T>
void prox_sp(T* data, int32_t dlen, int32_t k, int32_t dev_id/*=-1*/, cudaStream_t stream/*=nullptr*/, bool verbose/*=false*/)
{
	T* kg_data; // k greatest values of data
	int* cpu_inds = nullptr;
	T* cpu_data = nullptr;
	// https://thrust.github.io/doc/classthrust_1_1counting__iterator.html
	thrust::counting_iterator<int> ite_indices(0); // virtual list of indices from 0 to dlen-1
	int* indices; // full list of indices to access data (0 to dlen-1)
	if(verbose)
	{
		std::cout << "prox_sp" << std::endl;
		// cpu buffers for debugging
//		cpu_inds = new int[dlen];
//		cpu_data = new T[dlen];
		cudaMallocHost(&cpu_inds, sizeof(int)*dlen);
		cudaMallocHost(&cpu_data, sizeof(T)*dlen);
	}
	// get absolute values
	auto test = cudaMalloc(&kg_data, k*sizeof(T));
	assert(test == CUDA_SUCCESS);
	test = cudaMalloc(&indices, dlen*sizeof(int));
	assert(test == CUDA_SUCCESS);
	if(verbose)
	{
		cudaMemcpyAsync(cpu_data, data, sizeof(T)*dlen, cudaMemcpyDeviceToHost, stream);
		std::cout << "initial matrix (copied to CPU  RAM):" << std::endl;
		for(int i=0;i < dlen; i++)
		{
			std::cout << real(cpu_data[i]) << " ";
		}
		std::cout << std::endl;
	}
	// indices buf
	thrust::copy(thrust::cuda::par.on(stream), ite_indices, ite_indices+dlen, indices);
	//	cudaMemcpyAsync(indices, ite_indices.data(), sizeof(int)*dlen, cudaMemcpyDeviceToDevice, static_cast<cudaStream_t>(stream), stream);
	if(verbose)
	{
		std::cout << "indices from 0 to k=" << k << " (copied to CPU  RAM):" << std::endl;
		cudaMemcpyAsync(cpu_inds, indices, sizeof(int)*dlen, cudaMemcpyDeviceToHost, stream);
		for(int i=0;i < dlen; i++)
		{
			std::cout << cpu_inds[i] << " ";
		}
		std::cout << std::endl;
	}
	// sort
	thrust::sort_by_key(thrust::cuda::par.on(stream), data, data + dlen, indices, greater_abs<T>());
	//	thrust::sort(thrust::cuda::par.on(stream), kg_data, kg_data + dlen);//, thrust::less<T>());
	//	thrust::sort(cpu_data, cpu_data +dlen);
	if(verbose)
	{
		std::cout << "descendingly sorted matrix (copied to CPU  RAM):" << std::endl;
		cudaMemcpyAsync(cpu_data, data, sizeof(T)*dlen, cudaMemcpyDeviceToHost, stream);
		for(int i=0;i < dlen; i++)
		{
			std::cout << real(cpu_data[i]) << " ";
		}
		std::cout << std::endl;
		cudaMemcpyAsync(cpu_inds, indices, sizeof(int)*dlen, cudaMemcpyDeviceToHost, stream);
		std::cout << "sorted indices (copied to CPU RAM):" << std::endl;
		for(int i=0;i < dlen; i++)
		{
			std::cout << cpu_inds[i] << " ";
		}
		std::cout << std::endl;
	}
	// backup the k greatest values before zeroing
	thrust::copy(thrust::cuda::par.on(stream), data, data+k, kg_data);
	// zero the data out
	T zero;
//	bzero(&zero, sizeof(zero));
	memset(&zero, 0, sizeof(zero)); // bzero doesn't work on MS VS
	thrust::fill(thrust::cuda::par.on(stream), data, data+dlen, zero);
	// copy the k greatest values in their original place
	prox_sp_copy(kg_data, data, indices, k);
	if(verbose)
	{
		std::cout << "prox_sp image matrix (copied to CPU RAM):" << std::endl;
		cudaMemcpyAsync(cpu_data, data, sizeof(T)*dlen, cudaMemcpyDeviceToHost, stream);
		for(int i=0;i < dlen; i++)
		{
			std::cout << real(cpu_data[i]) << " ";
		}
		std::cout << std::endl;
		if(cpu_inds != nullptr)
//			delete [] cpu_inds;
			cudaFreeHost(cpu_inds);
		if(cpu_data != nullptr)
//			delete [] cpu_data;
			cudaFreeHost(cpu_data);
	}
	cudaFree(kg_data);
	cudaFree(indices);
}


template<typename T>
void prox_spcol2(T* data, int32_t dlen, int32_t ncols, int32_t k, int32_t dev_id/*=-1*/, bool verbose/*=false*/)
	//NOTE: this function is much slower than prox_spcol (it is kept for comparison)
{
	// repeat the prox_sp "kernel" on each data column
	// better to use streams for this
	int32_t nrows = dlen/ncols;
	// CUDA doesn't limit the number of streams
	// but the memory allocated in prox_sp_ is limited
	// so limit the number of streams
	int32_t nstreams = 4;
	cudaStream_t* streams = new cudaStream_t[nstreams];
	for(int i=0; i < nstreams; i++)
		cudaStreamCreate(&streams[i]);
	for(int j=0; j < ncols; j++)
	{
		prox_sp(data+j*nrows, nrows, k, dev_id, streams[j&3], verbose);
	}
	for(int i=0; i < nstreams; i++)
		cudaStreamDestroy(streams[i]);
	delete [] streams;
}

template<typename T>
void __global__ kernel_prox_spcol(T* data, int32_t dlen, int32_t nrows, int32_t ncols, int32_t k, int32_t dev_id/*=-1*/, bool verbose/*=false*/, T* all_kg_data, int32_t* all_indices)
{
    int col_id = blockDim.x * blockIdx.x + threadIdx.x;
	T *a, *b, tmp;
	int itmp;
	int32_t* indices;
	T* kg_data, *_data;
	thrust::counting_iterator<int> ite_indices(0); // virtual list of indices from 0 to nrows

	T zero;
	memset(&zero, 0, sizeof(zero)); // bzero doesn't work on MS VS
	greater_abs<T> ga;
	int32_t col_offset = nrows * col_id;
	if(col_id < ncols)
	{
		indices = all_indices  + col_offset;
		kg_data = all_kg_data + k * col_id;
		_data = data + col_offset;
		// create the list of column indices
		thrust::copy(thrust::device, ite_indices, ite_indices+nrows, indices);
		// pack descendingly the k greatest values to the left of _data
		for(int i=0;i < nrows - 1 && i < k; i++)
			for(int j=i+1;j < nrows; j++)
			{
				a = _data+i;
				b = _data+j;
				if(ga(*b, *a))
				{
					tmp = *a;
					*a = *b;
					*b = tmp;
					itmp = indices[i];
					indices[i] = indices[j];
					indices[j] = itmp;
				}
			}
		// copy the k greatest values in a tmp buf
		for(int i=0; i < k; i++)
			kg_data[i] = _data[i];
		// zero the _data column
		for(int i=0;i < nrows; i++)
			_data[i] = zero;
		// copy back k greatest values to their original positions
		for(int i=0; i < k; i++)
			_data[indices[i]] = kg_data[i];
	}
}


template<typename T>
void __global__ kernel_prox_spcol_shared(T* data, int32_t dlen, int32_t nrows, int32_t ncols, int32_t k, int32_t dev_id/*=-1*/, bool verbose/*=false*/, T* all_kg_data)
{

	extern __shared__ int32_t indices_buf[];
	// can't put kg_data on shared memory too because of alignment issue (I tried and I got cudaMemcpyAsync error: cudaErrorMisalignedAddress at exec time)
	// indices is the largest buffer so I chose it rather than kg_data (I tried to do the inverse, it's not better)
    int col_id = blockDim.x * blockIdx.x + threadIdx.x;
	T *a, *b, tmp;
	int itmp;
	int32_t* indices;
	T* kg_data, *_data;
	thrust::counting_iterator<int> ite_indices(0); // virtual list of indices from 0 to nrows

	T zero;
	memset(&zero, 0, sizeof(zero)); // bzero doesn't work on MS VS
	greater_abs<T> ga;
	int32_t col_offset = nrows * col_id;
	if(col_id < ncols)
	{
		indices = indices_buf + threadIdx.x * nrows;
		kg_data = all_kg_data + k * col_id;
		_data = data + col_offset;
		// create the list of column indices
		thrust::copy(thrust::device, ite_indices, ite_indices+nrows, indices);
		// pack descendingly the k greatest values to the left of _data
		for(int i=0;i < nrows - 1 && i < k; i++)
			for(int j=i+1;j < nrows; j++)
			{
				a = _data+i;
				b = _data+j;
				if(ga(*b, *a))
				{
					tmp = *a;
					*a = *b;
					*b = tmp;
					itmp = indices[i];
					indices[i] = indices[j];
					indices[j] = itmp;
				}
			}
		// copy the k greatest values in a tmp buf
		for(int i=0; i < k; i++)
			kg_data[i] = _data[i];
		// zero the _data column
		for(int i=0;i < nrows; i++)
			_data[i] = zero;
		// copy back k greatest values to their original positions
		for(int i=0; i < k; i++)
			_data[indices[i]] = kg_data[i];
	}
}

template<typename T> void prox_spcol(T* data, int32_t dlen, int32_t ncols, int32_t k, int32_t dev_id/*=-1*/, bool verbose/*=false*/)
{
	const int32_t max_shared_mem = 48*1024;
//	int* indices;
	T* kg_data;
	int32_t nrows = dlen/ncols;
	int threadsPerBlock = 256; // default value
	int blocksPerGrid = (ncols + threadsPerBlock - 1) / threadsPerBlock; // default value
	assert(cudaMalloc(&kg_data, sizeof(T)*k*ncols) == CUDA_SUCCESS);
//	assert(cudaMalloc(&indices, sizeof(int)*nrows*ncols) == CUDA_SUCCESS); // the two kernels below needs this
//	kernel_prox_spcol<T><<<blocksPerGrid,threadsPerBlock>>>(data, dlen, nrows, ncols, k, dev_id, verbose, kg_data, indices); // slower
//	kernel_prox_spcol2<T><<<blocksPerGrid,threadsPerBlock>>>(data, dlen, nrows, ncols, k, dev_id, verbose, kg_data, indices); // even slower
	/******** kernel using shared memory **********/
	threadsPerBlock = max_shared_mem / nrows / sizeof(int32_t);
	threadsPerBlock = threadsPerBlock>512?512:threadsPerBlock; // 512 is the max number of threads per block (event if I saw 1024 here https://forums.developer.nvidia.com/t/maximum-number-of-threads-on-thread-block/46392)
	blocksPerGrid = (ncols + threadsPerBlock - 1) / threadsPerBlock;
	kernel_prox_spcol_shared<T><<<blocksPerGrid,threadsPerBlock, nrows*threadsPerBlock*sizeof(int32_t)>>>(data, dlen, nrows, ncols, k, dev_id, verbose, kg_data);
	faust_kernelSafe();
	/******************/
//	cudaFree(indices);
	cudaFree(kg_data);
}

#include "proximity_ops.hu"
