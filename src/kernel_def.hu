/****************************************************************************/
/*                              Description:                                */
/*  For more information on the FAuST Project, please visit the website     */
/*  of the project : <http://faust.inria.fr>                         */
/*                                                                          */
/*                              License:                                    */
/*  Copyright (2019):    Hakim Hadj-Djilani, Nicolas Bellot, Adrien Leman, Thomas Gautrais,      */
/*                      Luc Le Magoarou, Remi Gribonval                     */
/*                      INRIA Rennes, FRANCE                                */
/*                      http://www.inria.fr/                                */
/*                                                                          */
/*  The FAuST Toolbox is distributed under the terms of the GNU Affero      */
/*  General Public License.                                                 */
/*  This program is free software: you can redistribute it and/or modify    */
/*  it under the terms of the GNU Affero General Public License as          */
/*  published by the Free Software Foundation.                              */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful, but     */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of              */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    */
/*  See the GNU Affero General Public License for more details.             */
/*                                                                          */
/*  You should have received a copy of the GNU Affero General Public        */
/*  License along with this program.                                        */
/*  If not, see <http://www.gnu.org/licenses/>.                             */
/*                                                                          */
/*                             Contacts:                                    */
/*      Hakim H. hakim.hadj-djilani@inria.fr                                */
/*      Nicolas Bellot  : nicolas.bellot@inria.fr                           */
/*      Adrien Leman    : adrien.leman@inria.fr                             */
/*      Thomas Gautrais : thomas.gautrais@inria.fr                          */
/*      Luc Le Magoarou : luc.le-magoarou@inria.fr                          */
/*      Remi Gribonval  : remi.gribonval@inria.fr                           */
/*                                                                          */
/*                              References:                                 */
/*  [1] Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse       */
/*  approximations of matrices and applications", Journal of Selected       */
/*  Topics in Signal Processing, 2016.                                      */
/*  <https://hal.archives-ouvertes.fr/hal-01167948v1>                       */
/****************************************************************************/

// 
template void kernel_add<float>(float* d_cu1, const float* d_cu2, int length);
template void kernel_sub<float>(float* d_cu1, const float* d_cu2, int length);
template void kernel_mult<float>(float* d_cu1, const float* d_cu2, int length);
template void kernel_mult_cplx<float2>(float2* d_cu1, const float2* d_cu2, int length);
template void kernel_mult_ids<float>(const float* d_cu1, const float* d_cu2, float* out, const int* ids, int length);
template void kernel_mult_ids_cplx<float2>(const float2* d_cu1, const float2* d_cu2, float2* out, const int* ids, int length);
template void kernel_div<float>(float* d_cu1, const float* d_cu2, int length);
template void kernel_div_cplx<float2>(float2* d_cu1, const float2* d_cu2, int length);
template void kernel_add_const<float>(float* d_cu1, float valeur, int length);
template void kernel_add_const_cplx<float2>(float2* d_cu1, float2 valeur, int length);
template void kernel_sub_const<float>(float* d_cu1, float valeur, int length);
template void kernel_sub_const_cplx<float2>(float2* d_cu1, float2 valeur, int length);
template void kernel_mult_const<float>(float* d_cu1, float valeur, int length);
template void kernel_div_const<float>(float* d_cu1, float valeur, int length);
template void kernel_square<float>(float* d_cu1, int length);
template void kernel_square<float>(float* d_cu_dst, const float* d_cu_src, int length);
template void kernel_sqrt<float>(float* d_cu1, int length);
template void kernel_inv<float>(float* d_cu1, int length);
template void kernel_abs<float>(float* d_cu1, const int length);
template void kernel_abs_cplx<float2>(float2* d_cu1, const int length);
template void kernel_abs_cplx2real<float2,float>(float2* d_cu1, float* d_cu2, const int length);
template void kernel_cplx2real<float2,float>(float2* d_cu1, float* d_cu2, const int length);
template void kernel_memcpy<float,float>(float* d_cu_dst, const float* d_cu_src, int length);
template void kernel_memset<float>(float* d_cu_dst, float valeur, int length);
template void kernel_memset_cplx<float2>(float2* d_cu_dst, float2 valeur, int length);
template void kernel_sparse2full<float>(float* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const float* dev_src_values, const int nnz, const int src_dim1, const int src_dim2);
template void kernel_add_sparse2full<float>(float* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const float* dev_src_values, const int nnz, const int src_dim1);
template void kernel_sub_sparse2full<float>(float* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const float* dev_src_values, const int nnz, const int src_dim1);
template void kernel_get_diag<float>(float* dst_diag, const float* src_M, int dim1, int dlen);
template void kernel_get_diag<float2>(float2* dst_diag, const float2* src_M, int dim1, int dlen);
template void kernel_add_diag_const<float>(float* d_cu1, float val, int dim1);
template void kernel_copy_diag<float>(float* d_cu_dst, float* d_cu_src, int dim1);
template void kernel_set_submatrix<float>(float* mat_dst, float* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_col);
template void kernel_relative_error<float>(float* data_dst, const float* data_src_th, const float* data_src_mes, const int length);
template void kernel_relative_error_cplx<float2>(float2* data_dst, const float2* data_src_th, const float2* data_src_mes, const int length);

template void kernel_add<double>(double* d_cu1, const double* d_cu2, int length);
template void kernel_sub<double>(double* d_cu1, const double* d_cu2, int length);
template void kernel_mult<double>(double* d_cu1, const double* d_cu2, int length);
template void kernel_mult_cplx<double2>(double2* d_cu1, const double2* d_cu2, int length);
template void kernel_mult_ids<double>(const double* d_cu1, const double* d_cu2, double* d_out, const int* ids, int length);
template void kernel_mult_ids_cplx<double2>(const double2* d_cu1, const double2* d_cu2, double2* d_out, const int* ids, int length);
template void kernel_div<double>(double* d_cu1, const double* d_cu2, int length);
template void kernel_div_cplx<double2>(double2* d_cu1, const double2* d_cu2, int length);
template void kernel_add_const<double>(double* d_cu1, double valeur, int length);
template void kernel_add_const_cplx<double2>(double2* d_cu1, double2 valeur, int length);
template void kernel_sub_const<double>(double* d_cu1, double valeur, int length);
template void kernel_sub_const_cplx<double2>(double2* d_cu1, double2 valeur, int length);
template void kernel_mult_const<double>(double* d_cu1, double valeur, int length);
template void kernel_div_const<double>(double* d_cu1, double valeur, int length);
template void kernel_square<double>(double* d_cu1, int length);
template void kernel_square<double>(double* d_cu_dst, const double* d_cu_src, int length);
template void kernel_sqrt<double>(double* d_cu1, int length);
template void kernel_inv<double>(double* d_cu1, int length);
template void kernel_abs<double>(double* d_cu1, const int length);
template void kernel_abs_cplx<double2>(double2* d_cu1, const int length);
template void kernel_abs_cplx2real<double2,double>(double2* d_cu1, double* d_cu2, const int length);
template void kernel_cplx2real<double2,double>(double2* d_cu1, double* d_cu2, const int length);
template void kernel_memcpy<double,double>(double* d_cu_dst, const double* d_cu_src, int length);
template void kernel_memset<double>(double* d_cu_dst, double valeur, int length);
template void kernel_memset_cplx<double2>(double2* d_cu_dst, double2 valeur, int length);
template void kernel_sparse2full<double>(double* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const double* dev_src_values, const int nnz, const int src_dim1, const int src_dim2);
template void kernel_add_sparse2full<double>(double* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const double* dev_src_values, const int nnz, const int src_dim1);
template void kernel_sub_sparse2full<double>(double* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const double* dev_src_values, const int nnz, const int src_dim1);
template void kernel_get_diag<double>(double* dst_diag, const double* src_M, int dim1, int dlen);
template void kernel_get_diag<double2>(double2* dst_diag, const double2* src_M, int dim1, int dlen);
template void kernel_add_diag_const<double>(double* d_cu1, double val, int dim1);
template void kernel_copy_diag<double>(double* d_cu_dst, double* d_cu_src, int dim1);
template void kernel_set_submatrix<double>(double* mat_dst, double* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_col);
template void kernel_relative_error<double>(double* data_dst, const double* data_src_th, const double* data_src_mes, const int length);
template void kernel_relative_error_cplx<double2>(double2* data_dst, const double2* data_src_th, const double2* data_src_mes, const int length);

template void kernel_add<int>(int* d_cu1, const int* d_cu2, int length);
template void kernel_sub<int>(int* d_cu1, const int* d_cu2, int length);
template void kernel_mult<int>(int* d_cu1, const int* d_cu2, int length);
template void kernel_add_const<int>(int* d_cu1, int valeur, int length);
template void kernel_sub_const<int>(int* d_cu1, int valeur, int length);
template void kernel_mult_const<int>(int* d_cu1, int valeur, int length);
template void kernel_memcpy<int,int>(int* d_cu_dst, const int* d_cu_src, int length);
template void kernel_memset<int>(int* d_cu_dst, int valeur, int length);
template void kernel_sparse2full<int>(int* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const int* dev_src_values, const int nnz, const int src_dim1, const int src_dim2);
template void kernel_add_sparse2full<int>(int* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const int* dev_src_values, const int nnz, const int src_dim1);
template void kernel_sub_sparse2full<int>(int* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const int* dev_src_values, const int nnz, const int src_dim1);
template void kernel_get_diag<int>(int* dst_diag, const int* src_M, int dim1, int dlen);
template void kernel_add_diag_const<int>(int* d_cu1, int val, int dim1);
template void kernel_copy_diag<int>(int* d_cu_dst, int* d_cu_src, int dim1);
template void kernel_set_submatrix<int>(int* mat_dst, int* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_col);

template void kernel_memcpy<double,float>(double* d_cu_dst, const float* d_cu_src, int length);
template void kernel_memcpy<float,double>(float* d_cu_dst, const double* d_cu_src, int length);

template void kernel_add_cplx<float2>(float2* d_cu1, const float2* d_cu2, int length);
template void kernel_add_cplx<double2>(double2* d_cu1, const double2* d_cu2, int length);

template  void kernel_positive_matrix<float>(float* A, int numElements);
template  void kernel_positive_matrix<double>(double* A, int numElements);

template void kernel_butterfly_diag_prod(const double* d_X, const double* d_D1, const double* d_D2, double* d_out, const int* ids,  int x_nrows, int x_ncols);
template void kernel_butterfly_diag_prod(const float* d_X, const float* d_D1, const float* d_D2, float* d_out, const int* ids,  int x_nrows, int x_ncols);

template void kernel_butterfly_diag_prod_cplx(const double2* d_X, const double2* d_D1, const double2* d_D2, double2* d_out, const int* ids,  int x_nrows, int x_ncols);
template void kernel_butterfly_diag_prod_cplx(const float2* d_X, const float2* d_D1, const float2* d_D2, float2* d_out, const int* ids,  int x_nrows, int x_ncols);




template __global__ void Add_inria<float>(float* A, const float *B, int numElements);
template __global__ void Sub_inria<float>(float* A, const float *B, int numElements);
template __global__ void Mult_inria<float>(float* A, const float *B, int numElements);
template __global__ void Mult_ids_inria<float>(const float* A, const float *B, float* out, const int *ids, int numElements);
template __global__ void Div_inria<float>(float* A, const float *B, int numElements);
template __global__ void AddConst_inria<float>(float* A, float val, int numElements);
template __global__ void SubConst_inria<float>(float* A, float val, int numElements);
template __global__ void MultConst_inria<float>(float* A, float val, int numElements);
template __global__ void DivConst_inria<float>(float* A, float val, int numElements);
template __global__ void Sqrt_inria<float>(float* A, int numElements);
template __global__ void Inv_inria<float>(float* A, int numElements);
template __global__ void Abs_inria<float>(float* A, int numElements);

template __global__ void Memcpy_inria<float,float>(float* d_cu_dst, const float* d_cu_src, int length);
template __global__ void Memset_inria<float>(float* dev_dst, float valeur, int numElements);
template __global__ void Memset_inria_cplx<float2>(float2* dev_dst, float2 valeur, int numElements);
template __global__ void Sparse2full_inria<float>(float* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const float* dev_src_values, const int nnz, const int src_dim1);
template __global__ void AddDiagConst_inria<float>(float* dev_dst, float val, int dim1);
template __global__ void GetDiag_inria<float>(float* dst_diag, const float* src_M,  int dim1, int dlen);
template __global__ void SetSubmatrix_inria<float>(float* mat_dst, float* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_elements);
template __global__ void RelativeError_inria<float>(float* data_dst, const float* data_src_th, const float* data_src_mes, const int length);
template __global__ void RelativeError_inria_cplx<float2>(float2* data_dst, const float2* data_src_th, const float2* data_src_mes, const int length);

template __global__ void Add_inria<double>(double* A, const double *B, int numElements);
template __global__ void Sub_inria<double>(double* A, const double *B, int numElements);
template __global__ void Mult_inria<double>(double* A, const double *B, int numElements);
template __global__ void Mult_ids_inria<double>(const double* A, const double *B, double* out, const int *ids, int numElements);
template __global__ void Div_inria<double>(double* A, const double *B, int numElements);
template __global__ void AddConst_inria<double>(double* A, double val, int numElements);
template __global__ void SubConst_inria<double>(double* A, double val, int numElements);
template __global__ void MultConst_inria<double>(double* A, double val, int numElements);
template __global__ void DivConst_inria<double>(double* A, double val, int numElements);
template __global__ void Sqrt_inria<double>(double* A, int numElements);
template __global__ void Inv_inria<double>(double* A, int numElements);
template __global__ void Abs_inria<double>(double* A, int numElements);
template __global__ void Memcpy_inria<double,double>(double* d_cu_dst, const double* d_cu_src, int length);
template __global__ void Memset_inria<double>(double* dev_dst, double valeur, int numElements);
template __global__ void Memset_inria_cplx<double2>(double2* dev_dst, double2 valeur, int numElements);
template __global__ void Sparse2full_inria<double>(double* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const double* dev_src_values, const int nnz, const int src_dim1);
template __global__ void AddDiagConst_inria<double>(double* dev_dst, double val, int dim1);
template __global__ void GetDiag_inria<double>(double* dst_diag, const double* src_M,  int dim1, int dlen);
template __global__ void SetSubmatrix_inria<double>(double* mat_dst, double* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_elements);
template __global__ void RelativeError_inria<double>(double* data_dst, const double* data_src_th, const double* data_src_mes, const int length);
template __global__ void RelativeError_inria_cplx<double2>(double2* data_dst, const double2* data_src_th, const double2* data_src_mes, const int length);

template __global__ void Add_inria<int>(int* A, const int *B, int numElements);
template __global__ void Sub_inria<int>(int* A, const int *B, int numElements);
template __global__ void Mult_inria<int>(int* A, const int *B, int numElements);
template __global__ void AddConst_inria<int>(int* A, int val, int numElements);
template __global__ void SubConst_inria<int>(int* A, int val, int numElements);
template __global__ void MultConst_inria<int>(int* A, int val, int numElements);
template __global__ void Memcpy_inria<int,int>(int* d_cu_dst, const int* d_cu_src, int length);
template __global__ void Memset_inria<int>(int* dev_dst, int valeur, int numElements);
template __global__ void Sparse2full_inria<int>(int* dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const int* dev_src_values, const int nnz, const int src_dim1);
template __global__ void AddDiagConst_inria<int>(int* dev_dst, int val, int dim1);
template __global__ void GetDiag_inria<int>(int* dst_diag, const int* src_M,  int dim1, int dlen);
template __global__ void SetSubmatrix_inria<int>(int* mat_dst, int* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_elements);

template __global__ void Memcpy_inria<float,double>(float* d_cu_dst, const double* d_cu_src, int length);
template __global__ void Memcpy_inria<double,float>(double* d_cu_dst, const float* d_cu_src, int length);
template __global__ void sum_reduce<float2>(float2 *g_idata, float2 *g_odata, unsigned int n);
template __global__ void sum_reduce<double2>(double2 *g_idata, double2 *g_odata, unsigned int n);
template __global__ void sum_abs_reduce<float2>(float2 *g_idata, float2 *g_odata, unsigned int n);
template __global__ void sum_abs_reduce<double2>(double2 *g_idata, double2 *g_odata, unsigned int n);
template __global__ void min_max_reduce<float2>(float2 *g_idata, float2 *g_odata, unsigned int n, bool max_func);
template __global__ void min_max_reduce<double2>(double2 *g_idata, double2 *g_odata, unsigned int n, bool max_func);
template __global__ void finish_min_max_reduce<float2>(float2 *g_idata, float2 *g_odata, unsigned int n, bool max_func);
template __global__ void finish_min_max_reduce<double2>(double2 *g_idata, double2 *g_odata, unsigned int n, bool max_func);

template __global__ void Abs_inria_cplx<double2>(double2* A, int numElements);
template __global__ void Abs_inria_cplx<float2>(float2* A, int numElements);

template __global__ void Add_inria_cplx<float2>(float2* A, const float2 *B, int numElements);
template __global__ void Add_inria_cplx<double2>(double2* A, const double2 *B, int numElements);
template __global__ void Mult_inria_cplx<float2>(float2* A, const float2 *B, int numElements);
template __global__ void Mult_ids_inria_cplx<float2>(const float2* A, const float2 *B, float2 *out, const int *ids, int numElements);
template __global__ void Mult_ids_inria_cplx<double2>(const double2* A, const double2 *B, double2* out, const int *ids, int numElements);
template __global__ void Div_inria_cplx<float2>(float2* A, const float2 *B, int numElements);
template __global__ void Div_inria_cplx<double2>(double2* A, const double2 *B, int numElements);


template __global__ void positive_matrix<float>(float* A, int numElements);
template __global__ void positive_matrix<double>(double* A, int numElements);

template __global__ void butterfly_diag_prod_cplx(const float2 *X, const float2 *D1, const float2 *D2, float2 * out, const int* ids,  int x_nrows, int x_ncols);
template __global__ void butterfly_diag_prod_cplx(const double2 *X, const double2 *D1, const double2 *D2, double2 * out, const int* ids,  int x_nrows, int x_ncols);

template __global__ void butterfly_diag_prod(const float *X, const float *D1, const float *D2, float * out, const int* ids,  int x_nrows, int x_ncols);
template __global__ void butterfly_diag_prod(const double *X, const double *D1, const double *D2, double * out, const int* ids,  int x_nrows, int x_ncols);


