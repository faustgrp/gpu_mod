#ifndef __GPU_MOD_CUMATARRAY__
#define __GPU_MOD_CUMATARRAY__
#include "cuMat.h"
#include "cuMatDs.h"
#include "cuMatSp.h"
#include "Real.h"
#include "STL_scalar_types.h"
#include <cusparse.h>
#include <cublas_v2.h>
#include <vector>
#include <algorithm>
#include <cassert>
#include <type_traits>
#include <string>
#include <cstdio>

//TODO: function to get nrows, ncols of a list matrix
//TODO: function to get nnz of a list sparse matrix
//TODO: these two functions can be implemented simply by using copy* functions and only affecting data of non nullptr pointers.
//TODO: function to erase a mat by index into a cuMatArray


template <typename T>
struct cuMatArray
{
	//TODO: use T... or delete it
	std::vector<cuMat<T>*> cu_array;
	bool del_mats;

	cuMatArray();
	~cuMatArray();
	/**
	 * Returns the number of rows (of the first factor).
	 */
	int32_t nrows() const;

	/**
	 * Returns the number of cols (of the last factor).
	 */
	int32_t ncols() const;

	/**
	 * Returns the total number of nonzeros (sum of factors nnz).
	 */
	size_t get_total_nnz() const;
	/**
	 * Displays the the list of factors according to op.
	 */
	void display(gm_Op op=OP_NOTRANSP);

	std::string  to_string(gm_Op op=OP_NOTRANSP);
	/**
	 * Computes the chain matrix product in cu_array applying the op (transpose, etc.) to it and if out is not nullptr it receives the result.
	 * The product is computed from the right factor to the left.
	 *
	 * Anyway it returns the result matrix (this is out if != nullptr).
	 */
	cuMatDs<T>* chain_matmul_r2l(T alpha, gm_Op op, cuMatDs<T>* out = nullptr);
	/**
	 * Does the same as chain_matmul_r2l but computing the product from the right to the left.
	 */
	cuMatDs<T>* chain_matmul_l2r(T alpha, gm_Op op, cuMatDs<T>* out = nullptr);
	/**
	 * This function computes the product alpha*op(cu_array*M).
	 * It relies on chain_matmul_r2l or chain_matmul_l2r depending on the op value.
	 *
	 */
	cuMatDs<T>* chain_matmul(T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out);

	/**
	 * Does the same as chain_matmul except that the cuMatArray can be sliced on rows and/or columns (of course the size of M must be adapted to the column slicing).
	 *
	 * \param rslice_start: start index for row slicing (if -1 then no slicling).
	 * \param cslice_start: start index for column slicing (if -1 then no slicling).
	 * \param rslice_size: size of the row slice.
	 * \param cslice_size: size of the column slice.
	 */
	cuMatDs<T>* sliced_chain_matmul(int rslice_start, int rslice_size, int cslice_start, int cslice_size, T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out);

	/**
	 * Does the same as chain_matmul except that the cuMatArray can be row indexed and/or column indexed before computing the product.
	 *
	 * \param ids: ids[0] are the indices for row dimension, ids[1] indices for column dimension.
	 * \param id_lens: id_lens[0] the number of indices for row dimension, id_lens[1] the number of indices for the column dimension.
	 */
	cuMatDs<T>* indexed_chain_matmul(size_t *ids[2], size_t id_lens[2], T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out);

	/**
	 * Multiplies the first matrix by scalar.
	 * The id-th factor is multiplied (0-based index).
	 *
	 * \param id: if -1 then it's auto-determined by taking the smallest factor id (optimization purpose). 
	 *
	 */
	void mul(const T& scalar, int32_t id=-1);

	/**
	 * The number of GPU matrices in the cuMatArray.
	 */
	size_t size();

	/**
	 * Inserts cuMat m at index id in the array.
	 */
	void insert(cuMat<T>* m, const int32_t id);

	/**
	 * Removes cuMat at index id in the array.
	 */
	void remove(int32_t id);

    /**
     *
     */
	Real<T> spectral_norm(const float threshold, const int32_t max_iter);

    /**
     *
     */
	T power_iteration(const float threshold, const int32_t max_iter);

	/**
	 * Transposes the array of matrices: reverses the order of matrices and transposes each one separately.
	 */
	void transpose();

	/**
	 * Transconjugate the array of matrices: reverses the order of matrices and transconjugate each one separately.
	 */
	void adjoint();

};

/********************* non-member functions */


/**
 *
 * This function is a casting wrapper to cuMatArray::chain_matmul_r2l.
 */
template<typename T>
gm_DenseMat_t marr_chain_matmul_r2l(gm_MatArray_t, T alpha, gm_Op);

/**
 * This function is a casting wrapper to cuMatArray::marr_chain_matmul.
 *
 */
template<typename T>
gm_DenseMat_t marr_chain_matmul(gm_MatArray_t cl, T alpha, gm_Op op, gm_DenseMat_t, gm_DenseMat_t dsm_out = nullptr);


/*
 * This function computes the product alpha*op(array*mat_data).
 *
 * It creates a cuMatDs M (GPU dense matrix) from the cpu buffer mat_data (of size nrows*ncols)
 * then it calls marr_chain_matmul(cuMatArray<T>* cl, T alpha, gm_Op op, cuMatDs<T>* M, cuMatDs<T>* dsm_out = nullptr)
 * to compute alpha*op(cl*M) (array is cast to a cuMatArray<T> cl).
 *
 *
 * \param the GPU array of factors to multiply mat_data by.
 * \param mat_data is the CPU data buffer for the matrix.
 * \param nrows the number of rows of the CPU matrix.
 * \param ncols the number of columns of the CPU matrix.
 * \param op the operation gm_Op to apply on the product.
 *
 * \return the product result into a GPU matrix (gm_DenseMat_t).
 *
 */
template<typename T>
gm_DenseMat_t marr_chain_matmul(gm_MatArray_t array, T alpha, gm_Op op, T* mat_data, int32_t nrows, int32_t ncols);

/**
 * This function does the same as the function just above but it copies the GPU result into a pre-allocated CPU buffer (data_out).
 */
template<typename T>
void marr_chain_matmul(gm_MatArray_t array, T alpha, gm_Op op, T* mat_data, int32_t nrows, int32_t ncols, T* data_out);

template<typename T>
gm_MatArray_t marr_create();

template<typename T>
void marr_free(gm_MatArray_t, const bool del_mats);

template<typename T>
void marr_add_gpu_spm(gm_MatArray_t, gm_SparseMat_t);

template<typename T>
void marr_add_gpu_dsm(gm_MatArray_t, gm_DenseMat_t);

template<typename T>
void marr_add_gpu_anymat(gm_MatArray_t, void*);

template<typename T>
gm_SparseMat_t marr_togpu_spm(gm_MatArray_t, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values);

template<typename T>
gm_DenseMat_t marr_togpu_dsm(gm_MatArray_t, int32_t nrows, int32_t ncols, T* data);

template<typename T>
gm_DenseMat_t marr_replace_dsm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, T* data, int32_t id);

template<typename T>
gm_SparseMat_t marr_replace_spm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values, int32_t id);

template<typename T>
gm_DenseMat_t marr_set_dsm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, T* data, int32_t id);

template<typename T>
gm_SparseMat_t marr_set_spm_at(gm_MatArray_t array, int32_t nrows, int32_t ncols, int32_t nnz, int32_t* row_ptr, int32_t* col_inds, T* values, int32_t id);

template<typename T>
bool marr_is_mat_sparse(gm_MatArray_t, u_int32_t index);

template<typename T>
void marr_erase_at(gm_MatArray_t, u_int32_t index, const bool free);

template<typename T>
void marr_insert(gm_MatArray_t array, cuMat<T>* mat, int32_t id);

template<typename T>
void marr_remove(gm_MatArray_t array, int32_t id);

template<typename T>
void marr_tocpu_spm(gm_MatArray_t, u_int32_t index, int32_t *nrows, int32_t *ncols, int32_t *nnz, int32_t *rowptr, int32_t *colids, T* values);

template<typename T>
void marr_tocpu_dsm(gm_MatArray_t, u_int32_t index, int32_t *nrows, int32_t *ncols, T * data);

template<typename T>
gm_SparseMat_t marr_get_spm(gm_MatArray_t, u_int32_t index);

template<typename T>
gm_DenseMat_t marr_get_dsm(gm_MatArray_t, u_int32_t index);

template<typename T>
size_t marr_size(gm_MatArray_t array);

template<typename T>
gm_DenseMat_t marr_sliced_chain_matmul(gm_MatArray_t array, int rslice_start, int rslice_size, int cslice_start, int cslice_size, T alpha, gm_Op op, gm_DenseMat_t M, gm_DenseMat_t dsm_out = nullptr );
template<typename T>
gm_DenseMat_t marr_indexed_chain_matmul(gm_MatArray_t array, size_t *ids[2], size_t id_lens[2], T &alpha, gm_Op op, gm_DenseMat_t M, gm_DenseMat_t dsm_out = nullptr);

#include "cuMatArray.hpp"
#endif
