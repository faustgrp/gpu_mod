#include "gm_interf.h"
#include "Real.h"


	template<>
Real<double2> real(const double2 &c)
{
	return c.x;
}

	template<>
Real<float2> real(const float2 &c)
{
	return c.x;
}

template<>
Real<double> real(const double &x)
{
	return x;
}

template<>
Real<float> real(const float &x)
{
	return x;
}


	template<>
std::string to_string(const double2 &c)
{
	return std::to_string(c.x)+" "+std::to_string(c.y);
}



	template<>
std::string to_string(const float2 &c)
{
	return std::to_string(c.x)+" "+std::to_string(c.y);
}

template<>
std::string to_string(const double &x)
{
	return std::to_string(x);
}

template<>
std::string to_string(const float &x)
{
	return std::to_string(x);
}
