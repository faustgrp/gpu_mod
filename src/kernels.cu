/****************************************************************************/
/*                              Description:                                */
/*  For more information on the FAuST Project, please visit the website     */
/*  of the project : <http://faust.inria.fr>                         */
/*                                                                          */
/*                              License:                                    */
/*  Copyright (2021):    Hakim Hadj-Djilani, Nicolas Bellot, Adrien Leman, Thomas Gautrais,      */
/*                      Luc Le Magoarou, Remi Gribonval                     */
/*                      INRIA Rennes, FRANCE                                */
/*                      http://www.inria.fr/                                */
/*                                                                          */
/*  The FAuST Toolbox is distributed under the terms of the GNU Affero      */
/*  General Public License.                                                 */
/*  This program is free software: you can redistribute it and/or modify    */
/*  it under the terms of the GNU Affero General Public License as          */
/*  published by the Free Software Foundation.                              */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful, but     */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of              */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    */
/*  See the GNU Affero General Public License for more details.             */
/*                                                                          */
/*  You should have received a copy of the GNU Affero General Public        */
/*  License along with this program.                                        */
/*  If not, see <http://www.gnu.org/licenses/>.                             */
/*                                                                          */
/*                             Contacts:                                    */
/*      Hakim H. hakim.hadj-djilani@inria.fr                                */
/*      Nicolas Bellot  : nicolas.bellot@inria.fr                           */
/*      Adrien Leman    : adrien.leman@inria.fr                             */
/*      Thomas Gautrais : thomas.gautrais@inria.fr                          */
/*      Luc Le Magoarou : luc.le-magoarou@inria.fr                          */
/*      Remi Gribonval  : remi.gribonval@inria.fr                           */
/*                                                                          */
/*                              References:                                 */
/*  [1] Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse       */
/*  approximations of matrices and applications", Journal of Selected       */
/*  Topics in Signal Processing, 2016.                                      */
/*  <https://hal.archives-ouvertes.fr/hal-01167948v1>                       */
/****************************************************************************/
#include "kernels.h"

//prototypes
template<typename T> __global__ void Add_inria(T *A, const T *B, int numElements);
template<typename T> __global__ void Add_inria_cplx(T *A, const T *B, int numElements);
template<typename T> __global__ void Sub_inria(T *A, const T *B, int numElements);
template<typename T> __global__ void Mult_inria(T *A, const T *B, int numElements);
template<typename T> __global__ void Mult_inria_cplx(T *A, const T *B, int numElements);
template<typename T> __global__ void Mult_ids_inria(const T *A, const T *B, T *out, const int *ids, int numElements);
template<typename T> __global__ void Mult_ids_inria_cplx(const T *A, const T *B, T *out, const int *ids, int numElements);
template<typename T> __global__ void Div_inria(T *A, const T *B, int numElements);
template<typename T> __global__ void Div_inria_cplx(T *A, const T *B, int numElements);

template<typename T> __global__ void AddConst_inria(T *A, T val, int numElements);
template<typename T> __global__ void AddConst_inria_cplx(T *A, T val, int numElements);
template<typename T> __global__ void SubConst_inria(T *A, T val, int numElements);
template<typename T> __global__ void SubConst_inria_cplx(T *A, T val, int numElements);
template<typename T> __global__ void MultConst_inria(T *A, T val, int numElements);
template<typename T> __global__ void DivConst_inria(T *A, T val, int numElements);

template<typename T> __global__ void Square_inria(T *A, int numElements);
template<typename T> __global__ void Square_inria(T *d_cu_dst, const T* d_cu_src, int numElements);
template<typename T> __global__ void Sqrt_inria(T *A, int numElements);
template<typename T> __global__ void Inv_inria(T *A, int numElements);
template<typename T> __global__ void Abs_inria(T *A, int numElements);
template<typename T> __global__ void Abs_inria_cplx(T *A, int numElements);
template<typename T, typename R> __global__ void Abs_inria_cplx2Real(T *A, R* B, int numElements);
template<typename T, typename R> __global__ void cplx2real(T *A, R* B, int numElements);

template<typename T, typename U> __global__ void Memcpy_inria(T* d_cu_dst, const U* d_cu_src, int length);


template<typename T> __global__ void Memset_inria(T* dev_dst, T valeur, int numElements);
template<typename T> __global__ void Memset_inria_cplx(T* dev_dst, T valeur, int numElements);

template<typename T> __global__ void Sparse2full_inria(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1);
template<typename T> __global__ void AddSparse2full_inria(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1);
template<typename T> __global__ void SubSparse2full_inria(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1);
template<typename T> __global__ void GetDiag_inria(T* dst_diag, const T* src_M,  int dim1, int dlen);
template<typename T> __global__ void AddDiagConst_inria(T* dev_dst, T val, int dim1);
template<typename T> __global__ void CopyDiag_inria(T* dev_dst, T* dev_src, int dim1);

template<typename T> __global__ void SetSubmatrix_inria(T* mat_dst, T* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_elements);


template<typename T> __global__ void RelativeError_inria(T* data_dst, const T* data_src_th, const T* data_src_mes, const int length);
template<typename T> __global__ void RelativeError_inria_cplx(T* data_dst, const T* data_src_th, const T* data_src_mes, const int length);

template <typename T> __global__ void sum_reduce(T *g_idata, T *g_odata, unsigned int n);


template<typename T> __global__ void butterfly_diag_prod(const T *X, const T *D1, const T *D2, T * out, const int* ids, int x_nrows, int x_ncols);
template<typename T> __global__ void butterfly_diag_prod_cplx(const T *X, const T *D1, const T *D2, T * out, const int* ids, int x_nrows, int x_ncols);



// Kernel wrappers
template<typename T> void kernel_add(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Add_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
   faust_kernelSafe();
}

template<typename T> void kernel_add_cplx(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Add_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
   faust_kernelSafe();
}

template<typename T> void kernel_sub(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Sub_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
   faust_kernelSafe();
}
template<typename T> void kernel_mult(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Mult_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
   faust_kernelSafe();
}
template<typename T> void kernel_mult_cplx(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Mult_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
   faust_kernelSafe();
}

template<typename T> void kernel_mult_ids(const T* d_cu1, const T* d_cu2, T* d_out, const int* ids, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Mult_ids_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, d_out, ids, length);
	faust_kernelSafe();
}

template<typename T> void kernel_mult_ids_cplx(const T* d_cu1, const T* d_cu2, T* d_out, const int* ids, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Mult_ids_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, d_out, ids, length);
	faust_kernelSafe();
}

template<typename T>  void kernel_div(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Div_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
   faust_kernelSafe();
}
template<typename T>  void kernel_div_cplx(T* d_cu1, const T* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Div_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
	faust_kernelSafe();
}
template<typename T> void kernel_add_const(T* d_cu1, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	AddConst_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, valeur, length);
	faust_kernelSafe();
}

template<typename T> void kernel_add_const_cplx(T* d_cu1, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	AddConst_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, valeur, length);
	faust_kernelSafe();
}

template<typename T> void kernel_sub_const(T* d_cu1, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	SubConst_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, valeur, length);
   faust_kernelSafe();
}

template<typename T> void kernel_sub_const_cplx(T* d_cu1, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	SubConst_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, valeur, length);
	faust_kernelSafe();
}

template<typename T> void kernel_mult_const(T* d_cu1, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	MultConst_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, valeur, length);
   faust_kernelSafe();
}

template<typename T>  void kernel_div_const(T* d_cu1, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	DivConst_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, valeur, length);
   faust_kernelSafe();
}

template<typename T> void kernel_square(T* d_cu1, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Square_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, length);
   faust_kernelSafe();
}

template<typename T> void kernel_square(T* d_cu_dst, const T* d_cu_src, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Square_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu_dst, d_cu_src, length);
   faust_kernelSafe();
}

template<typename T> void kernel_sqrt(T* d_cu1, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Sqrt_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, length);
   faust_kernelSafe();
}

template<typename T> void kernel_inv(T* d_cu1, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Inv_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, length);
   faust_kernelSafe();
}

template<typename T> void kernel_abs(T* d_cu1, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Abs_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, length);
   faust_kernelSafe();
}

template<typename T> void kernel_abs_cplx(T* d_cu1, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Abs_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, length);
	faust_kernelSafe();
}

template<typename T, typename R> void kernel_abs_cplx2real(T* d_cu1, R* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Abs_inria_cplx2Real<T,R><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
	faust_kernelSafe();
}

template<typename T, typename R> void kernel_cplx2real(T* d_cu1, R* d_cu2, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	cplx2real<T,R><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, d_cu2, length);
	faust_kernelSafe();
}

template<typename T> __global__ void positive_matrix(T *A, int numElements)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	while (i < numElements)
	{
		if(A[i] < T(0)) A[i] = T(0);
		i += gridDim.x * blockDim.x;
	}
}

template<typename T> void kernel_positive_matrix(T* d_cu, int length)
{
	//only for real T: float or double
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	positive_matrix<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu, length);
	faust_kernelSafe();
}

template<typename T, typename U> void kernel_memcpy(T* d_cu_dst, const U* d_cu_src, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Memcpy_inria<T,U><<<blocksPerGrid,threadsPerBlock>>>(d_cu_dst, d_cu_src, length);
   faust_kernelSafe();
}


template<typename T> void kernel_memset(T* d_cu_dst, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Memset_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu_dst, valeur, length);
   faust_kernelSafe();
}

template<typename T> void kernel_memset_cplx(T* d_cu_dst, T valeur, int length)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (length + threadsPerBlock - 1) / threadsPerBlock;
	Memset_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu_dst, valeur, length);
   faust_kernelSafe();
}

template<typename T> void kernel_sparse2full(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1, const int src_dim2)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (nnz + threadsPerBlock - 1) / threadsPerBlock;
	T zero = (T) 0.0;
	kernel_memset<T>(dev_dst, zero, src_dim1*src_dim2);
	Sparse2full_inria<T><<<blocksPerGrid,threadsPerBlock>>>(dev_dst, dev_src_rowind, dev_src_colind, dev_src_values, nnz, src_dim1);
   faust_kernelSafe();
}
template<typename T> void kernel_add_sparse2full(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (nnz + threadsPerBlock - 1) / threadsPerBlock;
	AddSparse2full_inria<T><<<blocksPerGrid,threadsPerBlock>>>(dev_dst, dev_src_rowind, dev_src_colind, dev_src_values, nnz, src_dim1);
   faust_kernelSafe();
}
template<typename T> void kernel_sub_sparse2full(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (nnz + threadsPerBlock - 1) / threadsPerBlock;
	SubSparse2full_inria<T><<<blocksPerGrid,threadsPerBlock>>>(dev_dst, dev_src_rowind, dev_src_colind, dev_src_values, nnz, src_dim1);
   faust_kernelSafe();
}
template<typename T> void kernel_get_diag(T* dst_diag, const T* src_M, int dim1, int dlen)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (dim1 + threadsPerBlock - 1) / threadsPerBlock;
	GetDiag_inria<T><<<blocksPerGrid,threadsPerBlock>>>(dst_diag, src_M, dim1, dlen);
	faust_kernelSafe();
}
template<typename T> void kernel_add_diag_const(T* d_cu1, T val, int dim1)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (dim1 + threadsPerBlock - 1) / threadsPerBlock;
	AddDiagConst_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu1, val, dim1);
   faust_kernelSafe();
}
template<typename T> void kernel_copy_diag(T* d_cu_dst, T* d_cu_src, int dim1)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (dim1 + threadsPerBlock - 1) / threadsPerBlock;
	CopyDiag_inria<T><<<blocksPerGrid,threadsPerBlock>>>(d_cu_dst, d_cu_src, dim1);
   faust_kernelSafe();
}
template<typename T> void kernel_set_submatrix(T* mat_dst, T* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_col)
{
	int threadsPerBlock = 256;
	int nb_elements = nb_rows*nb_col;
	int blocksPerGrid = (nb_elements + threadsPerBlock - 1) / threadsPerBlock;
	SetSubmatrix_inria<T><<<blocksPerGrid,threadsPerBlock>>>(mat_dst, mat_src, src_dim1, r1, c1, nb_rows, nb_elements);
   faust_kernelSafe();
}

template<typename T> void kernel_relative_error(T* data_dst, const T* data_src_th, const T* data_src_mes, const int nb_elements)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (nb_elements + threadsPerBlock - 1) / threadsPerBlock;
	RelativeError_inria<T><<<blocksPerGrid,threadsPerBlock>>>(data_dst, data_src_th, data_src_mes, nb_elements);
   faust_kernelSafe();
}

template<typename T> void kernel_relative_error_cplx(T* data_dst, const T* data_src_th, const T* data_src_mes, const int nb_elements)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (nb_elements + threadsPerBlock - 1) / threadsPerBlock;
	RelativeError_inria_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(data_dst, data_src_th, data_src_mes, nb_elements);
	faust_kernelSafe();
}

template <typename T> void kernel_sum_reduce(T *g_idata, T *g_odata, unsigned int n)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (n + threadsPerBlock - 1) / threadsPerBlock;
	sum_reduce<<<blocksPerGrid, threadsPerBlock/*, shared memory threadsPerBlock*sizeof(T)*/>>>((T*) g_idata, (T*) g_odata, n);
	faust_kernelSafe();
}

template <typename T> void kernel_sum_abs_reduce(T *g_idata, T *g_odata, unsigned int n)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (n + threadsPerBlock - 1) / threadsPerBlock;
	sum_abs_reduce<<<blocksPerGrid, threadsPerBlock/*, shared memory threadsPerBlock*sizeof(T)*/>>>((T*) g_idata, (T*) g_odata, n);
	faust_kernelSafe();
}

template <typename T> void kernel_min_max_reduce(T *g_idata, T *g_odata, unsigned int n, bool max_func)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (n + threadsPerBlock - 1) / threadsPerBlock;
	min_max_reduce<<<blocksPerGrid, threadsPerBlock/*, shared memory threadsPerBlock*sizeof(T)*/>>>((T*) g_idata, (T*) g_odata, n, max_func);
	faust_kernelSafe();
	if(n > 256)
		finish_min_max_reduce<<<blocksPerGrid, threadsPerBlock>>>((T*) g_idata, (T*) g_odata, n, max_func);
	faust_kernelSafe();
}


template<typename T> void kernel_butterfly_diag_prod_cplx(const T* d_X, const T* d_D1, const T* d_D2, T* d_out, const int* ids, int x_nrows, int x_ncols)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (x_nrows + threadsPerBlock - 1) / threadsPerBlock;
	butterfly_diag_prod_cplx<T><<<blocksPerGrid,threadsPerBlock>>>(d_X, d_D1, d_D2, d_out, ids, x_nrows, x_ncols);
	faust_kernelSafe();
	//std::cout << "kernel_butterfly_diag_prod_cplx end" << std::endl;
}

template<typename T> void kernel_butterfly_diag_prod(const T* d_X, const T* d_D1, const T* d_D2, T* d_out, const int* ids, int x_nrows, int x_ncols)
{
	int threadsPerBlock = 256;
	int blocksPerGrid = (x_nrows + threadsPerBlock - 1) / threadsPerBlock;
	butterfly_diag_prod<T><<<blocksPerGrid,threadsPerBlock>>>(d_X, d_D1, d_D2, d_out, ids, x_nrows, x_ncols);
	faust_kernelSafe();
}

// Kernels

template<typename T> __global__ void Add_inria(T* A, const T* B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] + B[i];
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Add_inria_cplx(T* A, const T* B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
	{
		A[i].x = A[i].x + B[i].x;
		A[i].y = A[i].y + B[i].y;
		i += gridDim.x * blockDim.x;
	}
}

template<typename T> __global__ void Sub_inria(T* A, const T* B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] - B[i];
	i += gridDim.x * blockDim.x;
    }
}
template<typename T> __global__ void Mult_inria(T *A, const T *B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] * B[i];
		i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Mult_inria_cplx(T *A, const T *B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    auto tmp = A[0].x;
    while (i < numElements)
	{
		tmp = A[i].x * B[i].x - A[i].y * B[i].y; // real part
		A[i].y = A[i].x * B[i].y + B[i].x * A[i].y;	 // imag part
		A[i].x = tmp;
		i += gridDim.x * blockDim.x;
	}
}

template<typename T> __global__ void Mult_ids_inria(const T *A, const T *B, T* out, const int* ids, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        out[i] = A[ids[i]] * B[i];
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Mult_ids_inria_cplx(const T *A, const T *B, T * out, const int* ids, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
	{
		out[i].x = B[i].x * A[ids[i]].x - B[i].y * A[ids[i]].y; // real part
		out[i].y = B[i].x * A[ids[i]].y + A[ids[i]].x * B[i].y;	 // imag part
		i += gridDim.x * blockDim.x;
	}
}

template<typename T> __global__ void Div_inria(T *A, const T *B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] / B[i];
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Div_inria_cplx(T *A, const T *B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
	    auto A_x = A[i].x;
	    auto A_y = A[i].y;
	    auto d = B[i].x * B[i].x + B[i].y * B[i].y;
	    A[i].x = (A_x * B[i].x + A_y * B[i].y) / d;
	    A[i].y = (A_x * B[i].y + B[i].x * A_y) / d;
	    i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void AddConst_inria(T *A, T val, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] + val;
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void AddConst_inria_cplx(T *A, T val, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i].x = A[i].x + val.x;
        A[i].y = A[i].y + val.y;
		i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void SubConst_inria(T *A, T val, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] - val;
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void SubConst_inria_cplx(T *A, T val, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i].x = A[i].x - val.x;
        A[i].y = A[i].y - val.y;
		i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void MultConst_inria(T *A, T val, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] * val;
	i += gridDim.x * blockDim.x;
    }
}
template<typename T> __global__ void DivConst_inria(T* A, T val, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i] / val;
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Square_inria(T *A, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = A[i]*A[i];
	i += gridDim.x * blockDim.x;
    }
}
template<typename T> __global__ void Square_inria(T* dst, const T* src, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        dst[i] = src[i]*src[i];
	i += gridDim.x * blockDim.x;
    }
}
template<typename T> __global__ void Sqrt_inria(T *A, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = sqrt(A[i]);
	i += gridDim.x * blockDim.x;
    }
}
template<typename T> __global__ void Inv_inria(T *A, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i] = 1/A[i] ;
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Abs_inria(T *A, int numElements)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;

	while (i < numElements)
	{
		A[i] = fabs(A[i]) ;
		i += gridDim.x * blockDim.x;
	}
}

template<typename T> __global__ void Abs_inria_cplx(T *A, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        A[i].x = sqrt(A[i].x*A[i].x+A[i].y*A[i].y);
		A[i].y = 0;
		i += gridDim.x * blockDim.x;
    }
}

template<typename T, typename R> __global__ void Abs_inria_cplx2Real(T *A, R* B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        B[i] = sqrt(A[i].x*A[i].x+A[i].y*A[i].y);
		i += gridDim.x * blockDim.x;
    }
}

template<typename T, typename R> __global__ void cplx2real(T *A, R* B, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        B[i] = A[i].x;
		i += gridDim.x * blockDim.x;
    }
}

template<typename T, typename U> __global__ void Memcpy_inria(T* dev_dst, const U* dev_src, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        dev_dst[i] = (T)dev_src[i] ;
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Memset_inria(T* dev_dst, T valeur, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
    {
        dev_dst[i] = valeur ;
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void Memset_inria_cplx(T* dev_dst, T valeur, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < numElements)
	{
		dev_dst[i].x = valeur.x ;
		dev_dst[i].y = valeur.y ;
		i += gridDim.x * blockDim.x;
	}
}

template<typename T> __global__ void Sparse2full_inria(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < nnz)
    {
        int j = (dev_src_colind[i]) * src_dim1 + (dev_src_rowind[i]);
        dev_dst[j] =  dev_src_values[i];
	i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void AddSparse2full_inria(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < nnz)
    {
        int j = (dev_src_colind[i]) * src_dim1 + (dev_src_rowind[i]);
        dev_dst[j] +=  dev_src_values[i];
        i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void SubSparse2full_inria(T *dev_dst, const int *dev_src_rowind, const int *dev_src_colind, const T* dev_src_values, const int nnz, const int src_dim1)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < nnz)
    {
        int j = (dev_src_colind[i]) * src_dim1 + (dev_src_rowind[i]);
        dev_dst[j] -=  dev_src_values[i];
        i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void GetDiag_inria(T* dst_diag, const T* src_M, int dim1, int dlen)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < dlen)
    {
	    int j = i*(dim1+1);
	    dst_diag[i] = src_M[j];
	    i += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void AddDiagConst_inria(T *A, T val, int dim1)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < dim1)
    {
        int j = i*(dim1+1);
        A[j] = A[j] + val;
	i += gridDim.x * blockDim.x;
    }
}


template<typename T> __global__ void CopyDiag_inria(T *dst, T* src, int dim1)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    while (i < dim1)
    {
        int j = i*(dim1+1);
        dst[j] = src[j];
	i += gridDim.x * blockDim.x;
    }
}
template<typename T> __global__ void SetSubmatrix_inria(T* mat_dst, T* mat_src, int src_dim1, int r1, int c1, int nb_rows, int nb_elements)
{
    int ind = blockDim.x*blockIdx.x+threadIdx.x;
    while(ind<nb_elements)
    {
	    int col = (int)(ind/nb_rows) ;
            int ligne = ind - nb_rows*col;
	    mat_dst[ind] = mat_src[(col+c1)*src_dim1+(ligne+r1)];
	    ind += gridDim.x * blockDim.x;
    }
}

template<typename T> __global__ void RelativeError_inria(T* data_dst, const T* data_src_th, const T* data_src_mes, const int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    while(i<numElements)
    {
            data_dst[i] = fabs((data_src_th[i]-data_src_mes[i])/data_src_th[i]);
	    i += gridDim.x * blockDim.x;
    }
}


template<typename T> __global__ void RelativeError_inria_cplx(T* data_dst, const T* data_src_th, const T* data_src_mes, const int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    while(i<numElements)
    {
	    auto a_ = data_src_th[i].x;
	    auto b_ = data_src_th[i].y;
	    auto a = data_src_mes[i].x;
	    auto b = data_src_mes[i].y;
	    auto d = (a_ * a_ + b_ * b_);
	    auto da = a - a_;
	    auto db = b - b_;
	    auto x = (da * a_ + db * b_) / d;
	    auto y = (db * a_ - da * b_) / d;
	    data_dst[i].x = sqrt(x * x + y * y);
	    data_dst[i].y = 0;
	    i += gridDim.x * blockDim.x;
    }
}

/*template<typename T> __global__ void SetLinfAR1(T* mat_Linf, T* mat_Hinf, T* atur, int nb_n, int nb_elements)
{
    int ind = blockDim.x*blockIdx.x+threadIdx.x;
    while(ind<nb_elements)
    {
        int col = (int)(ind/nb_n) ;
        int ligne = ind - nb_n*col;
	int nb_az = nb/2;
	if(ligne < nb_az)
	{
             mat_Linf[ind] = atur[ligne] * mat_Hinf[ind];
	}
	else
	{
	     mat_Linf[ind] = mat_Hinf[col*nb_n + ligne-nb_az];
	}
	ind += gridDim.x * blockDim.x;
    }
}*/
template <typename T> __global__ void sum_abs_reduce(T *g_idata, T *g_odata, unsigned int n)
{
	//NOTE: this code is dead code (not used); it fails when the matrix g_idata is of size greater than the number of threads per block (synchronization issue)
	//TODO: optimize by using shared memory
//	extern __shared__ T sdata[];
	unsigned int tid = threadIdx.x;
	unsigned int offset1, offset2, width, boffset;
	width = 1;
	boffset = blockIdx.x*blockDim.x;
	offset1 = boffset+tid*2;
	//set the working copy
	if(boffset+tid < n)
	{
		g_odata[boffset+tid] = abs(g_idata[boffset+tid]);
		__threadfence();
		while(width < gridDim.x*blockDim.x)
		{
			offset2 = offset1 + width;
			if(offset2 < n)
			{
//				g_odata[offset2].x = g_odata[offset2].y = 0;
				g_odata[offset1].x += g_odata[offset2].x;
				g_odata[offset1].y += g_odata[offset2].y;
			}
			offset1 <<= 1;
			width <<= 1;
//			__syncthreads();
			__threadfence();
		}
	}
}

template <typename T> __global__ void sum_reduce(T *g_idata, T *g_odata, unsigned int n)
{
	//NOTE: this function is probably in the same case as sum_abs_reduce above TODO: test it
	//TODO: optimize by using shared memory
//	extern __shared__ T sdata[];
	unsigned int tid = threadIdx.x;
	unsigned int offset1, offset2, width, boffset;
	width = 1;
	boffset = blockIdx.x*blockDim.x;
	offset1 = boffset+tid*2;
	//set the working copy
	if(boffset+tid < n)
	{
		g_odata[boffset+tid] = g_idata[boffset+tid];
		__threadfence();
		while(width < gridDim.x*blockDim.x)
		{
			offset2 = offset1 + width;
			if(offset2 < n)
			{
//				g_odata[offset2].x = g_odata[offset2].y = 0;
				g_odata[offset1].x += g_odata[offset2].x;
				g_odata[offset1].y += g_odata[offset2].y;
			}
			offset1 <<= 1;
			width <<= 1;
//			__syncthreads();
			__threadfence();
		}
	}
}

template <typename T> __device__ bool is_greater(T a, T b)
{
		return a.x*a.x+a.y*a.y > b.x*b.x+b.y*b.y;
}

template <typename T> __device__ bool is_lower(T a, T b)
{
		return a.x*a.x+a.y*a.y < b.x*b.x+b.y*b.y;
}

template <typename T> __device__ T abs(T a)
{
		T b;
		b.x = sqrt(a.x*a.x+a.y*a.y);
		b.y = 0;
		return b;
}


template <typename T> __global__ void min_max_reduce(T *g_idata, T *g_odata, unsigned int n, bool max_func)
{
	//TODO: optimize by using shared memory
//	extern __shared__ T sdata[];
	unsigned int tid = threadIdx.x;
	unsigned int offset1, offset2, width, boffset;
	width = 1;
	boffset = blockIdx.x*blockDim.x;
	offset1 = boffset+tid*2;
	//set the working copy
	if(boffset+tid < n)
	{
		g_odata[boffset+tid] = g_idata[boffset+tid];
		__threadfence();
		while(width < /*gridDim.x**/blockDim.x)
		{
			offset2 = offset1 + width;
			T a = g_odata[offset1];
			T b = g_odata[offset2];
			if(offset2 < n)
			{
//					if(a.x*a.x+a.y*a.y < b.x*b.x+b.y*b.y)
//					{
//					}
					if(/* max */ max_func && is_lower(a, b) || /* min */ !max_func && is_greater(a, b))
							g_odata[offset1] = g_odata[offset2];
//					g_odata[offset2].x = 0;
//					g_odata[offset2].y = 0;
			}
			offset1 <<= 1;
			width <<= 1;
			__threadfence();
		}
	}
}

template <typename T> __global__ void finish_min_max_reduce(T *g_idata, T *g_odata, unsigned int n, bool max_func)
{
	unsigned int tid = threadIdx.x;
//	unsigned int offset1, offset2, width, boffset;
	unsigned int offset2;
	if(blockIdx.x == gridDim.x-1)
	{ // last block
		// synchronize all blocks work
		// first try: only one thread
		if(tid == 0)
		{
			//first thread of last block
			T a = g_odata[0], b;
			//max of each first element every blockDim.x
			for(int i=1; i < gridDim.x; i++)
			{
				offset2 = i*blockDim.x;
				if(offset2 < n)
				{
					b = g_odata[offset2];
					if(/* max */ max_func && is_lower(a, b) || /* min */ !max_func && is_greater(a, b))
					{
						g_odata[0] = b;
						a = b;
					}
				}
			}
		}
	}
}

template<typename T> __global__ void butterfly_diag_prod_cplx(const T *X, const T *D1, const T *D2, T * out, const int* ids, int x_nrows, int x_ncols)
{
#ifdef BUTTERFLY_KERNEL_NO_SHARED_MEMORY
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int offset;

    // compute D1 * X + D2 * X[ids]
    while (i < x_nrows)
	{
		offset = 0;
		for(int j = 0; j < x_ncols; j++)
		{
			T x_id = X[ids[i] + offset];
			T x = X[i + offset];
			out[i+offset].x = x.x * D1[i].x - x.y * D1[i].y + x_id.x * D2[i].x - x_id.y * D2[i].y; // real part
			out[i+offset].y = x.x * D1[i].y + D1[i].x * x.y + x_id.x * D2[i].y + D2[i].x * x_id.y;	 // imag part
			offset  += x_nrows;
		}
		i += gridDim.x * blockDim.x;
	}
#else
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int offset;
    __shared__ T sD1[256];
    __shared__ T sD2[256];
    // compute D1 * X + D2 * X[ids]
    while (i < x_nrows)
	{
		offset = 0;
		sD1[threadIdx.x] = D1[i];
		sD2[threadIdx.x] = D2[i];
		for(int j = 0; j < x_ncols; j++)
		{
			T x_id = X[ids[i] + offset];
			T x = X[i + offset];
			T o;
			o.x = x.x * sD1[threadIdx.x].x - x.y * sD1[threadIdx.x].y + x_id.x * sD2[threadIdx.x].x - x_id.y * sD2[threadIdx.x].y; // real part
			o.y = x.x * sD1[threadIdx.x].y + sD1[threadIdx.x].x * x.y + x_id.x * sD2[threadIdx.x].y + sD2[threadIdx.x].x * x_id.y;	 // imag part
			out[i+offset] = o;
			offset  += x_nrows;
		}
		i += gridDim.x * blockDim.x;
	}
#endif
}

template<typename T> __global__ void butterfly_diag_prod(const T *X, const T *D1, const T *D2, T * out, const int* ids, int x_nrows, int x_ncols)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int offset;
    while (i < x_nrows)
    {
	    offset = 0;
	    for(int j = 0; j < x_ncols; j++)
	    {
		    T x_id = X[ids[i] + offset];
		    T x = X[i + offset];
		    out[i+j*x_nrows] = x * D1[i] + x_id * D2[i];
		    offset += x_nrows;
	    }
	    i += gridDim.x * blockDim.x;
    }
}
#include "kernel_def.hu"
