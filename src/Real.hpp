
template<>
struct _Real<float>
{
	using value_type = float;
};


template<>
struct _Real<double>
{
	using value_type = double;
};


template<>
struct _Real<std::complex<float>>
{
	using value_type = float;
};


template<>
struct _Real<std::complex<double>>
{
	using value_type = double;
};

template<>
struct _Real<float2>
{
	using value_type = float;
};


template<>
struct _Real<double2>
{
	using value_type = double;
};


