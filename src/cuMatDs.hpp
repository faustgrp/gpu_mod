template<typename T> cublasHandle_t cuMatDs<T>::handle = 0;

template<typename T>
cuMatDs<T>::cuMatDs(int32_t nrows, int32_t ncols, T* data, int32_t buf_nrows/*=-1*/, int32_t buf_ncols/*=-1*/) : cuMatDs<T>(nrows, ncols, buf_nrows, buf_ncols, data)
{
}

template<typename T>
cuMatDs<T>::cuMatDs(int32_t nrows, int32_t ncols, int32_t buf_nrows/*=-1*/, int32_t buf_ncols/*=-1*/, T* data /* = nullptr */, int32_t dev_id/*=-1*/) : cuMat<T>::cuMat(nrows, ncols), buf_nrows(buf_nrows), buf_ncols(buf_ncols), stream(nullptr), dev_id(cur_dev())
{

	if(! handle) cublasCreate(&handle);
	set_buf_nrows_ncols(this->buf_nrows, this->buf_ncols, this->nrows, this->ncols, "cuMatDs<T>::cuMatDs()");
	if(data == nullptr)
	{
		if(dev_id == -1)
			dev_id = cur_dev();
		alloc_dbuf(this->buf_nrows*this->buf_ncols, &(this->data), dev_id);
	}
	else this->data = data;
	if(dev_id != -1)
		this->dev_id = dev_id;
}

	template<typename T>
cuMatDs<T>::cuMatDs() : cuMat<T>(), data(nullptr), buf_nrows(0), buf_ncols(0), dev_id(-1), stream(nullptr)
{

}

	template<typename T>
cuMatDs<T>::cuMatDs(cuMatDs<T> &src) : cuMatDs<T>(src.nrows, src.ncols, src.buf_nrows, src.buf_ncols, /* data */nullptr, src.dev_id)
{
	stream = src.stream;
	copy_dbuf2dbuf(buf_nrows*buf_ncols, src.data, data, src.dev_id, dev_id, stream);
}

	template<typename T>
cuMatDs<T>::cuMatDs(cuMatDs<T> &&src) : cuMatDs<T>()
{
	*this = std::move(src);
}

	template<typename T>
cuMatDs<T>& cuMatDs<T>::operator=(const cuMatDs<T> &src)
{
	free_dbuf(data);
	alloc_dbuf(buf_nrows*buf_ncols, &data);
	stream = src.stream;
	copy_dbuf2dbuf(buf_nrows*buf_ncols, src.data, data, src.dev_id, dev_id, stream);
	this->nrows = src.nrows;
	this->ncols = src.ncols;
	buf_nrows = src.buf_nrows;
	buf_ncols = src.buf_ncols;
	dev_id = src.dev_id;
	stream = src.stream;
	return *this;
}

	template<typename T>
cuMatDs<T>& cuMatDs<T>::operator=(const cuMatDs<T>&& src)
{
	free_dbuf(data);
	data = src.data;
	src.data = nullptr;
	copy_attrs(src);
	return *this;
}

template<typename T>
void cuMatDs<T>::copy_attrs(const cuMatDs<T>& other, bool including_dev_id/*=true*/)
{
	buf_nrows = other.buf_nrows;
	buf_ncols = other.buf_ncols;
	stream = other.stream;
	if(including_dev_id)
		dev_id = other.dev_id;
	stream = other.stream;
	Mat::operator=(other);
}

template<typename T>
void cuMatDs<T>::transpose()
{
	auto switch_back = switch_dev(this->dev_id);
	apply_op(OP_TRANSP);
	switch_back();
}

template<typename T>
void cuMatDs<T>::adjoint()
{
	auto switch_back = switch_dev(this->dev_id);
	apply_op(OP_CONJTRANSP);
	switch_back();
}

template<typename T>
void cuMatDs<T>::conjugate()
{
	//TODO: an efficient kernel to conjugate directly
	auto switch_back = switch_dev(dev_id);
	adjoint();
	transpose();
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(cuMatSp<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	T one;
	auto other_ds = cuMatDs<T>::create(other);
	set_one(&one);
	add(*other_ds, one);
	delete other_ds;
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(cuMatDs<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	T one;
	set_one(&one);
	add(other, one);
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(hMatSp<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	auto cu_dsmat = cuMatDs<T>::create(other);
	add(cu_dsmat);
	delete cu_dsmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(hMatDs<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	auto cu_dsmat = cuMatDs<T>::create(other);
	add(*cu_dsmat);
	delete cu_dsmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(T* data, int32_t nrows, int32_t ncols)
{
	auto switch_back = switch_dev(this->dev_id);
	hMatDs<T> mat(nrows, ncols, data);
	add(mat);
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(cuMatDs<T>& other, const T& alpha)
{
	auto switch_back = switch_dev(this->dev_id);
	if(other.nrows != this->nrows || other.ncols != this->ncols)
		throw std::runtime_error("Dimensions of the two matrices must be equal.");
	//TODO: replace by a sum cuda kernel
	if(other.nrows != this->nrows || other.ncols != this->ncols)
		throw std::runtime_error("cuMatDs::add the dimensions must agree.");
	int m, n, k;
	int32_t lda, ldb, ldc;
	lda = ldb = ldc = this->nrows;
	T one;
	set_one(&one);
	m = this->nrows;
	n = this->ncols;
	k = this->nrows;
	auto A_id = cuMatDs<T>::createEye(m, m);
	auto status = cublasTgemm(cuMatDs<T>::handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k,
			/* alpha*/ &alpha,	A_id->data, lda, other.data, ldb,
			/* beta */ &one, this->data, ldc);
	check_cublas_error(status, "cuMatDs::add > cublasTgemm");
	delete A_id;
	switch_back();
}

template<typename T>
void cuMatDs<T>::add(int32_t nrows, int32_t ncols, int32_t nnz, const int32_t* row_ptr, const int32_t* col_inds, const T* values)
{
	auto switch_back = switch_dev(this->dev_id);
	auto spmat = cuMatSp<T>::create(nrows, ncols, values, row_ptr, col_inds, nnz);
	add(*spmat);
	delete spmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::sub(cuMatDs<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	T one, minus_one;
	set_one(&one);
	minus_one = minus_scal(one);
	add(other, minus_one);
	switch_back();
}

template<typename T>
void cuMatDs<T>::sub(cuMatSp<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	T one, minus_one;
	auto other_ds = cuMatDs<T>::create(other);
	set_one(&one);
	minus_one = minus_scal(one);
	add(*other_ds, minus_one);
	switch_back();
}

template<typename T>
void cuMatDs<T>::sub(hMatSp<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	auto cu_dsmat = cuMatDs<T>::create(other);
	sub(cu_dsmat);
	delete cu_dsmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::sub(hMatDs<T>& other)
{
	auto switch_back = switch_dev(this->dev_id);
	auto cu_dsmat = cuMatDs<T>::create(other);
	sub(*cu_dsmat);
	delete cu_dsmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::sub(T* data, int32_t nrows, int32_t ncols)
{
	auto switch_back = switch_dev(this->dev_id);
	hMatDs<T> mat(nrows, ncols, data);
	sub(mat);
	switch_back();
}

template<typename T>
void cuMatDs<T>::sub(int32_t nrows, int32_t ncols, int32_t nnz, const int32_t* row_ptr, const int32_t* col_inds, const T* values)
{
	auto switch_back = switch_dev(this->dev_id);
	auto spmat = cuMatSp<T>::create(nrows, ncols, values, row_ptr, col_inds, nnz);
	sub(*spmat);
	delete spmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::apply_op(gm_Op op)
{
	auto switch_back = switch_dev(this->dev_id);
	cuMatDs<T>* out;
	if(op == OP_NOTRANSP)
		out = cuMatDs<T>::create(this->nrows, this->ncols, buf_nrows, buf_ncols);
	else
		out = cuMatDs<T>::create(this->ncols, this->nrows, buf_ncols, buf_nrows);
	cuMatDs<T>::apply_op(this, op, out);
	auto tmp = data;
	data = out->data;
	out->data = tmp;
	this->nrows = out->nrows;
	this->ncols = out->ncols;
	this->buf_nrows = out->buf_nrows;
	this->buf_ncols = out->buf_ncols;
	delete out;
	switch_back();
}

	template<typename T>
cuMatDs<T>* cuMatDs<T>::apply_op(const cuMatDs<T>* in, gm_Op op, cuMatDs<T>* out /* = nullptr*/)
{
	auto switch_back = switch_dev(in->dev_id);
	T beta, alpha_one;
	if(out == nullptr)
	{
		if(op == OP_NOTRANSP)
			out = create(in->nrows, in->ncols, in->buf_nrows, in->buf_ncols);
		else
			out = create(in->ncols, in->nrows, in->buf_ncols, in->buf_nrows);
	}
	if(op == OP_NOTRANSP)
	{
		in->copy(out);
		return out;
	}
	set_one(&alpha_one);
	bzero(&beta, sizeof(T));
	auto cublas_op = gm_Op2cublas(op);

	// geam supports in-place op https://docs.nvidia.com/cuda/archive/9.2/cublas/index.html
	// only if A is not transposed (so it's not interesting here, we must create one extra buffer of the result)
	cublasStatus_t status = cublasTgeam(cuMatDs<T>::handle, cublas_op, CUBLAS_OP_N,
			in->ncols, in->nrows,
			&alpha_one, in->data, in->nrows,
			&beta, // zero
			out->data,
			in->ncols,
			out->data, in->ncols);
	check_cublas_error(status, "cublasTgeam called by cuMatDs::apply_op");
	auto itmp = in->nrows;
	out->nrows = in->ncols;
	out->ncols = itmp;
	switch_back();
	return out;
}

template<typename T>
void cuMatDs<T>::mul(cuMatDs<T>& other, T* output, gm_Op op_this/* = OP_NOTRANSP*/, gm_Op op_other/* = OP_NOTRANSP*/)
{
	auto switch_back = switch_dev(this->dev_id);
	auto cu_out = this->mul(other, (cuMatDs<T>*)nullptr, op_this, op_other);
	copy_dbuf2hbuf(cu_out->nrows*cu_out->ncols, cu_out->data, output, this->dev_id, this->stream);
	delete cu_out;
	switch_back();
}

template<typename T>
void cuMatDs<T>::mul(cuMatDs<T>& other, hMatDs<T>& output, gm_Op op_this/* = OP_NOTRANSP*/, gm_Op op_other/* = OP_NOTRANSP*/)
{
	auto switch_back = switch_dev(this->dev_id);
	auto cu_out = this->mul(other, (cuMatDs<T>*) nullptr, op_this, op_other);
	copy_dbuf2hbuf(cu_out->nrows*cu_out->ncols, cu_out->data, output->data, this->dev_id, this->stream);
	delete cu_out;
	switch_back();
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::mul(cuMatDs<T>& other, cuMatDs<T>* output /* = nullptr*/, gm_Op op_this /* OP_NOTRANSP*/ , gm_Op op_other /* OP_NOTRANSP*/)
{
	T alpha, beta;
	set_one(&alpha);
	bzero(&beta, sizeof(beta));
	int m = op_this==OP_NOTRANSP?this->nrows:this->ncols;
	int n = op_other==OP_NOTRANSP?other.ncols:other.nrows;
	if(output == nullptr)
		output = cuMatDs<T>::create(m, n);
	dsm_gemm(this, &other, output, alpha, beta, op_this, op_other);
	return output;
}

#if (CUDA_VERSION <= 9200)
template<typename T>
cuMatDs<T>* cuMatDs<T>::mul(cuMatSp<T>& other, cuMatDs<T>* output /* = nullptr*/, gm_Op op_this /* OP_NOTRANSP*/ , gm_Op op_other /* OP_NOTRANSP*/)
{
	auto switch_back = switch_dev(this->dev_id);
	int m, n, k;
	int ldb, ldc;
	T alpha, beta;
	cusparseOperation_t transa, transb;
	gm_Op output_op;
	T* dense_mat_data;
	cuMatDs<T>* extra_mat = nullptr;
	// csrmm2 computes C = α ∗ op ( A ) ∗ op ( B ) + β ∗ C
	// A must be sparse and B dense (but this is the reverse order that we need)
	// so the function computes (op_other(other)^*op_this(this)^*)^*
	m = other.nrows;
	k = other.ncols;
	set_one(&alpha);
	bzero(&beta, sizeof(T));
	if(op_this == op_other && op_this == OP_NOTRANSP)
	{
		n = this->nrows;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.ncols, this->nrows);
		auto transpo_this = cuMatDs<T>::create(this->ncols, this->nrows);
		cuMatDs<T>::apply_op(this, OP_TRANSP, transpo_this);
		ldb = m;
		ldc = k;
		transa = CUSPARSE_OPERATION_TRANSPOSE;
		transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = OP_TRANSP;
		dense_mat_data = transpo_this->data;
		extra_mat = transpo_this;
	}
	else if(op_this == op_other && op_this != OP_NOTRANSP)
	{
		n = this->ncols;
		ldb = k;
		ldc = m;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->ncols);
		transa = transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = op_this;
		dense_mat_data = this->data;
	}
	else if(op_this != OP_NOTRANSP && op_other == OP_NOTRANSP)
	{
		n = this->ncols;
		ldb = m;
		ldc = k;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.ncols, this->ncols);
		auto cusparse_op_this = gm_Op2cusparse(op_this);
		transa = cusparse_op_this;
		transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = op_this;
		dense_mat_data = this->data;
	}
	else if(op_this == OP_NOTRANSP && op_other == OP_TRANSP)
	{
		n = this->nrows;
		ldb = n;
		ldc = m;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->nrows);
		auto cusparse_op_other = gm_Op2cusparse(op_other);
		transa = CUSPARSE_OPERATION_NON_TRANSPOSE;
		transb = cusparse_op_other;
		output_op = op_other;
		dense_mat_data = this->data;
	}
	else if(op_this == OP_NOTRANSP && op_other == OP_CONJTRANSP)
		//unfortunately cusparse_csr2dense doesn't support CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE for dense matrix 2nd argument
		//https://docs.nvidia.com/cuda/archive/9.2/cusparse/index.html#cusparse-lt-t-gt-csrmm2
	{

		auto adjoint_this = cuMatDs<T>::create(this->ncols, this->nrows);
		this->copy(adjoint_this);
		adjoint_this->adjoint();
		n = this->nrows;
		ldb = k;
		ldc = m;
		transa = transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = op_other;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->nrows);
		dense_mat_data = adjoint_this->data;
		extra_mat = adjoint_this;
	}
	else if(op_this == OP_CONJTRANSP && op_other == OP_TRANSP || op_this == OP_TRANSP && op_other == OP_CONJTRANSP)
	{
		auto other_dense = cuMatDs<T>::create(other.ncols, other.nrows);
		cusparse_csr2dense(&other, other_dense, op_other);
		output_op = OP_NOTRANSP;
		if(output == nullptr)
			output = cuMatDs<T>::create(this->ncols, other_dense->ncols);
		output = this->mul(*other_dense, output, op_this, OP_NOTRANSP);
		delete other_dense;
		return output;
	}
	cusparseStatus_t status = cusparseTcsrmm2(other.handle,
			transa, transb,
			m, n, k,
			other.nnz,
			&alpha,
			other.descr, other.values, other.rowptr, other.colids,
			dense_mat_data, ldb,
			&beta, output->data, ldc);
	check_cusparse_error(status, "cuMatDs::mul(cuMatSp) cusparseTcsrmm2" );
	if(output_op != OP_NOTRANSP)
		output->apply_op(output_op);
	if(extra_mat)
		delete extra_mat;

	switch_back();
	return output;
}

#else
template<typename T>
cuMatDs<T>* cuMatDs<T>::mul(cuMatSp<T>& other, cuMatDs<T>* output /* = nullptr*/, gm_Op op_this /* OP_NOTRANSP*/ , gm_Op op_other /* OP_NOTRANSP*/)
{
	auto switch_back = switch_dev(this->dev_id);
	T alpha, beta;
	cusparseOperation_t transa, transb;
	gm_Op output_op;
	cuMatDs<T>* dense_mat;
	cuMatSp<T>* sp_mat;
	cuMat<T>* extra_mat = nullptr;
	cusparseStatus_t status;
	size_t bufferSize;
	std::string err_output_sz = "cuMatDs::mul the output buffer is not large enough.";
	void* externalBuffer;
	// csrmm2 computes C = α ∗ op ( A ) ∗ op ( B ) + β ∗ C
	// A must be sparse and B dense (but this is the reverse order that we need)
	// so the function computes (op_other(other)^*op_this(this)^*)^*
	set_one(&alpha);
	bzero(&beta, sizeof(T));
	if(op_this == op_other && op_this == OP_NOTRANSP)
	{
		if(output == nullptr)
			output = cuMatDs<T>::create(other.ncols, this->nrows);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.ncols*this->nrows)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.ncols;
			output->ncols = this->nrows;
		}
		auto transpo_other = other.clone();
		transpo_other->transpose();
		transa = CUSPARSE_OPERATION_NON_TRANSPOSE;
		transb = CUSPARSE_OPERATION_TRANSPOSE;
		output_op = OP_TRANSP;
		dense_mat = this;
		extra_mat = sp_mat = transpo_other;
	}
	else if(op_this == op_other && op_this != OP_NOTRANSP)
	{
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->ncols);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.nrows*this->ncols)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.nrows;
			output->ncols = this->ncols;
		}
		transa = transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = op_this;
		dense_mat = this;
		sp_mat = &other;
	}
	else if(op_this != OP_NOTRANSP && op_other == OP_NOTRANSP)
	{
		if(output == nullptr)
			output = cuMatDs<T>::create(other.ncols, this->ncols);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.ncols*this->ncols)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.ncols;
			output->ncols = this->ncols;
		}
		auto cusparse_op_this = gm_Op2cusparse(op_this);
		// spMM doesn't support OP_TRANSP for a sparse matrix, need to do it before manually
		sp_mat = other.clone();
		if(op_this == OP_TRANSP)
			sp_mat->transpose();
		else // op_this == OP_TRANSP
			sp_mat->adjoint();
		extra_mat = sp_mat;
		transa = CUSPARSE_OPERATION_NON_TRANSPOSE;
		transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = op_this;
		dense_mat = this;
	}
	else if(op_this == OP_NOTRANSP && op_other == OP_TRANSP)
	{
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->nrows);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.nrows*this->nrows)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.nrows;
			output->ncols = this->nrows;
		}
		auto cusparse_op_other = gm_Op2cusparse(op_other);
		transa = CUSPARSE_OPERATION_NON_TRANSPOSE;
		transb = cusparse_op_other;
		output_op = op_other;
		dense_mat = this;
		sp_mat = &other;
	}
	else if(op_this == OP_NOTRANSP && op_other == OP_CONJTRANSP)
	{

		auto adjoint_this = cuMatDs<T>::create(this->ncols, this->nrows);
		this->copy(adjoint_this);
		adjoint_this->adjoint();
		transa = transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
		output_op = op_other;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->nrows);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.nrows*this->nrows)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.nrows;
			output->ncols = this->nrows;
		}
		extra_mat = dense_mat = adjoint_this;
		sp_mat = &other;
	}
	else if(op_this == OP_CONJTRANSP && op_other == OP_TRANSP)
	{
		output_op = op_this;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->ncols);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.nrows*this->ncols)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.nrows;
			output->ncols = this->ncols;
		}
		extra_mat = sp_mat = other.clone();
		sp_mat->conjugate();
		dense_mat = this;
		transa = transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
	}
	else if(op_this == OP_TRANSP && op_other == OP_CONJTRANSP)
	{
		output_op = op_other;
		if(output == nullptr)
			output = cuMatDs<T>::create(other.nrows, this->ncols);
		else
		{
			if(output->buf_nrows * output->buf_ncols < other.nrows*this->ncols)
				throw std::runtime_error(err_output_sz);
			output->nrows = other.nrows;
			output->ncols = this->ncols;
		}
		extra_mat = dense_mat = this->clone();
		dense_mat->conjugate();
		sp_mat = &other;
		transa = transb = CUSPARSE_OPERATION_NON_TRANSPOSE;
	}
	// cusparseSpMM doesn't handle op_other == OP_CONJTRANSP
	// cusparseSpMM only handle transa == CUSPARSE_OPERATION_NON_TRANSPOSE
	// https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-generic-function-spmm (it's written in small in the end! "opB == CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE is not supported ")
	helper_cusparseSpMM(*sp_mat, *dense_mat, transa, transb, alpha, beta, *output, "cuMatDs::mul(cuMatSp)");

	if(output_op != OP_NOTRANSP)
		output->apply_op(output_op);
	if(extra_mat)
		delete extra_mat;
	switch_back();
	return output;
}
#endif

template<typename T>
void cuMatDs<T>::setOnes()
{
	auto switch_back = switch_dev(this->dev_id);
	T* ones_hmat;
	ones_hmat = new T[numel()];
	for(int i=0;i < numel(); i++) set_one(ones_hmat+i);
	copy_hbuf2dbuf(numel(), ones_hmat, this->data, dev_id, stream);
	delete ones_hmat;
	switch_back();
}

template<typename T>
void cuMatDs<T>::set_zeros()
{
	auto switch_back = switch_dev(this->dev_id);
	T* zeros_hmat;
	zeros_hmat = new T[numel()];
	bzero(zeros_hmat,numel()*sizeof(T));
	copy_hbuf2dbuf(numel(), zeros_hmat, this->data, dev_id, stream);
	delete zeros_hmat;
	switch_back();
}

template<typename T>
size_t cuMatDs<T>::get_nnz() const
{
	return numel();
}

template<typename T>
size_t cuMatDs<T>::get_nbytes() const
{
	return numel()*sizeof(T);
}

template<typename T>
size_t cuMatDs<T>::numel() const
{
	return this->nrows*this->ncols;
}


template<typename T>
Real<T> cuMatDs<T>::norm_frob()
{
	auto switch_back = switch_dev(this->dev_id);
	Real<T> alpha;
	cublasTnrm2(cuMatDs<T>::handle, this->ncols*this->nrows, data, 1, &alpha);
	switch_back();
	return alpha;
}

template<typename T>
Real<T> cuMatDs<T>::spectral_norm(const float threshold, const int32_t max_iter)
{
	auto switch_back = switch_dev(this->dev_id);
	cuMatDs<T>* AtA;
//	cuMatDs<T>::apply_op(this, OP_CONJTRANSP, At);
	Real<T> norm;
	if(this->ncols < this->nrows)
	{
		AtA = cuMatDs<T>::create(this->ncols, this->ncols);
		mul(*this, AtA, OP_CONJTRANSP, OP_NOTRANSP);
	}
	else
	{
		AtA = cuMatDs<T>::create(this->nrows, this->nrows);
		mul(*this, AtA, OP_NOTRANSP, OP_CONJTRANSP);
	}
	T power_ite = AtA->power_iteration(threshold, max_iter);
	norm = std::abs(gm_sqrt<T, STL<T>>(power_ite));
	delete AtA;
	switch_back();
	return norm;
}

template<typename T>
void cuMatDs<T>::destroy()
{
	auto switch_back = switch_dev(this->dev_id);
	cuMatDs<T>::destroy(this); // could call directly delete on this but prefer rely on already established code (in case of modif. on the latter)
	switch_back();
}

template<typename T>
cuMatDs<T>::~cuMatDs<T>()
{
	auto switch_back = switch_dev(this->dev_id);
	//std::cout<< "cuMatDs dtor is_sparse: " << is_sparse << " is_cuda: " << is_cuda << " ptr: " << this << std::endl;
	free_dbuf(data);
	switch_back();
}

template<typename T>
void cuMatDs<T>::destroy(cuMatDs<T> * inst)
{
	auto switch_back = switch_dev(inst->dev_id);
//	std::cout << "void cuMatDs<T>::destroy(cuMatDs<T> * inst)" << std::endl;
	delete inst;
	switch_back();
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::create(cuMatSp<T>& sp_mat, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	// create ds_mat on the same device as sp_mat to use cusparse_csr2dense
	auto switch_back = switch_dev(sp_mat.dev_id);
	auto ds_mat = cuMatDs<T>::create(sp_mat.nrows, sp_mat.ncols, /*buf_nrows=*/-1, /*buf_ncols=*/-1, dev_id=sp_mat.dev_id, stream);
	cusparse_csr2dense(&sp_mat, ds_mat, OP_NOTRANSP);
	switch_back();
	return ds_mat;
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::create(hMatDs<T> &hmat, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	return cuMatDs<T>::create(hmat.nrows, hmat.ncols, hmat.data, /*buf_nrows*/-1, /*buf_ncols*/-1, dev_id, stream);
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::create(int32_t nrows, int32_t ncols, T* data,
		int32_t buf_nrows/*=-1*/, int32_t buf_ncols/*=-1*/,
		int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	//	std::cout << "cuMatDs<T>::create nrows: " << nrows << " ncols: " << ncols << " data: " << data  << std::endl;
	auto cu_mat = cuMatDs<T>::create(nrows, ncols, buf_nrows, buf_ncols, dev_id, stream);
	copy_hbuf2dbuf(nrows*ncols, data, cu_mat->data, dev_id, stream);
	//std::cout << "cuMatDs create is_sparse: " << cu_mat->is_sparse()  << " is_cuda: " <<  cu_mat->is_cuda() << "cu_mat ptr: " << cu_mat << std::endl;
	return cu_mat;
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::create(hMatSp<T> &hmat, int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	auto switch_back = switch_dev(dev_id);
	auto cu_spmat = cuMatSp<T>::create(hmat);
	auto cu_dsmat = cuMatDs<T>::create(cu_spmat);
	delete cu_spmat;
	switch_back();
	return cu_dsmat;
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::clone(int32_t dev_id/*=-1*/)
{
	auto cu_mat = cuMatDs<T>::create(this->nrows, this->ncols, buf_nrows, buf_ncols, dev_id);
//	cublasTcopy(cuMatDs<T>::handle, numel(), data, 1, cu_mat->data, 1);
	copy_dbuf2dbuf(buf_nrows*buf_ncols, this->data, cu_mat->data, this->dev_id, dev_id, this->stream);
	return cu_mat;
}

template<typename T>
void cuMatDs<T>::copy(cuMatDs<T>* cu_mat) const
{
	//	std::cout << "void cuMatDs<T>::copy(cuMatDs<T>* cu_mat)" << std::endl;
	if(cu_mat->buf_nrows*cu_mat->buf_ncols < this->nrows*this->ncols)
	{
		std::cerr << "src buffer size:" << buf_nrows << "x" << buf_ncols << " dst buffer size:" << cu_mat->buf_nrows << "x" << cu_mat->buf_ncols << std::endl;
		throw std::runtime_error("The destination buffer is not large enough for the copy.");
	}
	//TODO: resize instead of error as it's done in cuMatSp?
//	cublasTcopy(cuMatDs<T>::handle, nrows*ncols, data, 1, cu_mat->data, 1);
	copy_dbuf2dbuf(this->nrows*this->ncols, this->data, cu_mat->data, this->dev_id, cu_mat->dev_id, this->stream);
	cu_mat->nrows = this->nrows;
	cu_mat->ncols = this->ncols;
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::create(int32_t nrows, int32_t ncols, int32_t buf_nrows/*=-1*/, int32_t buf_ncols/*=-1*/,
		int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	set_buf_nrows_ncols(buf_nrows, buf_ncols, nrows, ncols, "cuMatDs<T>::create()");
	auto cu_mat = new cuMatDs<T>(nrows, ncols, buf_nrows, buf_ncols, nullptr/*, cu_data*/, dev_id);
	return cu_mat;
}

template<typename T>
cuMatDs<T>* cuMatDs<T>::createEye(int32_t nrows, int32_t ncols, int32_t buf_nrows, int32_t buf_ncols,
		int32_t dev_id/*=-1*/, void* stream/*=nullptr*/)
{
	auto eye = cuMatDs<T>::create(nrows, ncols, buf_nrows, buf_ncols, dev_id, stream);
	eye->set_eyes();
	return eye;
}

template<typename T>
void cuMatDs<T>::set_eyes()
{
	T *eye_hmat;
	eye_hmat = new T[numel()];
	bzero(eye_hmat, sizeof(T)*numel());
	auto min_nrows_cols = this->nrows<this->ncols?this->nrows:this->ncols;
	for(int i=0,j=0;i < numel() && j < min_nrows_cols; i+=this->nrows+1, j++) set_one(eye_hmat+i);
	copy_hbuf2dbuf(numel(), eye_hmat, data, dev_id, stream);
	delete eye_hmat;
}

template<typename T>
void cuMatDs<T>::cpu_set(const T* data, int32_t nrows, int32_t ncols)
{
	if(this->nrows != nrows || this->ncols != ncols)
		resize(nrows, ncols);
	copy_hbuf2dbuf(numel(), data, this->data, dev_id, stream);
}

template<typename T>
void cuMatDs<T>::set_buf_nrows_ncols(int32_t &buf_nrows, int32_t &buf_ncols,
		const int32_t nrows, const int32_t ncols,
		const std::string caller /* = ""*/)
{
	if(buf_nrows < 0)
		buf_nrows = nrows;
	if(buf_ncols < 0)
		buf_ncols = ncols;
	if(buf_nrows*buf_ncols < nrows*ncols)
	{
		std::cerr <<  "buf_nrows=" << buf_nrows << " buf_ncols=" << buf_ncols << " nrows=" << nrows << " ncols=" << ncols << std::endl;
		throw std::runtime_error(caller+" assertion failed: buf_nrows >= nrows && buf_ncols >= ncols.");
	}
}

/********************* non-member functions */

template<typename T>
void dsm_dot(cuMatDs<T>* x, cuMatDs<T>* y, T* alpha)
{
	auto switch_back = switch_dev(x->dev_id);
	//TODO: warning if x or y is not a vector (at least a dim size must be 1)
	cuMatDs<T> *x_mat = static_cast<cuMatDs<T>*>(x);
	cuMatDs<T> *y_mat = static_cast<cuMatDs<T>*>(y);
	cublasTdot(cuMatDs<T>::handle, x_mat->nrows*x_mat->ncols, x_mat->data, 1, y_mat->data, 1, alpha);
}

	template<typename T>
gm_DenseMat_t dsm_create(int32_t nrows, int32_t ncols, T* data, int32_t buf_nrows, int32_t buf_ncols, const void* stream/*=nullptr*/)
{
	return cuMatDs<T>::create(nrows, ncols, data, buf_nrows, buf_ncols, /*dev_id*/-1, const_cast<void*>(stream));
}

	template<typename T>
gm_DenseMat_t dsm_create(int32_t nrows, int32_t ncols, int32_t buf_nrows, int32_t buf_ncols)
{
	return cuMatDs<T>::create(nrows, ncols, buf_nrows, buf_ncols);
}

template<typename T>
void dsm_tocpu(gm_DenseMat_t cu_mat, T* data, uint32_t offset /*= 0*/, int32_t size /* = -1 */)
{
	cuMatDs<T>* cu_mat_ds = static_cast<cuMatDs<T>*>(cu_mat);
	if(cu_mat_ds->is_sparse() || ! cu_mat_ds->is_cuda())
		throw std::runtime_error("dsm_tocpu error: matrix is sparse or not cuda");
	if(size == -1)
		size = cu_mat_ds->nrows*cu_mat_ds->ncols;
	else
		if(size > cu_mat_ds->buf_nrows*cu_mat_ds->buf_ncols)
		{
			throw std::runtime_error("dsm_tocpu error: the given offset and size overflow the dense mat buffer.");
		}
	copy_dbuf2hbuf(size, cu_mat_ds->data + offset, data, cu_mat_ds->dev_id, cu_mat_ds->stream);
}

	template<typename T>
void dsm_get_info(gm_DenseMat_t cu_mat, int32_t *nrows, int32_t *ncols)
{
	//	std::cout << "dsm_get_info cu_mat=" << cu_mat << std::endl;
	cuMatDs<T>* cu_mat_ds = static_cast<cuMatDs<T>*>(cu_mat);
	if(cu_mat_ds->is_sparse() || ! cu_mat_ds->is_cuda())
		throw std::runtime_error("dsm_get_info error: matrix is sparse or not cuda");
	if(nrows != nullptr)
		*nrows = cu_mat_ds->nrows;
	if(ncols != nullptr)
		*ncols = cu_mat_ds->ncols;
}

template<typename T>
T cuMatDs<T>::power_iteration(const float threshold, const int32_t max_iter)
{
	auto switch_back = switch_dev(this->dev_id);
	auto xk = cuMatDs<T>::create(this->ncols, 1, this->nrows<this->ncols?this->ncols:this->nrows, 1);
	auto xk_norm = cuMatDs<T>::create(this->ncols, 1, this->nrows<this->ncols?this->ncols:this->nrows, 1);
	T lambda, old_lambda, lambda_diff;
	bzero(&lambda, sizeof(T));
	old_lambda = lambda;
	xk->setOnes();
	set_one(&old_lambda);
	lambda_diff = ::sub(old_lambda,lambda);
	int32_t i = 0;
	while((::abs(lambda_diff) > threshold || ::abs(lambda) <= threshold) && i < max_iter)
	{
		i++;
		old_lambda = lambda;
		xk->copy(xk_norm);
		xk_norm->normalize();
		mul(*xk_norm, xk);
		dsm_dot(xk, xk_norm, &lambda);
		lambda_diff = ::sub(old_lambda, lambda);
	}
	delete xk_norm;
	delete xk;
	switch_back();
	return lambda;
}

template<typename T>
T cuMatDs<T>::coeff_max()
{
	auto switch_back = switch_dev(this->dev_id);
	auto len = numel();
	T max = faust_cu_max(data, len);
	switch_back();
	return max;
}

template<typename T>
T cuMatDs<T>::coeff_min()
{
	auto switch_back = switch_dev(this->dev_id);
	auto len = numel();
	T min = faust_cu_min(data, len);
	switch_back();
	return min;
}

template<typename T>
T cuMatDs<T>::coeff(const int32_t i, const int32_t j) const
{
	T c;
	coeff(i, j, c);
	return c;
}

template<typename T>
void cuMatDs<T>::coeff(const int32_t i, const int32_t j, T & c) const
{
	auto switch_back = switch_dev(this->dev_id);
	if(i < 0 || i >= this->nrows)
		throw std::runtime_error("index i is out of bounds for the rows.");
	if(j < 0 || j >= this->ncols)
		throw std::runtime_error("index j is out of bounds for the columns.");
	copy_dbuf2hbuf(1, this->data+j*this->nrows+i, &c, dev_id, stream);
	switch_back();
}

template<typename T>
void cuMatDs<T>::set_coeff(const int32_t i, const int32_t j, const T & c) const
{
	auto switch_back = switch_dev(this->dev_id);
	if(i < 0 || i >= this->nrows)
		throw std::runtime_error("index i is out of bounds for the rows.");
	if(j < 0 || j >= this->ncols)
		throw std::runtime_error("index j is out of bounds for the columns.");
	copy_hbuf2dbuf(1, &c, this->data+j+this->nrows+i, dev_id, stream);
	switch_back();
}

template<typename T>
T cuMatDs<T>::sum()
{
	auto switch_back = switch_dev(this->dev_id);
	auto len = numel();
	auto s = faust_cu_sum(data, len);
	switch_back();
	return s;
}

template<typename T>
T cuMatDs<T>::mean()
{
	auto s = sum();
	return s/(this->nrows*this->ncols);
}

template<typename T>
T cuMatDs<T>::trace()
{
	auto switch_back = switch_dev(this->dev_id);
	auto len = this->nrows<this->ncols?this->nrows:this->ncols;
	cuMatDs<T> diag(len, 1);
	kernel_get_diag(diag.data, data, this->nrows, len);
	T trace = faust_cu_sum(diag.data, len);
	switch_back();
	return trace;
}

template<typename T>
void cuMatDs<T>::resize(const int32_t nrows, const int32_t ncols)
{
	auto switch_back = switch_dev(this->dev_id);
	if(this->nrows != nrows || this->ncols != ncols)
	{
		if(nrows*ncols < buf_nrows*buf_ncols)
		{ // the matrix buffer is larger that the current matrix size needs
			// the new size too, just change nrows, ncols
			this->nrows = nrows;
			this->ncols = ncols;
		}
		else
		{ // realloc the matrix buffer
			T* cu_data;
			alloc_dbuf(nrows*ncols, &cu_data);
			buf_nrows = nrows;
			buf_ncols = ncols;
			this->nrows = nrows;
			this->ncols = ncols;
			// free old buf
			if(this->data != nullptr)
				free_dbuf(this->data);
			this->data = cu_data;
		}
	}
	switch_back();
}

template<typename T>
void cuMatDs<T>::mul(const T& scal)
{
	auto switch_back = switch_dev(this->dev_id);
	cublasTscal(cuMatDs<T>::handle, this->ncols*this->nrows, &scal, data, 1);
	switch_back();
}

template<typename T>
Real<T> cuMatDs<T>::norm_l1()
{
	auto switch_back = switch_dev(this->dev_id);
	Real<T> lambda;
	// sum each column elements
	int32_t sum_size[2] = {this->ncols, 1};
	Real<T>* col_sums = new Real<T>[this->ncols];
	Real<T>* gpu_col_sums; // TODO: use directly a cuMatDs
	alloc_dbuf(this->ncols, &gpu_col_sums, this->dev_id);
	for(int j=0;j < this->ncols; j++)
	{
		col_sums[j] = faust_cu_sum_abs(data+this->nrows*j, this->nrows);
	}
	//TODO: a function faust_cu_sum_abs_max to avoid the cpu copy (it implies to write a new kernel)
	// or use thrust maybe
	copy_hbuf2dbuf(this->ncols, col_sums, gpu_col_sums, this->dev_id, this->stream);
	lambda = faust_cu_max(gpu_col_sums, this->ncols);
	delete col_sums;
	free_dbuf(gpu_col_sums);
	switch_back();
	return lambda;
}

template<typename T>
	void cuMatDs<T>::set_stream(const void* stream)
{
	this->stream = const_cast<void*>(stream);
}

template<typename T>
	void cuMatDs<T>::mv_to_gpu(int32_t dev_id)
{
	if(this->dev_id != dev_id)
	{
		T* data;
		auto len = buf_nrows*buf_ncols;
		alloc_dbuf(len, &data, dev_id);
		copy_dbuf2dbuf(len, this->data, data, this->dev_id, dev_id, this->stream);
		this->dev_id = dev_id;
		auto switch_back = switch_dev(this->dev_id); // just in case
		free_dbuf(this->data);
		switch_back();
		this->data = data;
	}
}

template<typename T>
void dsm_gemm(const gm_DenseMat_t vA, const gm_DenseMat_t vB, gm_DenseMat_t vC, const T &alpha, const T &beta, const gm_Op op_A, const gm_Op op_B)
{
	const cuMatDs<T> *A = static_cast<cuMatDs<T>*>(vA);
	const cuMatDs<T> *B = static_cast<cuMatDs<T>*>(vB);
	cuMatDs<T> *C = static_cast<cuMatDs<T>*>(vC);
	dsm_gemm(A, B, C, alpha, beta, op_A, op_B);
}

template<typename T>
void dsm_gemm(const cuMatDs<T>* A, const cuMatDs<T>* B, cuMatDs<T>* C, const T &alpha, const T &beta, const gm_Op op_A, const gm_Op op_B)
{
	auto switch_back = switch_dev(A->dev_id);
	int m, n, k, l;
	int32_t lda, ldb, ldc;
	auto cublas_op_A = gm_Op2cublas(op_A);
	auto cublas_op_B = gm_Op2cublas(op_B);
	lda = ldb = ldc = A->nrows;
	m = op_A==OP_NOTRANSP?A->nrows:A->ncols;
	n = op_B==OP_NOTRANSP?B->ncols:B->nrows;
	k = op_A==OP_NOTRANSP?A->ncols:A->nrows;
	l = op_B==OP_NOTRANSP?B->nrows:B->ncols;
	if(k != l) throw std::runtime_error("dsm_gemm() dimensions must agree.");
	if(C == nullptr)
		throw std::runtime_error("dsm_gemm() C is nullptr, it must be initialized.");
	if(m*n > C->buf_nrows*C->buf_ncols)
		throw std::runtime_error("dsm_gemm() the C buf. size is not large enough.");
	C->nrows = m;
	C->ncols = n;
	lda = A->nrows;
	ldb = B->nrows;
	ldc = m;
	auto status = cublasTgemm(cuMatDs<T>::handle, cublas_op_A, cublas_op_B, m, n, k,
			/* alpha*/ &alpha,	A->data, lda, B->data, ldb,
			/* beta */ &beta, C->data, ldc);
	check_cublas_error(status, "dsm_gemm > cublasTgemm");
	switch_back();
}

#if (CUDA_VERSION > 9200)
template<typename T>
cusparseStatus_t dsm_mat2desc(const cuMatDs<T>& A, cusparseDnMatDescr_t& dnMatDescr)
{
	int64_t ld = A.nrows;
	cudaDataType valueType = type2cudaDataType(*(A.data));
	cusparseOrder_t order = CUSPARSE_ORDER_COL; // cuda-CUDA_VERSION/targets/x86_64-linux/include/cusparse.h
	cusparseStatus_t ret =  cusparseCreateDnMat(&dnMatDescr, A.nrows, A.ncols, ld, static_cast<void*>(A.data), valueType, order);
	return ret;
}
#endif
