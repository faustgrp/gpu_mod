SRC=$(wildcard src/*.cpp)
OBJ=$(subst .cpp,.o, $(SRC))
SLIB_DIR=../tests_cuBLAS-cuSPARSE
LIB_DIR=./libs

CC=g++
CFLAGS=-Isrc -I$(HOME)/cuda-samples/Common -I$(HOME)/cuda-includes -O3 -I. -I/usr/include/eigen3
STATIC_LIBS=$(addprefix $(SLIB_DIR),libcublas_static.a libcusparse_static.a) #/usr/lib/gcc/x86_64-redhat-linux/9/libstdc++.a

LDFLAGS=-L$(LIB_DIR) -lcublas -lcusparse -lcudart -lcuda

all: libgm.so test_gm

libgm.so: $(OBJ)
	$(CC) -shared -o $@ $(filter-out src/test_gm.o, $(OBJ)) $(LDFLAGS)

libgm_static.so: $(OBJ) $(STATIC_LIBS)
	$(CC) -shared -o $@ $(filter-out src/test_gm.o, $(OBJ)) -static $(STATIC_LIBS)

test_gm: test/test_gm.o libgm.so
	$(CC) -o test_gm -ldl -L. $<

%.o: %.cpp
	$(CC) $(CFLAGS) -fPIC -c $< -o $@

gm.cpp: gm.hpp gm.h

cuda_utils.cpp: cuda_utils.hpp cuda_utils.h

clean:
	rm -f $(OBJ)
	rm -f test/*.o
